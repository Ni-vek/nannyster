# execute first apt to run apt-get update
include_recipe "apt"

execute "installing or updating build essentials (make)" do
    command "cd / && apt-get -y install build-essential"
    user "root"
end
execute "installing or updating pcre" do
    command "cd / && apt-get -y install libpcre3-dev"
    user "root"
end

include_recipe "nginx"
include_recipe "php"
include_recipe "php-fpm"

execute "installing or updating php gd" do
    command "cd / && apt-get -y install php5-gd"
    user "root"
end

execute "installing or updating php curl" do
    command "cd / && apt-get -y install php5-curl"
    user "root"
end

execute "installing composer" do
    command "cd /var/www/ma-nourrice && curl -sS https://getcomposer.org/installer | php"
    user "root"
end

# execute "installing or updating phalcon" do
#     if File.directory?('/cphalcon')
#         command "cd / && rm -R /cphalcon && git clone git://github.com/phalcon/cphalcon.git && cd cphalcon/build && ./install && cd / && rm /etc/php5/fpm/conf.d/phalcon.ini && echo 'extension=phalcon.so' > /etc/php5/fpm/conf.d/phalcon.ini"
#         user "root"
#     else
#         command "cd / && git clone git://github.com/phalcon/cphalcon.git && cd cphalcon/build && ./install && cd / && echo 'extension=phalcon.so' > /etc/php5/fpm/conf.d/phalcon.ini"
#         user "root"
#     end
# end

include_recipe "mongodb"
php_pear "mongo" do
  action :install
end

execute "restoring mongodb" do
    command "cd /var/www/ma-nourrice/install/mongodb/ && mongorestore --port #{node['mongodb']['config']['port']}"
    user "root"
end

execute "restarting services" do
    command "service nginx restart && service mongodb restart && service php5-fpm restart"
    user "root"
end

execute "retrieving dependencies" do
    command "cd /var/www/ma-nourrice && php composer.phar install"
    user "root"
end