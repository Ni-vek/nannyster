//Tooltips
$(document).ready(function() {
    jQuery('[data-rel="tooltip"]').tooltip({html: true});
    // French shortened
    jQuery.timeago.settings.strings = {
        prefixAgo: "il y a",
        prefixFromNow: "d'ici",
        seconds: "moins d'une minute",
        minute: "une minute",
        minutes: "%d minutes",
        hour: "une heure",
        hours: "%d heures",
        day: "un jour",
        days: "%d jours",
        month: "un mois",
        months: "%d mois",
        year: "un an",
        years: "%d ans"
    };
    $('.timeago').timeago();

    var offset = 220;
    var duration = 'slow';
    jQuery(window).scroll(function() {
        if (jQuery(this).scrollTop() > offset) {
            jQuery('#btn-scroll-up').fadeIn(duration);
        } else {
            jQuery('#btn-scroll-up').fadeOut(duration);
        }
    });
});

//Check for session timeout
var sessionCheck = setInterval(
        function() {
            $.ajax({
                type: "POST",
                url: "/session/check",
                data: 'check',
                cache: false

            })
                    .done(function(res) {
                        var cookie = $.cookie('NSC');
                        if (res === "0") {
                            if(window.location.pathname !== '/session/lock' && window.location.pathname !== '/index'){
                                if (cookie) {
                                    window.location.href = '/session/lock';
                                }
                                else {
                                    window.location.href = '/index';
                                }
                            }
                        }
                        else{
                            if(window.location.pathname === '/session/lock' || window.location.pathname === '/index'){
                                    window.location.href = '/dashboard';
                            }
                        }
                    });
        }
, 1440001); // 24 minutes and 1 second. php session.gc_maxlifetime is set to 24 minutes.



// Find the right method, call on correct element
function launchFullscreen(element) {
    if (!$('body').hasClass("full-screen")) {
        $('body').addClass("full-screen");
        if (element.requestFullscreen) {
            element.requestFullscreen();
        } else if (element.mozRequestFullScreen) {
            element.mozRequestFullScreen();
        } else if (element.webkitRequestFullscreen) {
            element.webkitRequestFullscreen();
        } else if (element.msRequestFullscreen) {
            element.msRequestFullscreen();
        }

    } else {
        $('body').removeClass("full-screen");
        if (document.exitFullscreen) {
            document.exitFullscreen();
        } else if (document.mozCancelFullScreen) {
            document.mozCancelFullScreen();
        } else if (document.webkitExitFullscreen) {
            document.webkitExitFullscreen();
        }

    }

}