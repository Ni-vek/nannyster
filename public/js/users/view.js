jQuery(function($) {
    $(function() {
        var cookie = $.cookie('nannyInformationInfoPerso');
        if (!cookie) {
            $('#div-infos-informations-perso').slideToggle();
        }
    });


    //cookie acceptation des infos sur les informations personnelles
    $('#infos-informations-perso').click(function(event) {
        event.preventDefault();
        $.cookie('nannyInformationInfoPerso', 'OK', {
            expires: 365,
            path: '/'
        });
        $(this).parent().slideToggle();
    });

    //automatically show next editable
    $('.editable').on('hidden', function(e, reason) {
        if (reason === 'save' || reason === 'nochange') {
            var $next = $(this).closest('div.profile-info-row').next().find('span.editable');
            console.log($next);
            setTimeout(function() {
                $next.editable('show');
            }, 300);
        }
    });

    //editables on first profile page
    $.fn.editable.defaults.mode = 'popup';
    $.fn.editableform.buttons = '<button type="submit" class="btn btn-info editable-submit"><i class="icon-ok icon-white"></i></button>' +
            '<button type="button" class="btn editable-cancel"><i class="icon-remove"></i></button>';
    $.fn.datepicker.dates['fr'] =
            {
                days: ["Dimanche", "Lundi", "Mardi", "Mercredi", "Jeudi", "Vendredi", "Samedi", "Dimanche"],
                daysShort: ["Dim", "Lun", "Mar", "Mer", "Jeu", "Ven", "Sam", "Dim"],
                daysMin: ["Di", "Lu", "Ma", "Me", "Je", "Ve", "Sa", "Di"],
                months: ["Janvier", "Février", "Mars", "Avril", "Mai", "Juin", "Juillet", "Aoüt", "Septembre", "Octobre", "Novembre", "Decembre"],
                monthsShort: ["Jan", "Fev", "Mar", "Avr", "Mai", "Jun", "Jui", "Aou", "Sep", "Oct", "Nov", "Dec"],
                today: "Aujourd'hui",
                clear: "Effacer"
            };

    //editables 
    $('#dateBirth').editable({
        type: 'date',
        url: '/users/ajaxProfileEditor',
        name: 'birth_date',
        pk: pkey,
        emptytext: 'Date de naissance non renseignée',
        format: 'yyyy-mm-dd',
        viewformat: 'dd-mm-yyyy',
        lang: 'fr',
        error: function(data) {
            $.gritter.add({
                title: 'Erreur',
                text: data,
                class_name: 'gritter-error'
            });
        },
        success: function(response, newValue) {
            $.gritter.add({
                title: 'Confirmation',
                text: 'Votre date de naissance a bien été mise à jour.',
                class_name: 'gritter-success'
            });
        },
        validate: function(newValue) {
            if (newValue == '') {
                $.gritter.add({
                    title: 'Erreur',
                    text: 'Votre date de naissance est obligatoire.',
                    class_name: 'gritter-error'
                });
                return 'Champ obligatoire';
            }
        }
    });

    $('#cityBirth').editable({
        type: 'select2',
        url: '/users/ajaxProfileEditor',
        name: 'birth_city',
        pk: pkey,
        emptytext: 'Ville non renseignée',
        select2: {
            placeholder: 'Selectionnez une ville',
            allowClear: true,
            width: '230px',
            minimumInputLength: 3,
            id: function(e) {
                return e.id;
            },
            ajax: {
                url: '/cities/ajaxCitiesFinder',
                type: 'post',
                dataType: 'json',
                data: function(term, page) {
                    return {data: {
                            q: term
                        }};
                },
                results: function(data, page) {
                    var cities = [];
                    for (var key in data.results) {
                        cities.push({id: key, text: data.results[key]});
                    }
                    return {results: cities};
                },
                formatResult: function(cities) {
                    return cities.text;
                },
                formatSelection: function(cities) {
                    return cities.text;
                }
            }
        }
        /* suucess not needed
         ,
         success: function(response) {
         $('#RequestUser').text(response.newVal);
         }   
         */
    });

//    $('#cityBirth').editable({
//        type: 'text',
//        url: '/users/ajaxProfileEditor',
//        name: 'city_birth',
//        pk: pkey,
//        emptytext: 'Lieu de naissance non renseignée',
//        error: function(data) {
//            $.gritter.add({
//                title: 'Erreur',
//                text: data,
//                class_name: 'gritter-error'
//            });
//        },
//        success: function(response, newValue) {
//            $.gritter.add({
//                title: 'Confirmation',
//                text: 'Votre lieu de naissance a bien été mis à jour.',
//                class_name: 'gritter-success'
//            });
//        },
//        validate: function(newValue) {
//            if (newValue == '') {
//                $.gritter.add({
//                    title: 'Erreur',
//                    text: 'Votre lieu de naissance est obligatoire.',
//                    class_name: 'gritter-error'
//                });
//                return 'Champ obligatoire';
//            }
//        }
//    });

    $('#rc_address').editable({
        type: 'text',
        url: '/users/ajaxProfileEditor',
        name: '\\Nannyster\\Models\\Insurances.address.civil_responsability',
        pk: pkey,
        emptytext: 'Adresse non renseignée',
        error: function(data) {
            $.gritter.add({
                title: 'Erreur',
                text: data,
                class_name: 'gritter-error'
            });
        },
        success: function(response, newValue) {
            $.gritter.add({
                title: 'Confirmation',
                text: 'L\'adresse a bien été mise à jour.',
                class_name: 'gritter-success'
            });
        },
        validate: function(newValue) {
            if (newValue == '') {
                $.gritter.add({
                    title: 'Erreur',
                    text: 'L\'adresse est obligatoire.',
                    class_name: 'gritter-error'
                });
                return 'Champ obligatoire';
            }
        }
    });

    $('#rc_validity').editable({
        type: 'date',
        url: '/users/ajaxProfileEditor',
        name: '\\Nannyster\\Models\\Insurances.validity_date.civil_responsability',
        pk: pkey,
        emptytext: 'Date de validité non renseignée',
        viewformat: 'dd-mm-yyyy',
        lang: 'fr',
        error: function(data) {
            $.gritter.add({
                title: 'Erreur',
                text: data,
                class_name: 'gritter-error'
            });
        },
        success: function(response, newValue) {
            $.gritter.add({
                title: 'Confirmation',
                text: 'La date de validité a bien été mise à jour.',
                class_name: 'gritter-success'
            });
        },
        validate: function(newValue) {
            if (newValue == '') {
                $.gritter.add({
                    title: 'Erreur',
                    text: 'La date de validité est obligatoire.',
                    class_name: 'gritter-error'
                });
                return 'Champ obligatoire';
            }
        }
    });

    $('#rc_no').editable({
        type: 'text',
        url: '/users/ajaxProfileEditor',
        name: '\\Nannyster\\Models\\Insurances.policy_nb.civil_responsability',
        pk: pkey,
        emptytext: 'N° de police non renseigné',
        error: function(data) {
            $.gritter.add({
                title: 'Erreur',
                text: data,
                class_name: 'gritter-error'
            });
        },
        success: function(response, newValue) {
            $.gritter.add({
                title: 'Confirmation',
                text: 'Le n° de police a bien été mis à jour.',
                class_name: 'gritter-success'
            });
        },
        validate: function(newValue) {
            if (newValue == '') {
                $.gritter.add({
                    title: 'Erreur',
                    text: 'Le n° de police est obligatoire.',
                    class_name: 'gritter-error'
                });
                return 'Champ obligatoire';
            }
        }
    });

    $('#rc_city').editable({
        type: 'select2',
        url: '/users/ajaxProfileEditor',
        name: '\\Nannyster\\Models\\Insurances.city.civil_responsability',
        pk: pkey,
        emptytext: 'Ville non renseignée',
        select2: {
            placeholder: 'Selectionnez une ville',
            allowClear: true,
            width: '230px',
            minimumInputLength: 3,
            id: function(e) {
                return e.id;
            },
            ajax: {
                url: '/cities/ajaxCitiesFinder',
                type: 'post',
                dataType: 'json',
                data: function(term, page) {
                    return {data: {
                            q: term
                        }};
                },
                results: function(data, page) {
                    var cities = [];
                    for (var key in data.results) {
                        cities.push({id: key, text: data.results[key]});
                    }
                    return {results: cities};
                },
                formatResult: function(cities) {
                    return cities.text;
                },
                formatSelection: function(cities) {
                    return cities.text;
                }
            }
        }
    });

    $('#rc_compagny').editable({
        type: 'text',
        url: '/users/ajaxProfileEditor',
        name: '\\Nannyster\\Models\\Insurances.compagny_name.civil_responsability',
        pk: pkey,
        emptytext: 'Compagnie d\'assurance responsabilité civile non renseignée',
        error: function(data) {
            $.gritter.add({
                title: 'Erreur',
                text: data,
                class_name: 'gritter-error'
            });
        },
        success: function(response, newValue) {
            $.gritter.add({
                title: 'Confirmation',
                text: 'Votre compagnie d\'assurance a bien été mise à jour.',
                class_name: 'gritter-success'
            });
        },
        validate: function(newValue) {
            if (newValue == '') {
                $.gritter.add({
                    title: 'Erreur',
                    text: 'Votre compagnie d\'assurance est obligatoire.',
                    class_name: 'gritter-error'
                });
                return 'Champ obligatoire';
            }
        }
    });

    $('#auto_address').editable({
        type: 'text',
        url: '/users/ajaxProfileEditor',
        name: '\\Nannyster\\Models\\Insurances.address.auto_insurance',
        pk: pkey,
        emptytext: 'Adresse non renseignée',
        error: function(data) {
            $.gritter.add({
                title: 'Erreur',
                text: data,
                class_name: 'gritter-error'
            });
        },
        success: function(response, newValue) {
            $.gritter.add({
                title: 'Confirmation',
                text: 'L\'adresse a bien été mise à jour.',
                class_name: 'gritter-success'
            });
        },
        validate: function(newValue) {
            if (newValue == '') {
                $.gritter.add({
                    title: 'Erreur',
                    text: 'L\'adresse est obligatoire.',
                    class_name: 'gritter-error'
                });
                return 'Champ obligatoire';
            }
        }
    });

    $('#auto_validity').editable({
        type: 'date',
        url: '/users/ajaxProfileEditor',
        name: '\\Nannyster\\Models\\Insurances.validity_date.auto_insurance',
        pk: pkey,
        emptytext: 'Date de validité non renseignée',
        viewformat: 'dd-mm-yyyy',
        lang: 'fr',
        error: function(data) {
            $.gritter.add({
                title: 'Erreur',
                text: data,
                class_name: 'gritter-error'
            });
        },
        success: function(response, newValue) {
            $.gritter.add({
                title: 'Confirmation',
                text: 'La date de validité a bien été mise à jour.',
                class_name: 'gritter-success'
            });
        },
        validate: function(newValue) {
            if (newValue == '') {
                $.gritter.add({
                    title: 'Erreur',
                    text: 'La date de validité est obligatoire.',
                    class_name: 'gritter-error'
                });
                return 'Champ obligatoire';
            }
        }
    });

    $('#auto_no').editable({
        type: 'text',
        url: '/users/ajaxProfileEditor',
        name: '\\Nannyster\\Models\\Insurances.policy_nb.auto_insurance',
        pk: pkey,
        emptytext: 'N° de police non renseigné',
        error: function(data) {
            $.gritter.add({
                title: 'Erreur',
                text: data,
                class_name: 'gritter-error'
            });
        },
        success: function(response, newValue) {
            $.gritter.add({
                title: 'Confirmation',
                text: 'Le n° de police a bien été mis à jour.',
                class_name: 'gritter-success'
            });
        },
        validate: function(newValue) {
            if (newValue == '') {
                $.gritter.add({
                    title: 'Erreur',
                    text: 'Le n° de police est obligatoire.',
                    class_name: 'gritter-error'
                });
                return 'Champ obligatoire';
            }
        }
    });

    $('#auto_city').editable({
        type: 'select2',
        url: '/users/ajaxProfileEditor',
        name: '\\Nannyster\\Models\\Insurances.city.auto_insurance',
        pk: pkey,
        emptytext: 'Ville non renseignée',
        select2: {
            placeholder: 'Selectionnez une ville',
            allowClear: true,
            width: '230px',
            minimumInputLength: 3,
            id: function(e) {
                return e.id;
            },
            ajax: {
                url: '/cities/ajaxCitiesFinder',
                type: 'post',
                dataType: 'json',
                data: function(term, page) {
                    return {data: {
                            q: term
                        }};
                },
                results: function(data, page) {
                    var cities = [];
                    for (var key in data.results) {
                        cities.push({id: key, text: data.results[key]});
                    }
                    return {results: cities};
                },
                formatResult: function(cities) {
                    return cities.text;
                },
                formatSelection: function(cities) {
                    return cities.text;
                }
            }
        }
    });

    $('#auto_compagny').editable({
        type: 'text',
        url: '/users/ajaxProfileEditor',
        name: '\\Nannyster\\Models\\Insurances.compagny_name.auto_insurance',
        pk: pkey,
        emptytext: 'Compagnie d\'assurance responsabilité civile non renseignée',
        error: function(data) {
            $.gritter.add({
                title: 'Erreur',
                text: data,
                class_name: 'gritter-error'
            });
        },
        success: function(response, newValue) {
            $.gritter.add({
                title: 'Confirmation',
                text: 'Votre compagnie d\'assurance a bien été mise à jour.',
                class_name: 'gritter-success'
            });
        },
        validate: function(newValue) {
            if (newValue == '') {
                $.gritter.add({
                    title: 'Erreur',
                    text: 'Votre compagnie d\'assurance est obligatoire.',
                    class_name: 'gritter-error'
                });
                return 'Champ obligatoire';
            }
        }
    });

    $('#address').editable({
        type: 'text',
        url: '/users/ajaxProfileEditor',
        name: '\\Nannyster\\Models\\Addresses.address_1.address',
        pk: pkey,
        emptytext: 'Adresse non renseignée',
        error: function(data) {
            console.log(data);
            $.gritter.add({
                title: 'Erreur',
                text: data,
                class_name: 'gritter-error'
            });
        },
        success: function(response, newValue) {
            $.gritter.add({
                title: 'Confirmation',
                text: 'Votre adresse a bien été mise à jour.',
                class_name: 'gritter-success'
            });
        },
        validate: function(newValue) {
            if (newValue == '') {
                $.gritter.add({
                    title: 'Erreur',
                    text: 'Votre adresse est obligatoire.',
                    class_name: 'gritter-error'
                });
                return 'Champ obligatoire';
            }
        }
    });

    $('#address2').editable({
        type: 'text',
        url: '/users/ajaxProfileEditor',
        name: '\\Nannyster\\Models\\Addresses.address_2.address',
        pk: pkey,
        emptytext: 'Adresse 2 non renseignée',
        error: function(data) {
            console.log(data);
            $.gritter.add({
                title: 'Erreur',
                text: data,
                class_name: 'gritter-error'
            });
        },
        success: function(response, newValue) {
            $.gritter.add({
                title: 'Confirmation',
                text: 'Votre adresse a bien été mise à jour.',
                class_name: 'gritter-success'
            });
        },
        validate: function(newValue) {
            if (newValue !== '') {
                $('#add-address3').toggle();
            }
            else {
                $('#add-address3').hide();
                $('#address3').hide();
            }
        }
    });

    $('#address3').editable({
        type: 'text',
        url: '/users/ajaxProfileEditor',
        name: '\\Nannyster\\Models\\Addresses.address_3.address',
        pk: pkey,
        emptytext: 'Adresse 3 non renseignée',
        error: function(data) {
            console.log(data);
            $.gritter.add({
                title: 'Erreur',
                text: data,
                class_name: 'gritter-error'
            });
        },
        success: function(response, newValue) {
            $.gritter.add({
                title: 'Confirmation',
                text: 'Votre adresse a bien été mise à jour.',
                class_name: 'gritter-success'
            });
        }
    });

    $('#pajemploi').editable({
        type: 'text',
        url: '/users/ajaxProfileEditor',
        name: 'pajemploi_nb',
        pk: pkey,
        emptytext: 'N° Pajemploi non renseigné',
        error: function(data) {
            $.gritter.add({
                title: 'Erreur',
                text: data,
                class_name: 'gritter-error'
            });
        },
        success: function(response, newValue) {
            $.gritter.add({
                title: 'Confirmation',
                text: 'Votre N° Pajemploi a bien été mis à jour.',
                class_name: 'gritter-success'
            });
        },
        validate: function(newValue) {
            if (newValue == '') {
                $.gritter.add({
                    title: 'Erreur',
                    text: 'Votre N° Pajemploi est obligatoire.',
                    class_name: 'gritter-error'
                });
                return 'Champ obligatoire';
            }
        }
    });

    $('#tauxHoraire').editable({
        type: 'text',
        url: '/users/ajaxProfileEditor',
        name: '\\Nannyster\\Models\\MoneyRates.hourly_rate.money_rate',
        pk: pkey,
        emptytext: 'Taux horaire non renseigné',
        display: function(value) {
            if (value != 'Non renseigné') {
                $(this).text(value + ' €');
            }
            ;
        },
        error: function(data) {
            $.gritter.add({
                title: 'Erreur',
                text: data,
                class_name: 'gritter-error'
            });
        },
        success: function(response, newValue) {
            $.gritter.add({
                title: 'Confirmation',
                text: 'Votre taux horaire a bien été mis à jour.',
                class_name: 'gritter-success'
            });
        },
        validate: function(newValue) {
            if (newValue == '') {
                $.gritter.add({
                    title: 'Erreur',
                    text: 'Votre taux horaire est obligatoire.',
                    class_name: 'gritter-error'
                });
                return 'Champ obligatoire';
            }
        }
    });

    $('#indemniteEntretien').editable({
        type: 'text',
        url: '/users/ajaxProfileEditor',
        name: '\\Nannyster\\Models\\MoneyRates.maintenance_allowance.money_rate',
        pk: pkey,
        emptytext: 'Indemnité d\'entretien non renseignée',
        display: function(value) {
            if (value != 'Non renseigné') {
                $(this).text(value + ' €');
            }
            ;
        },
        error: function(data) {
            $.gritter.add({
                title: 'Erreur',
                text: data,
                class_name: 'gritter-error'
            });
        },
        success: function(response, newValue) {
            $.gritter.add({
                title: 'Confirmation',
                text: 'Votre indemnité d\'entretien a bien été mise à jour.',
                class_name: 'gritter-success'
            });
        },
        validate: function(newValue) {
            if (newValue == '') {
                $.gritter.add({
                    title: 'Erreur',
                    text: 'Votre indemnité d\'entretien est obligatoire.',
                    class_name: 'gritter-error'
                });
                return 'Champ obligatoire';
            }
        }
    });

    $('#fraisPetitDejeuner').editable({
        type: 'text',
        url: '/users/ajaxProfileEditor',
        name: '\\Nannyster\\Models\\MoneyRates.breakfast_costs.money_rate',
        pk: pkey,
        emptytext: 'Frais de petit déjeuner non renseignés',
        display: function(value) {
            if (value != 'Non renseigné') {
                $(this).text(value + ' €');
            }
            ;
        },
        error: function(data) {
            $.gritter.add({
                title: 'Erreur',
                text: data,
                class_name: 'gritter-error'
            });
        },
        success: function(response, newValue) {
            $.gritter.add({
                title: 'Confirmation',
                text: 'Vos frais de petit déjeuner ont bien été mis à jour.',
                class_name: 'gritter-success'
            });
        },
        validate: function(newValue) {
            if (newValue == '') {
                $.gritter.add({
                    title: 'Erreur',
                    text: 'Vos frais de petit déjeuner sont obligatoires.',
                    class_name: 'gritter-error'
                });
                return 'Champ obligatoire';
            }
        }
    });

    $('#fraisDejeuner').editable({
        type: 'text',
        url: '/users/ajaxProfileEditor',
        name: '\\Nannyster\\Models\\MoneyRates.lunch_costs.money_rate',
        pk: pkey,
        emptytext: 'Frais de déjeuner non renseignés',
        display: function(value) {
            if (value != 'Non renseigné') {
                $(this).text(value + ' €');
            }
            ;
        },
        error: function(data) {
            $.gritter.add({
                title: 'Erreur',
                text: data,
                class_name: 'gritter-error'
            });
        },
        success: function(response, newValue) {
            $.gritter.add({
                title: 'Confirmation',
                text: 'Vos frais de repas ont bien été mis à jour.',
                class_name: 'gritter-success'
            });
        },
        validate: function(newValue) {
            if (newValue == '') {
                $.gritter.add({
                    title: 'Erreur',
                    text: 'Vos frais de repas sont obligatoires.',
                    class_name: 'gritter-error'
                });
                return 'Champ obligatoire';
            }
        }
    });

    $('#fraisGouter').editable({
        type: 'text',
        url: '/users/ajaxProfileEditor',
        name: '\\Nannyster\\Models\\MoneyRates.snack_costs.money_rate',
        pk: pkey,
        emptytext: 'Frais de gouter non renseignés',
        display: function(value) {
            if (value != 'Non renseigné') {
                $(this).text(value + ' €');
            }
            ;
        },
        error: function(data) {
            $.gritter.add({
                title: 'Erreur',
                text: data,
                class_name: 'gritter-error'
            });
        },
        success: function(response, newValue) {
            $.gritter.add({
                title: 'Confirmation',
                text: 'Vos frais de gouter ont bien été mis à jour.',
                class_name: 'gritter-success'
            });
        },
        validate: function(newValue) {
            if (newValue == '') {
                $.gritter.add({
                    title: 'Erreur',
                    text: 'Vos frais de gouter sont obligatoires.',
                    class_name: 'gritter-error'
                });
                return 'Champ obligatoire';
            }
        }
    });

    $('#fraisDiner').editable({
        type: 'text',
        url: '/users/ajaxProfileEditor',
        name: '\\Nannyster\\Models\\MoneyRates.diner_costs.money_rate',
        pk: pkey,
        emptytext: 'Frais de diner non renseignés',
        display: function(value) {
            if (value != 'Non renseigné') {
                $(this).text(value + ' €');
            }
            ;
        },
        error: function(data) {
            $.gritter.add({
                title: 'Erreur',
                text: data,
                class_name: 'gritter-error'
            });
        },
        success: function(response, newValue) {
            $.gritter.add({
                title: 'Confirmation',
                text: 'Vos frais de diner ont bien été mis à jour.',
                class_name: 'gritter-success'
            });
        },
        validate: function(newValue) {
            if (newValue == '') {
                $.gritter.add({
                    title: 'Erreur',
                    text: 'Vos frais de diner sont obligatoires.',
                    class_name: 'gritter-error'
                });
                return 'Champ obligatoire';
            }
        }
    });

    $('#ssociale').on('shown', function(e, editable) {
        editable.input.$input.mask('9 99 99 99 999 999 99');
    });
    $('#ssociale').editable({
        type: 'text',
        url: '/users/ajaxProfileEditor',
        name: 'social_security_nb',
        pk: pkey,
        emptytext: 'N° de sécurité sociale non renseigné',
        error: function(data) {
            $.gritter.add({
                title: 'Erreur',
                text: data,
                class_name: 'gritter-error'
            });
        },
        success: function(response, newValue) {
            $.gritter.add({
                title: 'Confirmation',
                text: 'Votre N° de sécurité sociale a bien été mis à jour.',
                class_name: 'gritter-success'
            });
        },
        validate: function(newValue) {
            if (newValue == '') {
                $.gritter.add({
                    title: 'Erreur',
                    text: 'Votre N° de sécurité sociale est obligatoire.',
                    class_name: 'gritter-error'
                });
                return 'Champ obligatoire';
            }
        }
    });

    $('#city').editable({
        type: 'select2',
        url: '/users/ajaxProfileEditor',
        name: '\\Nannyster\\Models\\Addresses.city.address',
        pk: pkey,
        emptytext: 'Ville non renseignée',
        select2: {
            placeholder: 'Selectionnez une ville',
            allowClear: true,
            width: '230px',
            minimumInputLength: 3,
            id: function(e) {
                return e.id;
            },
            ajax: {
                url: '/cities/ajaxCitiesFinder',
                type: 'post',
                dataType: 'json',
                data: function(term, page) {
                    return {data: {
                            q: term
                        }};
                },
                results: function(data, page) {
                    var cities = [];
                    for (var key in data.results) {
                        cities.push({id: key, text: data.results[key]});
                    }
                    return {results: cities};
                },
                formatResult: function(cities) {
                    return cities.text;
                },
                formatSelection: function(cities) {
                    return cities.text;
                }
            }
        }
    });
//    $('#city').editable({
//        type: 'text',
//        url: '/users/ajaxProfileEditor',
//        name: 'city',
//        pk: pkey,
//        emptytext: 'Ville non renseignée',
//        error: function(response, newValue) {
//            $.gritter.add({
//                title: 'Erreur',
//                text: 'Une erreur s\'est produite lors de la mise à jour de votre ville. Veuillez recommencer.',
//                class_name: 'gritter-error'
//            });
//        },
//        success: function(response, newValue) {
//            $.gritter.add({
//                title: 'Confirmation',
//                text: 'Votre ville a bien été mise à jour.',
//                class_name: 'gritter-success'
//            });
//        },
//        validate: function(newValue) {
//            if (newValue == '') {
//                $.gritter.add({
//                    title: 'Erreur',
//                    text: 'Votre ville est obligatoire.',
//                    class_name: 'gritter-error'
//                });
//                return 'Champ obligatoire';
//            }
//        }
//    });

    $('#mobile').on('shown', function(e, editable) {
        editable.input.$input.mask('99 99 99 99 99');
    });
    $('#mobile').editable({
        type: 'text',
        url: '/users/ajaxProfileEditor',
        name: 'mobile',
        pk: pkey,
        emptytext: 'Mobile non renseigné',
        error: function(response, newValue) {
            $.gritter.add({
                title: 'Erreur',
                text: 'Une erreur s\'est produite lors de la mise à jour de votre numéro de mobile. Veuillez recommencer.',
                class_name: 'gritter-error'
            });
        },
        success: function(response, newValue) {
            $.gritter.add({
                title: 'Confirmation',
                text: 'Votre numéro de mobile a bien été mis à jour.',
                class_name: 'gritter-success'
            });
        },
        validate: function(newValue) {
            if (newValue == '') {
                $.gritter.add({
                    title: 'Erreur',
                    text: 'Votre numéro de mobile est obligatoire.',
                    class_name: 'gritter-error'
                });
                return 'Champ obligatoire';
            }
        }
    });

    $('#phone').on('shown', function(e, editable) {
        editable.input.$input.mask('99 99 99 99 99');
    });
    $('#phone').editable({
        type: 'text',
        url: '/users/ajaxProfileEditor',
        name: 'phone',
        pk: pkey,
        emptytext: 'Téléphone non renseigné',
        error: function(response, newValue) {
            $.gritter.add({
                title: 'Erreur',
                text: 'Une erreur s\'est produite lors de la mise à jour de votre numéro de téléphone. Veuillez recommencer.',
                class_name: 'gritter-error'
            });
        },
        success: function(response, newValue) {
            $.gritter.add({
                title: 'Confirmation',
                text: 'Votre numéro de téléphone a bien été mis à jour.',
                class_name: 'gritter-success'
            });
        },
        validate: function(newValue) {
            if (newValue == '') {
                $.gritter.add({
                    title: 'Erreur',
                    text: 'Votre numéro de téléphone est obligatoire.',
                    class_name: 'gritter-error'
                });
                return 'Champ obligatoire';
            }
        }
    });

    $('#visibility').editable({
        type: 'select',
        value: $('#visibility').html(),
        source: [
            {
                value: 'true',
                text: 'Vos informations personnelles ne sont visibles que par vous'
            },
            {
                value: 'false',
                text: 'Vos informations personnelles sont visibles par tous'
            }
        ],
        url: '/users/ajaxProfileEditor',
        name: 'private_informations',
        pk: pkey,
        display: function(value) {
            if (value === 'true') {
                $(this).text('Vos informations personnelles ne sont visibles que par vous');
            }
            if (value === 'false') {
                $(this).text('Vos informations personnelles sont visibles par tous');
            }
        },
        error: function(response, newValue) {
            $.gritter.add({
                title: 'Erreur',
                text: 'Une erreur s\'est produite lors de la mise à jour de vos paramètres de confidentialité. Veuillez recommencer.',
                class_name: 'gritter-error'
            });
        },
        success: function(response, newValue) {
            $.gritter.add({
                title: 'Confirmation',
                text: 'Vos paramètres de confidentialité ont bien été mis à jour.',
                class_name: 'gritter-success'
            });
        }
    });

    $('#notifParam').editable({
        type: 'select',
        value: $('#notifParam').html(),
        source: [
            {
                value: true,
                text: 'Vous recevez toutes les notifications par email'
            },
            {
                value: false,
                text: 'Vous ne recevez pas les notifications par email'
            }
        ],
        url: '/users/ajaxProfileEditor',
        name: 'email_notifications',
        pk: pkey,
        display: function(value) {
            if (value) {
                $(this).text('Vous recevez toutes les notifications par email');
            }
            if (!value) {
                $(this).text('Vous ne recevez pas les notifications par email');
            }
        },
        error: function(response, newValue) {
            $.gritter.add({
                title: 'Erreur',
                text: 'Une erreur s\'est produite lors de la mise à jour de vos paramètres de notification. Veuillez recommencer.',
                class_name: 'gritter-error'
            });
        },
        success: function(response, newValue) {
            $.gritter.add({
                title: 'Confirmation',
                text: 'Vos paramètres de notification ont bien été mis à jour.',
                class_name: 'gritter-success'
            });
        }
    });

    $('#agreementFirst').editable({
        type: 'date',
        format: 'yyyy-mm-dd',
        viewformat: 'dd-mm-yyyy',
        datepicker: {
            weekStart: 1
        },
        url: '/users/ajaxProfileEditor',
        name: '\\Nannyster\\Models\\Agreements.start_date.agreement',
        pk: pkey,
        emptytext: 'Date d\'agrément non renseignée',
        error: function(response, newValue) {
            $.gritter.add({
                title: 'Erreur',
                text: 'Une erreur s\'est produite lors de la mise à jour de votre agrément. Veuillez recommencer.',
                class_name: 'gritter-error'
            });
        },
        success: function(response, newValue) {
            $.gritter.add({
                title: 'Confirmation',
                text: 'Votre agrément a bien été mis à jour.',
                class_name: 'gritter-success'
            });
        }
    });

    $('#agreementRenew').editable({
        type: 'date',
        format: 'yyyy-mm-dd',
        viewformat: 'dd-mm-yyyy',
        datepicker: {
            weekStart: 1
        },
        url: '/users/ajaxProfileEditor',
        name: '\\Nannyster\\Models\\Agreements.renew_date.agreement',
        pk: pkey,
        emptytext: 'Date d\'agrément non renseignée',
        error: function(response, newValue) {
            $.gritter.add({
                title: 'Erreur',
                text: 'Une erreur s\'est produite lors de la mise à jour de votre agrément. Veuillez recommencer.',
                class_name: 'gritter-error'
            });
        },
        success: function(response, newValue) {
            $.gritter.add({
                title: 'Confirmation',
                text: 'Votre agrément a bien été mis à jour.',
                class_name: 'gritter-success'
            });
        }
    });

    $('#agreementNumber').editable({
        type: 'text',
        url: '/users/ajaxProfileEditor',
        name: '\\Nannyster\\Models\\Agreements.id_number.agreement',
        pk: pkey,
        emptytext: 'Numéro d\'agrément non renseigné',
        error: function(response, newValue) {
            $.gritter.add({
                title: 'Erreur',
                text: 'Une erreur s\'est produite lors de la mise à jour de votre numéro d\'agrément. Veuillez recommencer.',
                class_name: 'gritter-error'
            });
        },
        success: function(response, newValue) {
            $.gritter.add({
                title: 'Confirmation',
                text: 'Votre numéro d\'agrément a bien été mis à jour.',
                class_name: 'gritter-success'
            });
        }
    });

    $('#agreementChildren').editable({
        type: 'textarea',
        escape: false,
        emptytext: 'Nombre d\'enfants autorisés non renseigné',
        url: '/users/ajaxProfileEditor',
        name: '\\Nannyster\\Models\\Agreements.children_permissions.agreement',
        pk: pkey,
        error: function(response, newValue) {
            $.gritter.add({
                title: 'Erreur',
                text: 'Une erreur s\'est produite lors de la mise à jour des paramètres de votre agrément. Veuillez recommencer.',
                class_name: 'gritter-error'
            });
        },
        success: function(response, newValue) {
            $.gritter.add({
                title: 'Confirmation',
                text: 'Les paramètres de votre agrément ont bien été mis à jour.',
                class_name: 'gritter-success'
            });
        }
    });

    $('#annuaireVisibility').editable({
        type: 'select',
        value: $('#annuaireVisibility').html(),
        source: [
            {
                value: 'Y',
                text: 'Je souhaite apparaître automatiquement dans l\'annuaire'
            },
            {
                value: 'N',
                text: 'Je ne souhaite pas apparaître dans l\'annuaire'
            }
        ],
        url: '/users/ajaxProfileEditor',
        name: 'directory_visible',
        pk: pkey,
        display: function(value) {
            if (value == 'Y') {
                $(this).text('Je souhaite apparaître automatiquement dans l\'annuaire');
            }
            if (value == 'N') {
                $(this).text('Je ne souhaite pas apparaître dans l\'annuaire');
            }
        },
        error: function(response, newValue) {
            $.gritter.add({
                title: 'Erreur',
                text: 'Une erreur s\'est produite lors de la mise à jour de vos paramètres de l\'annuaire Ma Nourrice. Veuillez recommencer.',
                class_name: 'gritter-error'
            });
        },
        success: function(response, newValue) {
            $.gritter.add({
                title: 'Confirmation',
                text: 'Vos paramètres de l\'annuaire Ma Nourrice ont bien été mis à jour.',
                class_name: 'gritter-success'
            });
        }
    });

    $('#annuaireInfo').editable({
        type: 'select',
        value: $('#annuaireInfo').html(),
        source: [
            {
                value: 'Y',
                text: 'Vos coordonnées sont visibles dans l\'annuaire'
            },
            {
                value: 'N',
                text: 'Vos coordonnées ne sont pas visibles dans l\'annuaire'
            }
        ],
        url: '/users/ajaxProfileEditor',
        name: 'directory_private_informations',
        pk: pkey,
        display: function(value) {
            if (value == 'Y') {
                $(this).text('Vos coordonnées sont visibles dans l\'annuaire');
            }
            if (value == 'N') {
                $(this).text('Vos coordonnées ne sont pas visibles dans l\'annuaire');
            }
        },
        error: function(response, newValue) {
            $.gritter.add({
                title: 'Erreur',
                text: 'Une erreur s\'est produite lors de la mise à jour de vos paramètres de l\'annuaire Ma Nourrice. Veuillez recommencer.',
                class_name: 'gritter-error'
            });
        },
        success: function(response, newValue) {
            $.gritter.add({
                title: 'Confirmation',
                text: 'Vos paramètres de l\'annuaire Ma Nourrice ont bien été mis à jour.',
                class_name: 'gritter-success'
            });
        }
    });

    //Buttons to add or remove addresses lines
    $('#add-address2').click(function(event) {
        event.preventDefault();
        $('#add-br-address2').toggle();
        $('#address2').toggle();
        $(this).toggle();
    });
    $('#add-address3').click(function(event) {
        event.preventDefault();
        $('#add-br-address3').toggle();
        $('#address3').toggle();
        $(this).toggle();
    });

    /*
     $('#profile-feed-1').slimScroll({
     });
     $(document).on('click', '#deleteRecommandation', function(){
     var dataid = $(this).attr('data-id');
     $.ajax({
     type: 'POST',
     url: "/usersnannyrecommandations/ajaxCommentsRemover",
     data: {
     'id' : dataid
     }
     })
     .fail(function() {
     $.gritter.add({
     title: 'Erreur',
     text: 'Le commentaire n\'a pas pu être supprimé. Veuillez recommencer.',
     class_name: 'gritter-error'
     });
     })
     .done(function() {
     var count = $('#count-comments').html();
     $('#count-comments').html(count - 1);
     if($('#count-comments').html() == 1){
     $('#s-comments').remove();
     }
     $('#comment-id-'+dataid).slideUp();
     $.gritter.add({
     title: 'Confirmation',
     text: 'Le commentaire a bien été supprimé.',
     class_name: 'gritter-success'
     });
     });
     });
     */
    /*
     $(document).on('click', '#acceptRecommandation', function(){
     var dataid = $(this).attr('data-id');
     $.ajax({
     type: 'POST',
     url: "/usersnannyrecommandations/ajaxCommentsAccepter",
     data: {
     'id' : dataid
     }
     })
     .fail(function() {
     $.gritter.add({
     title: 'Erreur',
     text: 'Le commentaire n\'a pas pu être accepté. Veuillez recommencer.',
     class_name: 'gritter-error'
     });
     })
     .done(function() {
     $('#label-comment-'+dataid).removeClass('label-danger');
     $('#label-comment-'+dataid).addClass('label-success');
     $('#label-comment-'+dataid).html('Commentaire validé');
     $('.iconClass-accept-'+dataid).remove();
     $.gritter.add({
     title: 'Confirmation',
     text: 'Le commentaire a bien été accepté.',
     class_name: 'gritter-success'
     });
     });
     });
     */



    var uploader = new plupload.Uploader({
        runtimes: 'html5',
        container: 'plupload',
        browse_button: 'browse',
        drop_element: 'droparea',
        url: '/users/ajaxImageEditor',
        multipart: true,
        urlstream_upload: true,
        max_file_size: '10mb',
        filters: [
            {
                title: 'Images',
                extensions: 'jpg,png,gif,JPG,PNG,GIF,jpeg,JPEG'
            }
        ]
    });

    uploader.init();

    uploader.bind('FilesAdded', function(up, files) {
        $('#progressbar').slideDown();
        uploader.start();
        uploader.refresh();
    });

    uploader.bind('Error', function(up, err) {
        console.log(up);
        console.log(err);
        $.gritter.add({
            title: 'Erreur',
            text: 'Votre fichier ne doit pas excéder 10MB et doit être au format JPG, JPEG, PNG ou GIF.',
            class_name: 'gritter-error'
        });
        hideProgessBar();
        uploader.refresh();

    });

    uploader.bind('FileUploaded', function(up, file, response) {
        var data = $.parseJSON(response.response);
        if (data.error) {
            $.gritter.add({
                title: 'Erreur',
                text: data.message,
                class_name: 'gritter-error'
            });
            hideProgessBar();
        } else {
            $.gritter.add({
                title: 'Confirmation',
                text: data.message,
                class_name: 'gritter-success'
            });
            hideProgessBar();
            $('#avatar').attr('src', '/img/avatars/' + data.newImage);
            $('.nav-user-photo').attr('src', '/img/avatars/thumbs/' + data.newImage);
        }
    });

    uploader.bind('UploadProgress', function(up, file) {
        $('#progressbar-inner').css('width', file.percent + '%');
        if (file.percent === 100) {
            $('#progressbar').attr('data-percent', 'Téléchargement terminé');
        }
        else {
            $('#progressbar').attr('data-percent', file.percent + '%');
        }
    });

    function hideProgessBar() {
        setTimeout(function() {
            $('#progressbar').slideUp();
            $('#progressbar-inner').css('width', '0%');
            $('#progressbar').attr('data-percent', '0%');
        }, 2000);
    }
});
