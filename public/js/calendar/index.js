jQuery(function($) {

    /* initialize the external events
     -----------------------------------------------------------------*/

    $('#external-events div.external-event').each(function() {

        // create an Event Object (http://arshaw.com/fullcalendar/docs/event_data/Event_Object/)
        // it doesn't need to have a start or end
        var eventObject = {
            title: $.trim($(this).text()) // use the element's text as the event title
        };

        // store the Event Object in the DOM element so we can get to it later
        $(this).data('eventObject', eventObject);

        // make the event draggable using jQuery UI
        $(this).draggable({
            zIndex: 999,
            revert: true, // will cause the event to go back to its
            revertDuration: 0  //  original position after the drag
        });

    });




    /* initialize the calendar
     -----------------------------------------------------------------*/

    var date = new Date();
    var d = date.getDate();
    var m = date.getMonth();
    var y = date.getFullYear();


    var calendar = $('#calendar').fullCalendar({
        timeFormat: {
            // for agendaWeek and agendaDay
            agenda: 'H:mm{ - H:mm}', // 15:00 - 16:30

            // for all other views
            '': 'H:mm'            // 7p
        },
        axisFormat: 'H:mm',
        monthNames: ['Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Août', 'Septembre', 'Octobre', 'Novembre', 'Decembre'],
        monthNamesShort: ['Jan', 'Fév', 'Mars', 'Avr', 'Mai', 'Juin', 'Juil', 'Août', 'Sept', 'Oct', 'Nov', 'Déc'],
        dayNames: ['Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi'],
        dayNamesShort: ['Dim', 'Lun', 'Mar', 'Mer', 'Jeu', 'Ven', 'Sam'],
        slotMinutes: 30,
        snapMinutes: 15,
        firstHour: 7,
        firstDay: 1,
        height: 600,
        allDaySlot: false,
        defaultView: 'agendaWeek',
        weekMode: 'variable',
        lazyFetching: true,
        buttonText: {
            prev: '<i class="icon-chevron-left"></i>',
            next: '<i class="icon-chevron-right"></i>',
            month: 'Mois',
            agendaWeek: 'Semaine',
            agendaDay: 'Jour',
            today: 'Aujourd\'hui'
        },
        header: {
            left: 'prev,next today',
            center: 'title',
            right: 'month,agendaWeek,agendaDay'
        },
        columnFormat: {
            month: 'ddd', // Mon
            week: 'ddd d/MM', // Mon 9/7
            day: 'dddd d MMM yyyy'  // Monday 9/7
        },
        titleFormat: {
            month: 'MMMM yyyy', // September 2009
            week: "'Du' d[ MMM][ yyyy] { 'au' d MMM yyyy}", // Sep 7 - 13 2009
            day: 'dddd d MMM yyyy'                  // Mercredi 12 Mars 2013
        },
        events: {
            url: '/calendar/ajaxCalendarFinder',
            type: 'POST',
            error: function(jq, err, test) {
                alert('there was an error while fetching events!' + err + "/" + test);
            }/*,
             color: 'yellow',
             textColor: 'black'*/
        },
        editable: true,
        droppable: true, // this allows things to be dropped onto the calendar !!!
        loading: function(bool) {
            if (bool)
                $('#calendar').append('<div class="message-loading-overlay"><div class="inner-loading-overlay"><h1 class="orange">Chargement du planning</h1><i class="icon-spin icon-spinner orange2 bigger-300"></i></div></div>');
            else
                $('.message-loading-overlay').remove();
        },
        eventDrop: function(e) {
            $('#saving-planning').removeClass('hidden');
            $.ajax({
                url: '/calendar/ajaxAddEvent',
                type: 'POST',
                data: {
                    'start': (Date.parse(e.start)) / 1000,
                    'end': (Date.parse(e.end)) / 1000,
                    'internal_id': e.internal_id,
                    'title': e.title,
                    'contract_id': e.contract_id,
                    'creator_id': e.creator_id,
                    '_id': e._id,
                    'className': e.className[0],
                    'editable': e.editable
                }
            })
                    .done(function(data) {
                        if (e.mId == null) {
                            e._id = data;
                            $('#calendar').fullCalendar('updateEvent', e);
                        }
                    })
                    .fail(function() {
                        $.gritter.add({
                            title: 'Erreur',
                            text: 'L\'évènement n\'a pas pu être modifié',
                            class_name: 'gritter-error'
                        });
                        
                    })
                    .always(function() {
                        $('#saving-planning').addClass('hidden');
                    });

        },
        eventResize: function(e) {
            $('#saving-planning').removeClass('hidden');
            $.ajax({
                url: '/calendar/ajaxAddEvent',
                type: 'POST',
                data: {
                    'start': (Date.parse(e.start)) / 1000,
                    'end': (Date.parse(e.end)) / 1000,
                    'internal_id': e.internal_id,
                    'title': e.title,
                    'contract_id': e.contract_id,
                    'creator_id': e.creator_id,
                    '_id': e._id,
                    'className': e.className[0],
                    'editable': e.editable
                }
            })
                    .done(function(data) {
                        if (e.mId == null) {
                            e._id = data;
                            $('#calendar').fullCalendar('updateEvent', e);
                        }
                    })
                    .fail(function() {
                        $.gritter.add({
                            title: 'Erreur',
                            text: 'L\'évènement n\'a pas pu être modifié',
                            class_name: 'gritter-error'
                        });
                    })
                    .always(function() {
                        $('#saving-planning').addClass('hidden');
                    });

        },
        drop: function(date, allDay) { // this function is called when something is dropped

            // retrieve the dropped element's stored Event Object
            var originalEventObject = $(this).data('eventObject');
            var $extraEventClass = $(this).attr('data-class');


            // we need to copy it, so that multiple events don't have a reference to the same object
            var copiedEventObject = $.extend({}, originalEventObject);

            // assign it the date that was reported
            var tempDate = new Date(date);  //clone date
            copiedEventObject.start = date;
            copiedEventObject.end = new Date(tempDate.setHours(tempDate.getHours() + parseInt($(this).attr('data-duration'))));
            copiedEventObject.allDay = allDay;
            copiedEventObject.contract_id = $(this).attr('data-contract-id');
            copiedEventObject.creator_id = $(this).attr('data-creator');
            copiedEventObject.class_name = $(this).attr('data-class');
            copiedEventObject.editable = true;
            copiedEventObject._id = null;
            copiedEventObject.mId = null;

            if ($extraEventClass)
                copiedEventObject['className'] = [$extraEventClass];


            $('#saving-planning').removeClass('hidden');
            $.ajax({
                url: '/calendar/ajaxAddEvent',
                type: 'POST',
                data: {
                    'start': (Date.parse(copiedEventObject.start)) / 1000,
                    'end': (Date.parse(copiedEventObject.end)) / 1000,
                    'title': copiedEventObject.title,
                    'contract_id': copiedEventObject.contract_id,
                    'creator_id': copiedEventObject.creator_id,
                    'className': copiedEventObject.className[0],
                    'editable': copiedEventObject.editable
                }
            })
                    .done(function(data) {
                        if (copiedEventObject.mId == null) {
                            copiedEventObject._id = data;
                        }
                        // render the event on the calendar
                        // the last `true` argument determines if the event "sticks" (http://arshaw.com/fullcalendar/docs/event_rendering/renderEvent/)
                        $('#calendar').fullCalendar('renderEvent', copiedEventObject, true);
                    })
                    .fail(function() {
                        $.gritter.add({
                            title: 'Erreur',
                            text: 'L\'évènement n\'a pas pu être créé',
                            class_name: 'gritter-error'
                        });
                    })
                    .always(function() {
                        $('#saving-planning').addClass('hidden');
                    });
        },
        eventRender: function(event, element, calEvent) {
        },
        eventClick: function(calEvent, jsEvent, view) {
            eventClicked = calEvent;
            console.log(view);

            var form = $("<h3>" + calEvent.title + "</h3><div>Du " + new Date(calEvent.start).toLocaleDateString() + " à " + new Date(calEvent.start).toLocaleTimeString() + "</div><div>Au " + new Date(calEvent.end).toLocaleDateString() + " à " + new Date(calEvent.end).toLocaleTimeString() + "</div>");

            var div = bootbox.dialog({
                message: form,
                buttons: {
                    "delete": {
                        "label": "<i class='icon-trash'></i> Supprimer l'événement",
                        "className": "btn-sm btn-danger",
                        "callback": function() {
                            $.ajax({
                                url: '/calendar/ajaxCalendarRemover',
                                type: 'post',
                                data: {
                                    'id': calEvent._id
                                }
                            })
                                    .done(function(response) {
                                        if (response.error) {
                                            $.gritter.add({
                                                title: 'Erreur',
                                                text: 'L\'évènement n\'a pas pu être supprimé',
                                                class_name: 'gritter-danger'
                                            });
                                        }
                                        else {
                                            calendar.fullCalendar('removeEvents', function(ev) {
                                                return (ev._id == calEvent._id);
                                            });
                                        }
                                    })
                                    .fail(function(response) {
                                    })
                                    .always(function() {
                                    });
                        }
                    },
                    "close": {
                        "label": "<i class='icon-remove'></i> Ferner",
                        "className": "btn-sm"
                    }
                }

            });

            form.on('submit', function() {
                calEvent.title = form.find("input[type=text]").val();
                calendar.fullCalendar('updateEvent', calEvent);
                div.modal("hide");
                return false;
            });


            //console.log(calEvent.id);
            //console.log(jsEvent);
            //console.log(view);

            // change the border color just for fun
            //$(this).css('border-color', 'red');

        }

    });

    $(document).on('click', '#unlock-event', function() {
        eventClicked.editable = true;
        $('#calendar').fullCalendar('updateEvent', eventClicked);
        $.ajax({
            url: '/calendar/ajaxCalendarLockerUnlocker',
            type: 'POST',
            data: {
                'start': (Date.parse(eventClicked.start)) / 1000,
                'end': (Date.parse(eventClicked.end)) / 1000,
                'internal_id': eventClicked.internal_id,
                'title': eventClicked.title,
                'contract_id': eventClicked.contract_id,
                'creator_id': eventClicked.creator_id,
                '_id': eventClicked._id,
                'className': eventClicked.class_name,
                'editable': eventClicked.editable
            }
        })
                .done(function(response) {
                    if (!response.error) {
                        $.gritter.add({
                            title: 'Confirmation',
                            text: 'L\'évènement à bien été dévérrouillé',
                            class_name: 'gritter-success'
                        });
                    }
                    else {
                        $.gritter.add({
                            title: 'erreur',
                            text: 'Une erreur est survenue. Veuillez recommencer.',
                            class_name: 'gritter-error'
                        });
                    }
                })
                .fail(function() {
                    $.gritter.add({
                        title: 'erreur',
                        text: 'Une erreur est survenue. Veuillez recommencer.',
                        class_name: 'gritter-error'
                    });
                })
                .always(function() {
                });
    });

    $(document).on('click', '#lock-event', function() {
        eventClicked.editable = false;
        $('#calendar').fullCalendar('updateEvent', eventClicked);
        $.ajax({
            url: '/calendar/ajaxCalendarLockerUnlocker',
            type: 'POST',
            data: {
                'start': (Date.parse(eventClicked.start)) / 1000,
                'end': (Date.parse(eventClicked.end)) / 1000,
                'internal_id': eventClicked.internal_id,
                'title': eventClicked.title,
                'contract_id': eventClicked.contract_id,
                'creator_id': eventClicked.creator_id,
                '_id': eventClicked._id,
                'className': eventClicked.class_name,
                'editable': eventClicked.editable
            }
        })
                .done(function(response) {
                    if (!response.error) {
                        $.gritter.add({
                            title: 'Confirmation',
                            text: 'L\'évènement à bien été vérrouillé',
                            class_name: 'gritter-success'
                        });
                    }
                    else {
                        $.gritter.add({
                            title: 'erreur',
                            text: 'Une erreur est survenue. Veuillez recommencer.',
                            class_name: 'gritter-error'
                        });
                    }
                })
                .fail(function() {
                    $.gritter.add({
                        title: 'erreur',
                        text: 'Une erreur est survenue. Veuillez recommencer.',
                        class_name: 'gritter-error'
                    });
                })
                .always(function() {
                });
    });

    $(document).on('click', '#remove-event', function() {

        var eventId = $(this).attr('data-id');
        bootbox.dialog({
            message: "Etes vous sûr de vouloir supprimer cet évènement?",
            buttons: {
                "close": {
                    "label": "<i class='icon-remove'></i> Annuler",
                    "className": "btn-sm"
                },
                "delete": {
                    "label": "<i class='icon-trash'></i> Supprimer",
                    "className": "btn-sm btn-danger",
                    "callback": function() {
                        $('#calendar').append('<div class="message-loading-overlay"><div class="inner-loading-overlay"><h1 class="orange">Suppression de l\'évènement</h1><i class="icon-spin icon-spinner orange2 bigger-300"></i></div></div>');

                        $.ajax({
                            url: '/calendar/ajaxCalendarRemover',
                            type: 'post',
                            data: {
                                'id': eventId
                            }
                        })
                                .done(function(response) {
                                    if (!response.error) {
                                        $.gritter.add({
                                            title: 'Confirmation',
                                            text: 'L\'évènement à bien été supprimé',
                                            class_name: 'gritter-success'
                                        });
                                        $('#calendar').fullCalendar('removeEvents', function(ev) {
                                            return (ev._id == eventId);
                                        });
                                    }
                                    else {
                                        $.gritter.add({
                                            title: 'erreur',
                                            text: 'Une erreur est survenue. Veuillez recommencer.',
                                            class_name: 'gritter-error'
                                        });
                                    }
                                })
                                .fail(function(response) {
                                    $.gritter.add({
                                        title: 'erreur',
                                        text: 'Une erreur est survenue. Veuillez recommencer.',
                                        class_name: 'gritter-error'
                                    });
                                })
                                .always(function() {
                                    $('.message-loading-overlay').remove();
                                    console.log("complete");
                                });
                    }
                }
            }

        });
    });
});