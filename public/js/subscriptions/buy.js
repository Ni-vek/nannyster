jQuery(function($) {

    $('#credit-card-payment').validate({
        errorElement: 'div',
        errorClass: 'help-block',
        focusInvalid: false,
        rules: {
            type: {
                required: true
            },
            number: {
                required: true,
                creditcard: true
            },
            cvv2: {
                required: true,
                minlength: 3,
                maxlength: 3
            },
            firstname: {
                required: true
            },
            surname: {
                required: true
            },
            expire_month: {
                required: true
            },
            expire_year: {
                required: true
            }
        },
        messages: {
            type: {
                required: 'Type de carte non renseigné'
            },
            number: {
                required: 'Numéro de carte non renseigné',
                creditcard: 'Numéro de carte non valide'
            },
            expire_month: {
                required: 'Mois d\'expiration non renseigné'
            },
            expire_year: {
                required: 'Année d\'expiration non renseigné'
            },
            cvv2: {
                required: 'Cryptogramme visuel non renseigné',
                minlength: 'Cryptogramme visuel ne doit contenir que 3 chiffres',
                maxlength: 'Cryptogramme visuel ne doit contenir que 3 chiffres'
            },
            firstname: {
                required: 'Prénom non renseigné'
            },
            surname: {
                required: 'Nom non renseigné'
            }
        },
        highlight: function(e) {

        },
        success: function(e) {

        },
        errorPlacement: function(error, element) {

        },
        submitHandler: function(form) {
            form.submit();
            bootbox.dialog({
                message: '<div id="loader-wrapper" class="center"><h3>Veuillez patienter. Nous procédons au paiement.</h3><div>Le traitement peut prendre jusqu\'à 1 minute</div><div id="loader"></div></div>',
                closeButton: false
            });
        },
        invalidHandler: function(event, validator) {
            console.log(event);
            console.log(validator);
            var errors = "<ul>";
            for (var errorFromList in validator.errorList){
                errors += "<li>" + validator.errorList[errorFromList].message + "</li>";
            }
            errors += "</ul>";
            $.gritter.add({
                title: 'Erreur',
                text: errors,
                class_name: 'gritter-error'
            });
        }
    });

});