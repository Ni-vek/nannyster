jQuery(function($) {
    var oTable1 = $('#invoices-table').dataTable({
        "aoColumns": [
            {"bSortable": false},
            null, null,
            {"bSortable": false}
        ]});


    $('table th input:checkbox').on('click', function() {
        var that = this;
        $(this).closest('table').find('tr > td:first-child input:checkbox')
                .each(function() {
                    this.checked = that.checked;
                    $(this).closest('tr').toggleClass('selected');
                });

    });
});