jQuery(function($) {

//Chosen to create an anccount for the mother
    $('#mother_city_id_select').ajaxChosen({
        dataType: 'json',
        type: 'POST',
        url: '/cities/ajaxCitiesFinder',
        //data: {'keyboard': 'cat'}, //Or can be [{'name':'keyboard', 'value':'cat'}]. chose your favorite, it handles both.
        success: function(data, textStatus, jqXHR) {
//            doSomething();
        }
    }, {
        processItems: function(data) {
            return data.results;
        },
//        useAjax: function(e) {
//            return true;
//        },
//        generateUrl: function(q) {
//            return '/cities/ajaxCitiesFinder';
//        },
        loadingImg: '/img/loading.gif',
        minLength: 2
    });
    $('#father_city_id_select').ajaxChosen({
        dataType: 'json',
        type: 'POST',
        url: '/cities/ajaxCitiesFinder',
        //data: {'keyboard': 'cat'}, //Or can be [{'name':'keyboard', 'value':'cat'}]. chose your favorite, it handles both.
        success: function(data, textStatus, jqXHR) {
//            doSomething();
        }
    }, {
        processItems: function(data) {
            return data.results;
        },
//        useAjax: function(e) {
//            return true;
//        },
//        generateUrl: function(q) {
//            return '/cities/ajaxCitiesFinder';
//        },
        loadingImg: '/img/loading.gif',
        minLength: 2
    });

    function getUserHtmlTable(user) {
        console.log(user);
        var html = "<div class=\"row\">" +
                "<div class=\"col-xs-12 col-sm-3 center\">" +
                "<span class=\"profile-picture\">" +
                "<img class=\"img-responsive\" alt=\"" + user.firstname + " " + user.surname + "\" src=\"/img/avatars/" + user.avatar_file + "\">" +
                "</span>" +
                "</div>" +
                "<div class=\"col-xs-12 col-sm-9\">" +
                "<div class=\"profile-user-info\">" +
                "<div class=\"profile-info-row\">" +
                "<div class=\"contract-profile-info-name\">" +
                "Prénom NOM" +
                "</div>" +
                "<div class=\"contract-profile-info-value\">" +
                "<span>" +
                user.firstname + " " + user.surname +
                "</span>" +
                "</div>" +
                "</div>" +
                "<div class=\"profile-info-row\">" +
                "<div class=\"contract-profile-info-name\">" +
                "Adresse" +
                "</div>" +
                "<div class=\"contract-profile-info-value\">" +
                "<i class=\"icon-map-marker light-orange bigger-110\"></i> " +
                "<span>" +
                user.address.address_1 + ",<br/>" + user.address.city.zip_code + ", " + user.address.city.real_name +
                "</span>" +
                "</div>" +
                "</div>" +
                "<div class=\"profile-info-row\">" +
                "<div class=\"contract-profile-info-name\">" +
                "Téléphone" +
                "</div>" +
                "<div class=\"contract-profile-info-value\">" +
                "<span>" +
                user.phone +
                "</span>" +
                "</div>" +
                "</div>" +
                "<div class=\"profile-info-row\">" +
                "<div class=\"contract-profile-info-name\">" +
                "Mobile" +
                "</div>" +
                "<div class=\"contract-profile-info-value\">" +
                "<span>" +
                user.mobile +
                "</span>" +
                "</div>" +
                "</div>" +
                "<div class=\"profile-info-row\">" +
                "<div class=\"contract-profile-info-name\">" +
                "Email" +
                "</div>" +
                "<div class=\"contract-profile-info-value\">" +
                "<span>" +
                user.email +
                "</span>" +
                "</div>" +
                "</div>" +
                "</div>" +
                "</div>" +
                "</div>";
        return html;
    }

    $('#mother-form-account-cretor').validate({
        errorElement: 'div',
        errorClass: 'help-block',
        focusInvalid: false,
        rules: {
            email: {
                required: true,
                email: true,
                remote: {
                    url: "/users/ajaxCheckUserMail",
                    type: "post",
                    complete: function(data) {
                        if (data.responseText !== "true") {
                            $('#mother-modal-form').modal('hide');
                            var mother_html = getUserHtmlTable(data.responseJSON);
                            bootbox.dialog({
                                message: "<h3 class=\"error\">Un compte Ma Nourrice existe déjà avec l'email " + data.responseJSON.email + "</h3>" + mother_html,
                                buttons:
                                        {
                                            "button":
                                                    {
                                                        "label": "<i class='icon-remove'></i> Ne pas importer",
                                                        "className": "btn-sm",
                                                        callback: function() {
                                                            $('#mother-modal-form').modal('show');
                                                        }
                                                    },
                                            "success":
                                                    {
                                                        "label": "<i class='icon-ok'></i> Importer ce compte!",
                                                        "className": "btn-sm btn-success",
                                                        "callback": function() {
                                                            if (data.responseJSON.civility.short_name === 'Mme') {
                                                                $('#mother_id').val(data.responseJSON._id.$id);
                                                                $('#mother_infos').html(mother_html).removeClass('hidden');
                                                                $('#mother_actions').addClass('hidden');
                                                                $('#delete-mother').removeClass('hidden');
                                                                $('#mother-modal-form').modal('hide');
                                                            }
                                                            else {
                                                                alert('Vous ne pouvez pas importer la fiche d\'un homme pour la mère!');
                                                            }
                                                        }
                                                    }
                                        }
                            });

                        }
                    }
                }
            },
            surname: {
                required: true
            },
            firstname: {
                required: true
            },
            address: {
                required: true
            },
            city: {
                required: true
            },
            phone: {
                required: true
            },
            mobile: {
                required: true
            }
        },
        messages: {
            email: {
                required: "Veuillez renseigner un email valide.",
                email: "Veuillez renseigner un email valide."
            },
            surname: {
                required: "Ce champ est requis"
            },
            firstname: {
                required: "Ce champ est requis"
            },
            address: {
                required: "Ce champ est requis"
            },
            city: {
                required: "Ce champ est requis"
            },
            phone: {
                required: "Ce champ est requis"
            },
            mobile: {
                required: "Ce champ est requis"
            }
        },
        highlight: function(e) {
            $(e).closest('.form-group').removeClass('has-info').addClass('has-error');
        },
        success: function(e) {
            $(e).closest('.form-group').removeClass('has-error').addClass('has-info');
            $(e).remove();
        },
        errorPlacement: function(error, element) {
            if (element.is(':checkbox') || element.is(':radio')) {
                var controls = element.closest('div[class*="col-"]');
                if (controls.find(':checkbox,:radio').length > 1)
                    controls.append(error);
                else
                    error.insertAfter(element.nextAll('.lbl:eq(0)').eq(0));
            }
            else if (element.is('.select2')) {
                error.insertAfter(element.siblings('[class*="select2-container"]:eq(0)'));
            }
            else if (element.is('.chosen-select')) {
                error.insertAfter(element.siblings('[class*="chosen-container"]:eq(0)'));
            }
            else
                error.insertAfter(element.parent());
        },
        submitHandler: function(form) {
            var datas = $('#mother-form-account-cretor').serializeArray();
            $('#submit-new-mother-account').prop("disabled", true).html('<i class="icon-refresh icon-spin"></i> Création du compte...');
            $.ajax({
                url: '/users/ajaxCreateUser',
                type: 'post',
                data: datas,
                dataType: 'json'
            })
                    .done(function(data) {
                        var html = getUserHtmlTable(data);
                        $('#mother_id').val(data._id.$id);
                        $('#mother_infos').html(html).removeClass('hidden');
                        $('#mother_actions').addClass('hidden');
                        $('#delete-mother').removeClass('hidden');
                        $('#mother-modal-form').modal('hide');
                    })
                    .fail(function() {
                        $.gritter.add({
                            title: 'Erreur',
                            text: 'Une erreur est survenue lors de la création de l\'utilisateur. Veuillez recommencer.',
                            class_name: 'gritter-error'
                        });
                    })
                    .always(function(){
                        $('#submit-new-mother-account').prop("disabled", false).html('<i class="icon-ok"></i> Créer le compte');
                    });
        },
        invalidHandler: function(form) {
        }
    });

    $('#father-form-account-cretor').validate({
        errorElement: 'div',
        errorClass: 'help-block',
        focusInvalid: false,
        rules: {
            email: {
                required: true,
                email: true,
                remote: {
                    url: "/users/ajaxCheckUserMail",
                    type: "post",
                    complete: function(data) {
                        if (data.responseText !== "true") {
                            $('#father-modal-form').modal('hide');
                            var father_html = getUserHtmlTable(data.responseJSON);
                            bootbox.dialog({
                                message: "<h3 class=\"error\">Un compte Ma Nourrice existe déjà avec l'email " + data.responseJSON.email + "</h3>" + father_html,
                                buttons:
                                        {
                                            "button":
                                                    {
                                                        "label": "<i class='icon-remove'></i> Ne pas importer",
                                                        "className": "btn-sm",
                                                        callback: function() {
                                                            $('#father-modal-form').modal('show');
                                                        }
                                                    },
                                            "success":
                                                    {
                                                        "label": "<i class='icon-ok'></i> Importer ce compte!",
                                                        "className": "btn-sm btn-success",
                                                        "callback": function() {
                                                            if (data.responseJSON.civility.short_name === 'Mme') {
                                                                $('#father_id').val(data.responseJSON._id.$id);
                                                                $('#father_infos').html(father_html).removeClass('hidden');
                                                                $('#father_actions').addClass('hidden');
                                                                $('#delete-father').removeClass('hidden');
                                                                $('#father-modal-form').modal('hide');
                                                            }
                                                            else {
                                                                alert('Vous ne pouvez pas importer la fiche d\'un homme pour la mère!');
                                                            }
                                                        }
                                                    }
                                        }
                            });

                        }
                    }
                }
            },
            surname: {
                required: true
            },
            firstname: {
                required: true
            },
            address: {
                required: true
            },
            city: {
                required: true
            },
            phone: {
                required: true
            },
            mobile: {
                required: true
            }
        },
        messages: {
            email: {
                required: "Veuillez renseigner un email valide.",
                email: "Veuillez renseigner un email valide."
            },
            surname: {
                required: "Ce champ est requis"
            },
            firstname: {
                required: "Ce champ est requis"
            },
            address: {
                required: "Ce champ est requis"
            },
            city: {
                required: "Ce champ est requis"
            },
            phone: {
                required: "Ce champ est requis"
            },
            mobile: {
                required: "Ce champ est requis"
            }
        },
        highlight: function(e) {
            $(e).closest('.form-group').removeClass('has-info').addClass('has-error');
        },
        success: function(e) {
            $(e).closest('.form-group').removeClass('has-error');//.addClass('has-info');
            $(e).remove();
        },
        errorPlacement: function(error, element) {
            if (element.is(':checkbox') || element.is(':radio')) {
                var controls = element.closest('div[class*="col-"]');
                if (controls.find(':checkbox,:radio').length > 1)
                    controls.append(error);
                else
                    error.insertAfter(element.nextAll('.lbl:eq(0)').eq(0));
            }
            else if (element.is('.select2')) {
                error.insertAfter(element.siblings('[class*="select2-container"]:eq(0)'));
            }
            else if (element.is('.chosen-select')) {
                error.insertAfter(element.siblings('[class*="chosen-container"]:eq(0)'));
            }
            else
                error.insertAfter(element.parent());
        },
        submitHandler: function(form) {
            var datas = $('#father-form-account-cretor').serializeArray();
            $('#submit-new-father-account').prop("disabled", true).html('<i class="icon-refresh icon-spin"></i> Création du compte...');
            $.ajax({
                url: '/users/ajaxCreateUser',
                type: 'post',
                data: datas,
                dataType: 'json'
            })
                    .done(function(data) {
                        var html = getUserHtmlTable(data);
                        $('#father_id').val(data._id.$id);
                        $('#father_infos').html(html).removeClass('hidden');
                        $('#father_actions').addClass('hidden');
                        $('#delete-father').removeClass('hidden');
                        $('#father-modal-form').modal('hide');
                    })
                    .fail(function() {
                        $.gritter.add({
                            title: 'Erreur',
                            text: 'Une erreur est survenue lors de la création de l\'utilisateur. Veuillez recommencer.',
                            class_name: 'gritter-error'
                        });
                    })
                    .always(function(){
                        $('#submit-new-father-account').prop("disabled", false).html('<i class="icon-ok"></i> Créer le compte');
                    });
        },
        invalidHandler: function(form) {
        }
    });


    //Phone and mobile mask
    $('#mother_phone, #mother_mobile, #father_phone, #father_mobile').mask('99 99 99 99 99');

    $('#difficulty_maj_value').val(($('#salaire_horaire_brut').val() * $('#difficulty_maj_rate').val() / 100).toFixed(2));
    $('#difficulty_maj_rate, #salaire_horaire_brut').keyup(function() {
        if ($('#difficulty_maj_rate').val() != '' && $('#difficulty_maj_rate').val() != null && $('#difficulty_maj_rate').val() != undefined) {
            $('#difficulty_maj_value').val(($('#salaire_horaire_brut').val() * $('#difficulty_maj_rate').val() / 100).toFixed(2));
        }
        else {
            $('#difficulty_maj_value').val('0.00');
        }
    });
    $('#salaire_horaire_brut').keyup(function() {
        var salaireBase = $('#salaire_horaire_brut').val();
        var salaireNet = salaireBase;
        $.each(cotisJson, function() {
            salaireNet -= ((salaireBase * this.base / 100) * this.taux / 100).toFixed(2);
        });
        $('#salaire_horaire_net').val(salaireNet.toFixed(2));
    });
    $('#hours_maj_value').val(($('#salaire_horaire_brut').val() * $('#hours_maj_rate').val() / 100).toFixed(2));
    $('#hours_maj_rate, #salaire_horaire_brut').keyup(function() {
        if ($('#hours_maj_rate').val() != '' && $('#hours_maj_rate').val() != null && $('#hours_maj_rate').val() != undefined) {
            $('#hours_maj_value').val(($('#salaire_horaire_brut').val() * $('#hours_maj_rate').val() / 100).toFixed(2));
        }
        else {
            $('#hours_maj_value').val('0.00');
        }
    });
    $('input[name="guard_type"]').click(function() {
        updateSalary(false);
    });
    $('#salaire_horaire_brut, #nb_hour_weekly').keyup(function() {
        updateSalary(false);
    });
    $('#partial_year_week_nb, #occasional_childcare_monthly_hours').keyup(function() {
        updateSalary(true);
    });
    $('#addVacancyPeriod').click(function(event) {
        event.preventDefault();
        $(this).before('<label class="control-label col-xs-12 col-sm-3 no-padding-right" for="fractionnement_conges_du_2">Du:</label><div class="col-xs-12 col-sm-3"><input type="text" id="fractionnement_conges_du_2" name="fractionnement_conges_du_2"></div><label class="control-label col-xs-12 col-sm-1 no-padding-right" for="fractionnement_conges_au_2">Au:</label><div class="col-xs-12 col-sm-3"><input type="text" id="fractionnement_conges_au_2" name="fractionnement_conges_au_2"></div>');
        $(this).remove();
        $('#fractionnement_conges_du, #fractionnement_conges_au').each(function() {
            $(this).mask('99/99/9999');
        });
    });
    $('#repas-fournis-ass-mat').click(function() {
        $('input#switch-field-repas').each(function() {
            $(this).prop('disabled', false);
        });
    });
    $('#repas-fournis-parents').click(function() {
        $('input#switch-field-repas').each(function() {
            $(this).prop('checked', false);
            $(this).prop('disabled', true);
        });
        $('#cout-petit-dejeuner, #cout-dejeuner, #cout-gouter, #cout-diner').val('');
        $('#cout-petit-dejeuner, #cout-dejeuner, #cout-gouter, #cout-diner').prop('disabled', true);
    });
    $('input#switch-field-repas').click(function() {
        if ($(this).is(':checked')) {
            $('#' + $(this).attr('data-target')).prop('disabled', false).val($(this).attr('data-value-target'));
        }
        else {
            $('#' + $(this).attr('data-target')).prop('disabled', true);
            $('#' + $(this).attr('data-target')).val('');
        }
    });
    $('[data-rel=tooltip]').tooltip();

    $('#contract-ajax-form-part-1').validate({
        errorElement: 'div',
        errorClass: 'help-block',
        focusInvalid: false,
        rules: {
            child_surname: {
                required: true
            },
            child_firstname: {
                required: true
            },
            child_birth_date: {
                required: true
            },
            contract_start_date: {
                required: true
            }
        },
        messages: {
            child_surname: {
                required: "Ce champ est requis"
            },
            child_firstname: {
                required: "Ce champ est requis"
            },
            child_birth_date: {
                required: "Ce champ est requis"
            },
            contract_start_date: {
                required: "Ce champ est requis"
            }
        },
        highlight: function(e) {
            $(e).closest('.form-group').removeClass('has-info').addClass('has-error');
        },
        success: function(e) {
            $(e).closest('.form-group').removeClass('has-error').addClass('has-info');
            $(e).remove();
        },
        errorPlacement: function(error, element) {
            if (element.is(':checkbox') || element.is(':radio')) {
                var controls = element.closest('div[class*="col-"]');
                if (controls.find(':checkbox,:radio').length > 1)
                    controls.append(error);
                else
                    error.insertAfter(element.nextAll('.lbl:eq(0)').eq(0));
            }
            else if (element.is('.select2')) {
                error.insertAfter(element.siblings('[class*="select2-container"]:eq(0)'));
            }
            else if (element.is('.chosen-select')) {
                error.insertAfter(element.siblings('[class*="chosen-container"]:eq(0)'));
            }
            else
                error.insertAfter(element.parent());
        },
        submitHandler: function(form) {
        },
        invalidHandler: function(form) {
        }
    });

    $('#contract-ajax-form-part-3').validate({
        errorElement: 'div',
        errorClass: 'help-block',
        focusInvalid: false,
        rules: {
            guard_type: {
                required: true
            },
            nb_hour_weekly: {
                required: true
            }
        },
        messages: {
            guard_type: {
                required: "Ce champ est requis"
            },
            nb_hour_weekly: {
                required: "Ce champ est requis"
            }
        },
        highlight: function(e) {
            $(e).closest('.form-group').removeClass('has-info').addClass('has-error');
        },
        success: function(e) {
            $(e).closest('.form-group').removeClass('has-error').addClass('has-info');
            $(e).remove();
        },
        errorPlacement: function(error, element) {
            if (element.is(':checkbox') || element.is(':radio')) {
                var controls = element.closest('div[class*="col-"]');
                if (controls.find(':checkbox,:radio').length > 1)
                    controls.append(error);
                else
                    error.insertAfter(element.nextAll('.lbl:eq(0)').eq(0));
            }
            else if (element.is('.select2')) {
                error.insertAfter(element.siblings('[class*="select2-container"]:eq(0)'));
            }
            else if (element.is('.chosen-select')) {
                error.insertAfter(element.siblings('[class*="chosen-container"]:eq(0)'));
            }
            else
                error.insertAfter(element.parent());
        },
        submitHandler: function(form) {
        },
        invalidHandler: function(form) {
        }
    });
    
    $('#contract-ajax-form-part-4').validate({
        errorElement: 'div',
        errorClass: 'help-block',
        focusInvalid: false,
        rules: {
            hourly_rate: {
                required: true
            },
            maintenance_allowance: {
                required: true
            },
            meal_given: {
                required: true
            }
        },
        messages: {
            hourly_rate: {
                required: "Ce champ est requis"
            },
            maintenance_allowance: {
                required: "Ce champ est requis"
            },
            meal_given: {
                required: "Ce champ est requis"
            }
        },
        highlight: function(e) {
            $(e).closest('.form-group').removeClass('has-info').addClass('has-error');
        },
        success: function(e) {
            $(e).closest('.form-group').removeClass('has-error').addClass('has-info');
            $(e).remove();
        },
        errorPlacement: function(error, element) {
            if (element.is(':checkbox') || element.is(':radio')) {
                var controls = element.closest('div[class*="col-"]');
                if (controls.find(':checkbox,:radio').length > 1)
                    controls.append(error);
                else
                    error.insertAfter(element.nextAll('.lbl:eq(0)').eq(0));
            }
            else if (element.is('.select2')) {
                error.insertAfter(element.siblings('[class*="select2-container"]:eq(0)'));
            }
            else if (element.is('.chosen-select')) {
                error.insertAfter(element.siblings('[class*="chosen-container"]:eq(0)'));
            }
            else
                error.insertAfter(element.parent());
        },
        submitHandler: function(form) {
        },
        invalidHandler: function(form) {
        }
    });

    $('#fuelux-wizard').ace_wizard()
            .on('change', function(e, info) {
                $('html, body').animate({scrollTop: 0}, 'slow');
                switch (info.step) {
                    case 1:
                        if (!$('#contract-ajax-form-part-1').valid()) {
                            e.preventDefault();
                            if ($('#mother_id').val() === '' && $('#father_id').val() === '') {
                                $('#alert-parents').removeClass('hidden');
                            }
                            $.gritter.add({
                                title: 'Champs non renseignés',
                                text: 'Certains champs obligatoires n\'ont pas été renseignés.',
                                class_name: 'gritter-error'
                            });
                            return false;
                        }
                        else {
                            if ($('#mother_id').val() === '' && $('#father_id').val() === '') {
                                $('#alert-parents').removeClass('hidden');
                                e.preventDefault();
                                $.gritter.add({
                                    title: 'Champs non renseignés',
                                    text: 'Certains champs obligatoires n\'ont pas été renseignés.',
                                    class_name: 'gritter-error'
                                });
                            return false;
                            }
                            else {
                                if (!$('#alert-parents').hasClass('hidden')) {
                                    $('#alert-parents').addClass('hidden');
                                }
                            }
                        }
                        break;
                    case 2:
                        if (!$('#contract-ajax-form-part-2').valid()) {
                            e.preventDefault();
                            $.gritter.add({
                                title: 'Champs non renseignés',
                                text: 'Certains champs obligatoires n\'ont pas été renseignés.',
                                class_name: 'gritter-error'
                            });
                            return false;
                        }
                        break;
                    case 3:
                        if (!$('#contract-ajax-form-part-3').valid()) {
                            e.preventDefault();
                            $.gritter.add({
                                title: 'Champs non renseignés',
                                text: 'Certains champs obligatoires n\'ont pas été renseignés.',
                                class_name: 'gritter-error'
                            });
                            return false;
                        }
                        break;
                    case 4:
                        if (!$('#contract-ajax-form-part-4').valid()) {
                            e.preventDefault();
                            $.gritter.add({
                                title: 'Champs non renseignés',
                                text: 'Certains champs obligatoires n\'ont pas été renseignés.',
                                class_name: 'gritter-error'
                            });
                            return false;
                        }
                        break;
                    case 5:
                        if (!$('#contract-ajax-form-part-5').valid()) {
                            e.preventDefault();
                            $.gritter.add({
                                title: 'Champs non renseignés',
                                text: 'Certains champs obligatoires n\'ont pas été renseignés.',
                                class_name: 'gritter-error'
                            });
                            return false;
                        }
                        break;
                }
                var datas = $('#contract-ajax-form-part-1, #contract-ajax-form-part-2, #contract-ajax-form-part-3, #contract-ajax-form-part-4, #contract-ajax-form-part-5').serializeArray();
                $('.wysiwyg-editor').each(function() {
                    datas.push({name: $(this).attr('id'), value: $(this).html()});
                });
                datas.push({name: 'valide', value: 'false'});
                datas.push({name: 'salaire_mensuel_brut', value: $('strong#salaire-mensu').html()});
                $.ajax({
                    url: '/contracts/ajaxCreate',
                    type: 'post',
                    data: datas
                })
                        .done(function(data) {
                            $('#contract_id').val(data);
                        });
            })
            .on('finished', function(e) {
                bootbox.confirm("<strong>Félicitations</strong><br/><br/>Votre nouveau contrat est terminé mais pas encore validé. Si vous voulez corriger une partie cliquez sur 'Annuler'. Sinon vous pouvez cliquer sur 'Valider' pour valider votre nouveau contrat.", function(confirmed) {
                    if (confirmed) {
                        var datas = $('#contract-ajax-form-part-1, #contract-ajax-form-part-2, #contract-ajax-form-part-3, #contract-ajax-form-part-4, #contract-ajax-form-part-5').serializeArray();
                        $('.wysiwyg-editor').each(function() {
                            datas.push({name: $(this).attr('id'), value: $(this).html()});
                        });
                        datas.push({name: 'valide', value: 'true'});
                        datas.push({name: 'salaire_mensuel_brut', value: $('strong#salaire-mensu').html()});
                        $.ajax({
                            url: '/contracts/ajaxCreate',
                            type: 'post',
                            data: datas
                        })
                                .done(function(data) {
                                    window.location.replace('/contracts/index');
                                })
                                .fail(function() {
                                    $.gritter.add({
                                        title: 'Erreur',
                                        text: 'Une erreur est survenue lors de la finalisation de votre contrat. Veuillez recommencer.',
                                        class_name: 'gritter-error'
                                    });
                                });
                    }
                });
            })
            .on('stepclick', function(e) {
                //return false;//prevent clicking on steps
            });
    //documentation : http://docs.jquery.com/Plugins/Validation/validate




    $('.wysiwyg-editor').each(function() {
        $(this).ace_wysiwyg({
            toolbar:
                    [
                        'bold',
                        'italic',
                        'strikethrough',
                        'underline',
                        null,
                        'justifyleft',
                        'justifycenter',
                        'justifyright',
                        null,
                        'createLink',
                        'unlink',
                        null,
                        'undo',
                        'redo'
                    ]
        });
    });
    $('.wysiwyg-editor').each(function() {
        $(this).slimScroll({
            height: '260px',
            railVisible: true
        });
    });
    //$('#nb_hour_weekly').ace_spinner({value:45,min:0,step:1, btn_up_class:'btn-info' , btn_down_class:'btn-info'});


    /*jQuery.validator.addMethod("phone", function (value, element) {
     return this.optional(element) || /^\(\d{3}\) \d{3}\-\d{4}( x\d{1,6})?$/.test(value);
     }, "Enter a valid phone number.");
     
     jQuery.validator.addMethod("datefr", function (value, element) {
     return this.optional(element) || /^(0[1-9]|[1-2][0-9]|3[01])-(0[1-9]|1[0-2])-([12][09][019][0-9])$/.test(value);
     }, "Veuillez entrer un date au format dd-mm-yyyy.");
     
     $('#validation-form').validate({
     errorElement: 'div',
     errorClass: 'help-block',
     focusInvalid: false,
     rules: {
     child_surname: {
     required: true
     },
     child_firstname: {
     required: true
     },
     child_birth_date: {
     datefr: true
     },
     mother_surname: {
     required: true
     },
     mother_firstname: {
     required: true
     },
     mother_email: {
     email: true
     }
     },
     
     messages: {
     child_surname: "Le nom de l'enfant est obligatoire.",
     child_firstname: "Le prénom de l'enfant est obligatoire.",
     mother_surname: "Le nom de la mère est obligatoire.",
     mother_firstname: "Le prénom de la mère est obligatoire.",
     mother_email: {
     email: "Merci de renseigner un email valide."
     }
     },
     
     highlight: function (e) {
     $(e).closest('.form-group').removeClass('has-info').addClass('has-error');
     },
     
     success: function (e) {
     $(e).closest('.form-group').removeClass('has-error').addClass('has-info');
     $(e).remove();
     },
     
     errorPlacement: function (error, element) {
     if(element.is(':checkbox') || element.is(':radio')) {
     var controls = element.closest('div[class*="col-"]');
     if(controls.find(':checkbox,:radio').length > 1) controls.append(error);
     else error.insertAfter(element.nextAll('.lbl:eq(0)').eq(0));
     }
     else if(element.is('.select2')) {
     error.insertAfter(element.siblings('[class*="select2-container"]:eq(0)'));
     }
     else if(element.is('.chosen-select')) {
     error.insertAfter(element.siblings('[class*="chosen-container"]:eq(0)'));
     }
     else error.insertAfter(element.parent());
     },
     
     submitHandler: function (form) {
     },
     invalidHandler: function (form) {
     }
     });*/

    $('#mother_phone_house, #mother_phone_work, #mother_phone_mobile, #father_phone_house, #father_phone_work, #father_phone_mobile').mask('99 99 99 99 99');
    $('#fractionnement_conges_du, #fractionnement_conges_au, #date_debut_contrat, #child_birth_date').mask('?99/99/9999');
    $("#import-mother").on(ace.click_event, function(event) {
        event.preventDefault();
        bootbox.dialog({
            title: '<span class="blue">Importer une fiche Ma Nourrice</span>',
            message: "<form class=\"form-horizontal\"><div class=\"form-group\"><label class=\"col-xs-12 col-sm-6 no-padding-right\" for=\"mother_search_surname\">Veuillez saisir le nom de la mère:</label><div class=\"col-xs-12 col-sm-5\"><div class=\"input-icon clearfix\"><input autocomplete=\"off\" type=\"text\" id=\"mother_search_surname\" name=\"mother_search_surname\" class=\"col-xs-12 col-sm-12\" /><i class=\"icon-user\"></i><div id=\"user-find-results-import\" class=\"hidden user-find-results-import\"></div></div><div class=\"hidden image-loading-mail\" id=\"loading-users-results\"><i class=\"icon-spin icon-spinner orange2 bigger-160\"></i></div></div></form><script type=\"text/javascript\">motherImportUser();motherFct();motherClick();</script>",
            buttons:
                    {
                        "success":
                                {
                                    "label": "<i class='icon-ok'></i> Terminé!",
                                    "className": "btn-sm btn-success",
                                    "callback": function() {
                                        //Example.show("great success");
                                    }
                                }
                    }
        });
    });
    $("#import-father").on(ace.click_event, function(event) {
        event.preventDefault();
        bootbox.dialog({
            title: 'Importer une fiche Ma Nourrice',
            message: "<form class=\"form-horizontal\"><div class=\"form-group\"><label class=\"col-xs-12 col-sm-6 no-padding-right\" for=\"father_search_surname\">Veuillez saisir le nom du père:</label><div class=\"col-xs-12 col-sm-5\"><div class=\"input-icon clearfix\"><input autocomplete=\"off\" type=\"text\" id=\"father_search_surname\" name=\"father_search_surname\" class=\"col-xs-12 col-sm-12\" autofocus /><i class=\"icon-user\"></i><div id=\"user-find-results-import\" class=\"hidden user-find-results-import\"></div></div><div class=\"hidden image-loading-mail\" id=\"loading-users-results\"><i class=\"icon-spin icon-spinner orange2 bigger-160\"></i></div></div></form><script type=\"text/javascript\">fatherImportUser();fatherFct();fatherClick()</script>",
            buttons:
                    {
                        "success":
                                {
                                    "label": "<i class='icon-ok'></i> Terminé!",
                                    "className": "btn-sm btn-success",
                                    "callback": function() {
                                        //Example.show("great success");
                                    }
                                }
                    }
        });
    });
    $('#delete-mother').click(function(event) {
        event.preventDefault();
        $('#mother_id').val('');
        $('#mother_infos').empty().addClass('hidden');
        $('#mother_actions').removeClass('hidden');
        $('#delete-mother').addClass('hidden');
    });
    $('#delete-father').click(function(event) {
        event.preventDefault();
        $('#father_id').val('');
        $('#father_infos').empty().addClass('hidden');
        $('#father_actions').removeClass('hidden');
        $('#delete-father').addClass('hidden');
    });
    $('.wysiwyg-speech-input').each(function() {
        $(this).removeAttr('style');
    });
    //Mother city finder
    var timer;
    $('#mother_city').on('keyup', function() {
        clearTimeout(timer);
        timer = setTimeout(function() {
            if ($('#mother_city').val().length >= 3) {
                $('#loading-city-results').removeClass('hidden');
                $.ajax({
                    url: '/cities/ajaxCitiesFinder',
                    type: 'POST',
                    data: {
                        'query': $('#mother_city').val()
                    }
                })
                        .done(function(data) {
                            $('#loading-mother-city-results').addClass('hidden');
                            $('#user-find-results-import').removeClass('hidden');
                            $('#user-find-results-import').html(data);
                        })
                        .fail(function() {
                            $('#loading-mother-city-results').addClass('hidden');
                            $.gritter.add({
                                title: 'Erreur',
                                text: 'Une erreur est survenue. Merci de recommencer ultérieurement.',
                                class_name: 'gritter-error'
                            });
                        })
                        .always(function() {
                        });
            }
        }, 700);
    });
});
function motherFct() {
    //Focusout recherche import contrat
    $('#mother_search_surname').on('focusout', function() {
        setTimeout(function() {
            $('#user-find-results-import').addClass('hidden');
            $('#loading-users-results').addClass('hidden');
        }, 250);
    });
    //Focusin recherche import contrat
    $('#mother_search_surname').on('focusin', function() {
        if (this.value) {
            $('#user-find-results-import').removeClass('hidden');
        }
    });
}

function motherImportUser() {
    var timer;
    $('#mother_search_surname').on('keydown', function() {
        if ($('#mother_search_surname').val() !== '') {
            $('#loading-users-results').removeClass('hidden');
            clearTimeout(timer);
            timer = setTimeout(function() {
                if ($('#mother_search_surname').val() !== '') {
                    $.ajax({
                        url: '/users/ajaxMotherUsersForNewContract',
                        type: 'POST',
                        data: {
                            'q': $('#mother_search_surname').val()
                        }
                    })
                            .done(function(data) {
                                $('#loading-users-results').addClass('hidden');
                                $('#user-find-results-import').removeClass('hidden');
                                $('#user-find-results-import').html(data);
                            })
                            .fail(function() {
                                $('#loading-users-results').addClass('hidden');
                                $.gritter.add({
                                    title: 'Erreur',
                                    text: 'Une erreur est survenue. Merci de recommencer ultérieurement.',
                                    class_name: 'gritter-error'
                                });
                            })
                            .always(function() {
                            })
                }
                else {
                    $('#loading-users-results').addClass('hidden');
                    $('#user-find-results-import').empty();
                    $('#user-find-results-import').addClass('hidden');
                }
            }, 700);
        }
    });
}

function motherClick() {
    $('#user-find-results-import').each(function() {
        $(this).on('click', '#user-result-span', function(event) {
            event.preventDefault();
            var id = $(this).attr('data-id');
            var firstname = $(this).attr('data-firstname');
            var surname = $(this).attr('data-surname');
            var address = $(this).attr('data-address');
            var zip_code = $(this).attr('data-zip-code');
            var city = $(this).attr('data-city');
            var phone = $(this).attr('data-phone');
            var mobile = $(this).attr('data-mobile');
            var email = $(this).attr('data-email');
            var pajemploi = $(this).attr('data-pajemploi');
            var avatar = $(this).attr('data-avatar');
            var mother_html = "<div class=\"row\">" +
                    "<div class=\"col-xs-12 col-sm-3 center\">" +
                    "<span class=\"profile-picture\">" +
                    "<img class=\"img-responsive\" alt=\"" + firstname + " " + surname + "\" src=\"/img/avatars/" + avatar + "\">" +
                    "</span>" +
                    "</div>" +
                    "<div class=\"col-xs-12 col-sm-9\">" +
                    "<div class=\"profile-user-info\">" +
                    "<div class=\"profile-info-row\">" +
                    "<div class=\"contract-profile-info-name\">" +
                    "Prénom NOM" +
                    "</div>" +
                    "<div class=\"contract-profile-info-value\">" +
                    "<span>" +
                    firstname + " " + surname +
                    "</span>" +
                    "</div>" +
                    "</div>" +
                    "<div class=\"profile-info-row\">" +
                    "<div class=\"contract-profile-info-name\">" +
                    "Adresse" +
                    "</div>" +
                    "<div class=\"contract-profile-info-value\">" +
                    "<i class=\"icon-map-marker light-orange bigger-110\"></i> " +
                    "<span>" +
                    address + ",<br/>" + zip_code + ", " + city +
                    "</span>" +
                    "</div>" +
                    "</div>" +
                    "<div class=\"profile-info-row\">" +
                    "<div class=\"contract-profile-info-name\">" +
                    "Téléphone" +
                    "</div>" +
                    "<div class=\"contract-profile-info-value\">" +
                    "<span>" +
                    phone +
                    "</span>" +
                    "</div>" +
                    "</div>" +
                    "<div class=\"profile-info-row\">" +
                    "<div class=\"contract-profile-info-name\">" +
                    "Mobile" +
                    "</div>" +
                    "<div class=\"contract-profile-info-value\">" +
                    "<span>" +
                    mobile +
                    "</span>" +
                    "</div>" +
                    "</div>" +
                    "<div class=\"profile-info-row\">" +
                    "<div class=\"contract-profile-info-name\">" +
                    "Email" +
                    "</div>" +
                    "<div class=\"contract-profile-info-value\">" +
                    "<span>" +
                    email +
                    "</span>" +
                    "</div>" +
                    "</div>" +
                    "</div>" +
                    "</div>" +
                    "</div>";
            $('#mother_id').val(id);
            $('#mother_infos').html(mother_html).removeClass('hidden');
            $('#mother_actions').addClass('hidden');
            $('#delete-mother').removeClass('hidden');
        });
    });
}

function fatherFct() {
    //Focusout recherche import contrat
    $('#father_search_surname').on('focusout', function() {
        setTimeout(function() {
            $('#user-find-results-import').addClass('hidden');
            $('#loading-users-results').addClass('hidden');
        }, 250);
    });
    //Focusin recherche import contrat
    $('#father_search_surname').on('focusin', function() {
        if (this.value) {
            $('#user-find-results-import').removeClass('hidden');
        }
    });
}

function fatherImportUser() {
    var timer;
    $('#father_search_surname').on('keydown', function() {
        if ($('#father_search_surname').val() !== '') {
            $('#loading-users-results').removeClass('hidden');
            clearTimeout(timer);
            timer = setTimeout(function() {
                if ($('#father_search_surname').val() !== '') {
                    $.ajax({
                        url: '/users/ajaxFatherUsersForNewContract',
                        type: 'POST',
                        data: {
                            'q': $('#father_search_surname').val()
                        }
                    })
                            .done(function(data) {
                                $('#loading-users-results').addClass('hidden');
                                $('#user-find-results-import').removeClass('hidden');
                                $('#user-find-results-import').html(data);
                            })
                            .fail(function() {
                                $('#loading-users-results').addClass('hidden');
                                $.gritter.add({
                                    title: 'Erreur',
                                    text: 'Une erreur est survenue. Merci de recommencer ultérieurement.',
                                    class_name: 'gritter-error'
                                });
                            })
                            .always(function() {
                            });
                }
                else {
                    $('#loading-users-results').addClass('hidden');
                    $('#user-find-results-import').empty();
                    $('#user-find-results-import').addClass('hidden');
                }
            }, 700);
        }
    });
}

function fatherClick() {
    $('#user-find-results-import').each(function() {
        $(this).on('click', '#user-result-span', function(event) {
            event.preventDefault();
            var id = $(this).attr('data-id');
            var firstname = $(this).attr('data-firstname');
            var surname = $(this).attr('data-surname');
            var address = $(this).attr('data-address');
            var zip_code = $(this).attr('data-zip-code');
            var city = $(this).attr('data-city');
            var phone = $(this).attr('data-phone');
            var mobile = $(this).attr('data-mobile');
            var email = $(this).attr('data-email');
            var pajemploi = $(this).attr('data-pajemploi');
            var avatar = $(this).attr('data-avatar');
            var father_html = "<div class=\"row\">" +
                    "<div class=\"col-xs-12 col-sm-3 center\">" +
                    "<span class=\"profile-picture\">" +
                    "<img class=\"img-responsive\" alt=\"" + firstname + " " + surname + "\" src=\"/img/avatars/" + avatar + "\">" +
                    "</span>" +
                    "</div>" +
                    "<div class=\"col-xs-12 col-sm-9\">" +
                    "<div class=\"profile-user-info\">" +
                    "<div class=\"profile-info-row\">" +
                    "<div class=\"contract-profile-info-name\">" +
                    "Prénom NOM" +
                    "</div>" +
                    "<div class=\"contract-profile-info-value\">" +
                    "<span>" +
                    firstname + " " + surname +
                    "</span>" +
                    "</div>" +
                    "</div>" +
                    "<div class=\"profile-info-row\">" +
                    "<div class=\"contract-profile-info-name\">" +
                    "Adresse" +
                    "</div>" +
                    "<div class=\"contract-profile-info-value\">" +
                    "<i class=\"icon-map-marker light-orange bigger-110\"></i> " +
                    "<span>" +
                    address + ",<br/>" + zip_code + ", " + city +
                    "</span>" +
                    "</div>" +
                    "</div>" +
                    "<div class=\"profile-info-row\">" +
                    "<div class=\"contract-profile-info-name\">" +
                    "Téléphone" +
                    "</div>" +
                    "<div class=\"contract-profile-info-value\">" +
                    "<span>" +
                    phone +
                    "</span>" +
                    "</div>" +
                    "</div>" +
                    "<div class=\"profile-info-row\">" +
                    "<div class=\"contract-profile-info-name\">" +
                    "Mobile" +
                    "</div>" +
                    "<div class=\"contract-profile-info-value\">" +
                    "<span>" +
                    mobile +
                    "</span>" +
                    "</div>" +
                    "</div>" +
                    "<div class=\"profile-info-row\">" +
                    "<div class=\"contract-profile-info-name\">" +
                    "Email" +
                    "</div>" +
                    "<div class=\"contract-profile-info-value\">" +
                    "<span>" +
                    email +
                    "</span>" +
                    "</div>" +
                    "</div>" +
                    "</div>" +
                    "</div>" +
                    "</div>";
            $('#father_id').val(id);
            $('#father_infos').html(father_html).removeClass('hidden');
            $('#father_actions').addClass('hidden');
            $('#delete-father').removeClass('hidden');
        });
    });
}

function updateSalary(setFoc) {
    var nbsVal = ($('#partial_year_week_nb').val() != '' ? $('#partial_year_week_nb').val() : '');
    var nbAMVal = ($('#occasional_childcare_monthly_hours').val() != '' ? $('#occasional_childcare_monthly_hours').val() : '');
    var salB = ($('#salaire_horaire_brut').val() != '' ? $('#salaire_horaire_brut').val() + '€' : '<span class="error">Taux horaire non renseigné</span>');
    var hW = ($('#nb_hour_weekly').val() != '' ? $('#nb_hour_weekly').val() + ' heures par semaine' : '<span class="error">Nombre d\'heures hebdomadaires non renseigné</span>');
    var nbS = ($('#partial_year_week_nb').val() != '' ? '<input type="text" name="partial_year_week_nb" id="partial_year_week_nb" placeholder="Nb de semaines" title="Veuillez renseigner le nombre de semaine prévues dans l\'année." value="' + $('#partial_year_week_nb').val() + '"/>' : '<input type="text" name="partial_year_week_nb" id="partial_year_week_nb" placeholder="Nb de semaines" title="Veuillez renseigner le nombre de semaine prévues dans l\'année."/>');
    var nbAM = ($('#occasional_childcare_monthly_hours').val() != '' ? '<input type="text" name="occasional_childcare_monthly_hours" id="occasional_childcare_monthly_hours" placeholder="Nb d\' heures mensuel" title="Veuillez renseigner le nombre d\'heures d\'accueil prévues dans le mois." value="' + $('#occasional_childcare_monthly_hours').val() + '"/>' : '<input type="text" name="occasional_childcare_monthly_hours" id="occasional_childcare_monthly_hours" placeholder="Nb d\' heures" title="Veuillez renseigner le nombre d\'heures d\'accueil prévues dans le mois."/>');
    var totAccCom = ($('#nb_hour_weekly').val() != '' && $('#salaire_horaire_brut').val() != '') ? (($('#salaire_horaire_brut').val() * $('#nb_hour_weekly').val() * 52 / 12).toFixed(2)) : '<span class="error" id="calcul-salaire-impossible">Calcul du salaire impossible</span>';
    var totAccIncom = ($('#nb_hour_weekly').val() != '' && $('#salaire_horaire_brut').val() != '' && $('#partial_year_week_nb').val() != '' && $('#partial_year_week_nb').val() != undefined) ? (($('#salaire_horaire_brut').val() * $('#nb_hour_weekly').val() * $('#partial_year_week_nb').val() / 12).toFixed(2)) : '<span class="error" id="calcul-salaire-impossible">Calcul du salaire impossible</span>';
    var totAccOcc = ($('#salaire_horaire_brut').val() != '' && $('#occasional_childcare_monthly_hours').val() != '' && $('#occasional_childcare_monthly_hours').val() != undefined) ? (($('#salaire_horaire_brut').val() * $('#occasional_childcare_monthly_hours').val()).toFixed(2)) : '<span class="error" id="calcul-salaire-impossible">Calcul du salaire impossible</span>';
    var scr = '<script type="text/javascript">$(\'#partial_year_week_nb, #occasional_childcare_monthly_hours\').keyup(function(){updateSalary(true);});';
    if (setFoc == true) {
        var sF = '$(\'#partial_year_week_nb\').focus().val(\'\').val(' + nbsVal + ');$(\'#occasional_childcare_monthly_hours\').focus().val(\'\').val(' + nbAMVal + ');</script>';
    }
    else {
        var sF = '$(\'#partial_year_week_nb\').val(' + nbsVal + ');$(\'#occasional_childcare_monthly_hours\').val(' + nbAMVal + ');</script>';
    }

    var aC = salB + ' X ' + hW + ' X 52 / 12 = <strong id="salaire-mensu">' + totAccCom + '</strong>€';
    var aI = salB + ' X ' + hW + ' x ' + nbS + ' / 12 = <strong id="salaire-mensu">' + totAccIncom + '</strong>€' + scr + sF;
    var aO = salB + ' X ' + nbAM + ' = <strong id="salaire-mensu">' + totAccOcc + '</strong>€' + scr + sF;
    if ($('input:radio[name="guard_type"]:checked').val() == 'accueil_mens_complete') {
        $('#calcul-mensualisation').empty().append(aC);
        $('input[name="vacancy_payment_modality"').each(function() {
            $(this).prop('disabled', true);
            $(this).prop('checked', false);
        });
    }
    if ($('input:radio[name="guard_type"]:checked').val() == 'accueil_mens_incomplete') {
        $('#calcul-mensualisation').empty().append(aI);
        $('input[name="vacancy_payment_modality"').each(function() {
            $(this).prop('disabled', false);
        });
    }
    if ($('input:radio[name="guard_type"]:checked').val() == 'accueil_occasion') {
        $('#calcul-mensualisation').empty().append(aO);
        $('input[name="vacancy_payment_modality"').each(function() {
            $(this).prop('disabled', true);
            $(this).prop('checked', false);
        });
    }
}
