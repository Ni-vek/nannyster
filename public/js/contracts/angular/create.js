(function() {
    app.controller('mother.form.create', ['$http', function($http) {

            this.mother = {
                civility: 'Mme'
            };
            this.create = function() {
                $http.post('/users/ajaxCreateUser', this.mother)
                        .success(function(response) {
                            alert(response);
                            //$('#mother-modal-form').modal('hide');
                        })
                        .error(function() {
                            $.gritter.add({
                                title: 'Erreur',
                                text: 'Une erreur est survenue lors de la création de l\'utilisateur. Veuillez recommencer.',
                                class_name: 'gritter-error'
                            });
                        });
            };
        }]);
    app.directive('chosen', function() {
        var linker = function(scope, element, attrs) {
            var list = attrs['chosen'];
            scope.$watch(list, function() {
                element.trigger('liszt:updated');
                element.trigger("chosen:updated");
            });
            element.ajaxChosen({
                dataType: 'json',
                type: 'POST',
                url: '/cities/ajaxCitiesFinder',
                //data: {'keyboard': 'cat'}, //Or can be [{'name':'keyboard', 'value':'cat'}]. chose your favorite, it handles both.
                success: function(data, textStatus, jqXHR) {
//            doSomething();
                }
            }, {
                processItems: function(data) {
                    return data.results;
                },
//        useAjax: function(e) {
//            return true;
//        },
//        generateUrl: function(q) {
//            return '/cities/ajaxCitiesFinder';
//        },
                loadingImg: '/img/loading.gif',
                minLength: 2
            });
        };
        return {
            restrict: 'A',
            link: linker
        };
    });
})();