<?php

require_once 'bootstrap.php';

for ($i = 1; $i < count($argv); $i++) {
    echo 'Loading '.$argv[$i].' fixture...' . PHP_EOL;
    
    $param = explode('-', $argv[$i]);
    $paramValue = (isset($param[1])?$param[1]:'');
    $argv[$i] = $param[0];
    
    if (substr($argv[$i],0,7) === 'restore') {
        if ($paramValue == ''){
            $paramValue = 'nannyster';
        }
        echo 'Restoring '.$mongo_dbname.PHP_EOL;
        if($mongo_dbname != 'app'){
            try{
                echo exec('sudo mv data/mongodb/'.$paramValue.'-export/app data/mongodb/'.$paramValue.'-export/'.$mongo_dbname);
            }
            catch(Exception $e){
                echo 'Can\'t rename database folder app to '.$mongo_dbname;'!';
            }
        }
        echo exec('mongorestore --drop --port '.$config['port'].' data/mongodb/'.$paramValue.'-export').PHP_EOL;
        if($mongo_dbname != 'app'){
            try{
                echo exec('sudo mv data/mongodb/'.$paramValue.'-export/'.$mongo_dbname.' data/mongodb/'.$paramValue.'-export/app');
            }
            catch(Exception $e){
                echo 'Can\'t rename database folder '.$mongo_dbname;' to app!';
            }
        }
        echo 'Mongo Restored!'.PHP_EOL;
    } 
    
    elseif (substr($argv[$i],0,4) === 'dump') {
        if ($paramValue == ''){ //$paramValue != 'avec' && $paramValue != 'sans' && $paramValue != 'mini')}
            $paramValue = 'nannyster';
        }
        $options = $application->getOptions();
        $dbname = ' ' . $options['resources']['doctrine']['dbal']['connections']['default']['parameters']['dbname'];
        $mongo_dbname = $options['resources']['doctrine']['odm']['documentManagers']['default']['dbname'];
        $user = ' -u ' . $options['resources']['doctrine']['dbal']['connections']['default']['parameters']['user'];
        $pass = $options['resources']['doctrine']['dbal']['connections']['default']['parameters']['password'];
        $pass = ($pass===''?'':' -p'.$pass);
        echo exec('mysqldump -h localhost ' . $user . $pass . $dbname . '  > data/sql/app.'.$paramValue.'-export.sql') . PHP_EOL;
        echo 'Dumped app.'.$paramValue.'-export.sql' . PHP_EOL;
        echo exec('mongodump --port '.$config['port'].' --db '.$mongo_dbname.' --out data/mongodb/'.$paramValue.'-export').PHP_EOL;
        //rename database export
        if($mongo_dbname != 'app'){
            try{
                echo exec('sudo mv data/mongodb/'.$paramValue.'-export/'.$mongo_dbname.' data/mongodb/'.$paramValue.'-export/app');
            }
            catch(Exception $e){
                echo 'Can\'t rename database folder app to '.$mongo_dbname;'!';
            }
        }
        echo 'Dumped '.$paramValue.'-export'.PHP_EOL;
    } else {
        for ($ii = 1; $ii < count($param); $ii++) {
            define(strtoupper($param[$ii]), true);
        }

        require_once(APP_DIR . '/../bin/fixtures/' . $param[0] . '/fixture.php');
    }
    echo 'Done with ' . $argv[$i] . PHP_EOL;
}