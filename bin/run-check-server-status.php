<?php
/**
 * Check server status each 30 minutes, and send mail if there's an error.
 */
require_once '/var/www/nannyster/vendor/mandrill/mandrill/src/Mandrill.php';

$cmds = array(
    'nginx' => array(
        'check' => 'pgrep nginx',
        'start' => 'service nginx start'
    ),
    'php' => array(
        'check' => 'pgrep php',
        'start' => 'service php5-fpm start'
    ),
    'mongodb' => array(
        'check' => 'pgrep mongo',
        'start' => 'service mongodb start'
    ),
    'courier' => array(
        'check' => 'pgrep courier',
        'start' => 'service courier-authdaemon start'
    ),
    'imap' => array(
        'check' => 'pgrep imap',
        'start' => 'service courier-imap start'
    )
);

$template = '';
$needToSendMail = false;

foreach($cmds as $k => $cmd){
	if(exec($cmds[$k]['check'])){
		$template .= '<p>'.$k.' is running at '.date('H:i:s').'</p>'.PHP_EOL;
	}
	else{
		$template .= '<p style="color: red;">'.$k.' doesn\'t seems to be running at '.date('H:i:s').'</p>'.PHP_EOL;
		$needToSendMail = true;
        if(exec($cmds[$k]['start'])){
            $template .= '<p style="color: red;">'.$k.' has been restarted at '.date('H:i:s').'</p>'.PHP_EOL;
        }
        else{
            $template .= '<p style="color: red;">'.$k.' can be restarted at '.date('H:i:s').'</p>'.PHP_EOL;
        }
	}
}

if($needToSendMail){
	$mandrill = new \Mandrill('HWk4p2D35PjYnpJrvxPFcg');
    $message = array(
        'html' => $template,
        'subject' => 'Server status error',
        'from_email' => 'contact@nannyster.fr',
        'from_name' => 'Nannyster',
        'to' => array(
            array(
                'email' => 'kevin.balini@nannyster.fr',
                'name' => 'Kevin BALINI',
                'type' => 'to'
            ),
            array(
                'email' => 'kevin.balini@gmail.com',
                'name' => 'Kevin BALINI',
                'type' => 'to'
            )
        ),
        'headers' => array('Reply-To' => 'contact@nannyster.fr'),
        'important' => false,
        'track_opens' => true,
        'track_clicks' => true,
        'auto_text' => null,
        'auto_html' => null,
        'inline_css' => null,
        'url_strip_qs' => null,
        'preserve_recipients' => true,
        'view_content_link' => null,
        'bcc_address' => null,
        'tracking_domain' => null,
        'signing_domain' => null,
        'return_path_domain' => null,
        'merge' => true,
        'tags' => array('Server Notification'),
        'google_analytics_domains' => array('nannyster.fr'),
        'google_analytics_campaign' => null,
        'metadata' => array('nannyster' => 'www.nannyster.fr')
    );
    $async = false;
    $ip_pool = 'Main Pool';
    $send_at = null;
    $result = $mandrill->messages->send($message, $async, $ip_pool, $send_at);
}
