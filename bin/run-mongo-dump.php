<?php
/**
 * To avoid PHP Warning when running binaries, add -n argument to command line
 */
$startTime = time();
require_once '/var/www/nannyster/vendor/mandrill/mandrill/src/Mandrill.php';

$cmd = 'mongodump --port 32172 -v --out /mongodb/dump/';
$needToSendMail = false;

if(exec($cmd)){
	$template = '<p>Dump success at '.date('H:i:s').' in '.(time() - $startTime).' seconds</p>';
}
else{
	$template = '<p style="color: red;">Dump failed at '.date('H:i:s').' in '.(time() - $startTime).' seconds</p>';
	$needToSendMail = true;
}

if($needToSendMail){
	$mandrill = new \Mandrill('HWk4p2D35PjYnpJrvxPFcg');
    $message = array(
        'html' => $template,
        'subject' => 'Dump MongoDb error',
        'from_email' => 'contact@nannyster.fr',
        'from_name' => 'Nannyster',
        'to' => array(array(
            'email' => 'kevin.balini@nannyster.fr',
            'name' => 'Kevin BALINI',
            'type' => 'to'
        )),
        'headers' => array('Reply-To' => 'contact@nannyster.fr'),
        'important' => false,
        'track_opens' => true,
        'track_clicks' => true,
        'auto_text' => null,
        'auto_html' => null,
        'inline_css' => null,
        'url_strip_qs' => null,
        'preserve_recipients' => true,
        'view_content_link' => null,
        'bcc_address' => null,
        'tracking_domain' => null,
        'signing_domain' => null,
        'return_path_domain' => null,
        'merge' => true,
        'tags' => array('Dump Notification'),
        'google_analytics_domains' => array('nannyster.fr'),
        'google_analytics_campaign' => null,
        'metadata' => array('nannyster' => 'www.nannyster.fr')
    );
    $async = false;
    $ip_pool = 'Main Pool';
    $send_at = null;
    $result = $mandrill->messages->send($message, $async, $ip_pool, $send_at);
}
