<?php

namespace Nannyster\Controllers;

use Phalcon\Mvc\View;
use Nannyster\Models\Users;
use Nannyster\Models\Profiles;
use Nannyster\Models\Civilities;
use Nannyster\Forms\LoginForm;
use Nannyster\Forms\SignUpForm;
use Nannyster\Forms\ForgotPasswordForm;

class IndexController extends ControllerBase
{

    public function indexAction()
    {

        $this->tag->appendTitle(' - Simplifiez-vous la vie');
        $this->view->setVar('activeClass', 'index');
        $form = new SignUpForm();
        $this->view->formSignUp = $form;
    }

    public function createProfileAction()
    {
        $profile = new Profiles();
        $profile->setActive(true);
        $profile->setName('Super Administrateur');
        $profile->setVisible(false);
        $profile->save();
        
        $profile = new Profiles();
        $profile->setActive(true);
        $profile->setName('Administrateur');
        $profile->setVisible(false);
        $profile->save();

        $profile = new Profiles();
        $profile->setActive(true);
        $profile->setName('Nourrice');
        $profile->setVisible(true);
        $profile->save();

        $profile = new Profiles();
        $profile->setActive(true);
        $profile->setName('Parent');
        $profile->setVisible(true);
        $profile->save();

        $profile = new Profiles();
        $profile->setActive(true);
        $profile->setName('Guest');
        $profile->setVisible(false);
        $profile->save();
    }

    public function createCivilityAction()
    {
        $civility = new Civilities();
        $civility->setName('Madame');
        $civility->setShortName('Mme');
        $civility->save();

        $civility = new Civilities();
        $civility->setName('Monsieur');
        $civility->setShortName('Mr');
        $civility->save();
    }

}
