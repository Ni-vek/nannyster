<?php

namespace Nannyster\Controllers;

class PayrollsController extends ControllerBase
{

	public function indexAction(){
		$this->tag->prependTitle('Fiches de paies - ');
		if($this->auth->getProfile() != 'Nourrice'){
	      $this->view->setVar('openTab', 'payrolls');
	      $this->view->setVar('activeClass', 'view-payrolls');
	    }
	    else{
	      $this->view->setVar('activeClass', 'payrolls');
	    }
	    $this->view->setVar('breadcrumbs', array(
	        'Fiches de paie' => array(
	            'last' => true)
	    ));
	}

	public function deleteAction(){
	}

	public function viewAction(){
		$this->tag->prependTitle('Fiche de paie - ##### Décembre 2013 ##### - ');
		if($this->auth->getProfile() != 'Nourrice'){
	      $this->view->setVar('openTab', 'payrolls');
	      $this->view->setVar('activeClass', 'view-payrolls');
	    }
	    else{
	      $this->view->setVar('activeClass', 'payrolls');
	    }
	    $this->view->setVar('breadcrumbs', array(
	        'Fiches de paie' => array(
	            'controller' => 'payrolls',
	            'action' => 'index'),
	        'Fiche de paie - ##### Décembre 2013 #####' => array(
	            'last' => true)
	    ));
	}

	public function createAction(){
		$this->tag->prependTitle('Créer une fiche de paie - ');
		if($this->auth->getProfile() != 'Nourrice'){
	      $this->view->setVar('openTab', 'payrolls');
	      $this->view->setVar('activeClass', 'new-payrolls');
	    }
	    $this->view->setVar('breadcrumbs', array(
	        'Fiches de paie' => array(
	            'controller' => 'payrolls',
	            'action' => 'index'),
	        'Créer une fiche de paie' => array(
	            'last' => true)
	    ));
	}

}