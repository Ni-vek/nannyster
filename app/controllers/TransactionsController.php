<?php

namespace Nannyster\Controllers;

use Nannyster\Payment\Paypal;
use Nannyster\Models\Subscriptions;
use Nannyster\Models\SubscriptionsBase;
use Nannyster\Models\Transactions;
use Nannyster\Models\Users;
use Nannyster\Models\Invoices;

class TransactionsController extends ControllerBase
{

    public function paymentAction()
    {
        $this->view->disable();
        if ($this->request->isPost()) {
            $data = $this->request->getPost();
            
            $subscription = SubscriptionsBase::findById($data['subscription']);
            
            $paypal = new Paypal();
            $payment = $paypal->createPaymentWithCreditCard($subscription, $this->auth->getIdentity(), $data);

//            echo '<pre>';
//            var_dump($payment);
//            echo '</pre>';
            if($payment && $payment->getState() === 'approved'){
                
                $payer = $payment->getPayer();
                $transaction = $payment->getTransactions();
                $related_resources = $transaction[0]->getRelatedResources();
                
                $user = Users::findById($this->auth->getId());
                
                $nannyster_transaction = new Transactions();
                $nannyster_transaction->setPaymentMethod($payer->getPaymentMethod())
                        ->setUser($user)
                        ->setTotal($transaction[0]->getAmount()->getTotal())
                        ->setCurrency($transaction[0]->getAmount()->getCurrency())
                        ->setSubscription($subscription)
                        ->setSaleId($related_resources[0]->getSale()->getId())
                        ->setSaleState($related_resources[0]->getSale()->getState())
                        ->setPaymentId($payment->getId())
                        ->setPaymentCreated(new \MongoDate(strtotime($payment->getCreateTime())))
                        ->setPaymentState($payment->getState())
                        ->save();
                
                $user_subscription = new Subscriptions();
                $user_subscription->setStartDate(new \MongoDate(strtotime(date('Y-m-d'))))
                        ->setSubscriptionBase($subscription)
                        ->setUser($user);
                
                // If the logged user has already a non expired subscription
                if($user->getSubscription()->getEndDate()->sec > strtotime(date('Y-m-d'))){
                    $user_subscription->setEndDate(new \MongoDate((strtotime(date('Y-m-d')) + ($user->getSubscription()->getEndDate()->sec - strtotime(date('Y-m-d'))) + 2678400 * $subscription->getDuration())));
                    $user->getSubscription()->delete();
                }
                else{
                    $user_subscription->setEndDate(new \MongoDate((strtotime(date('Y-m-d')) + 2678400 * $subscription->getDuration())));
                }
                $user_subscription->save();
                $user->setSubscription($user_subscription)
                        ->save();
                $user->fetchForSession();
                
                $invoice = new Invoices();
                $invoice->setSubscription($user_subscription)
                        ->setUser($user)
                        ->setTransaction($nannyster_transaction)
                        ->save();
                
                $this->auth->startSession($user);
                
                $this->flash->success('Votre paiement a bien été validé. Votre facture est disponible dans la partie "Factures" de votre compte.<a href="/invoices" class="btn btn-minier btn-success pull-right">Aller à mes factures</a>');
                $this->response->redirect('subscriptions/index');
                $this->response->send();
            }
            else{
                $this->flash->error('Une erreur est survenue lors de votre paiement. Veuillez recommencer.');
                $this->response->redirect('subscriptions/buy/' . $data['subscription']);
                $this->response->send();
            }
        }
        else{
            $this->response->redirect('subscriptions');
        }
    }

}
