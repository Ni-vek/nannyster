<?php

namespace Nannyster\Controllers;

use Phalcon\Mvc\View;
use Nannyster\Models\Users;
use Nannyster\Models\Profiles;
use Nannyster\Models\PasswordChanges;
use Nannyster\Models\Civilities;
use Nannyster\Forms\ChangePasswordForm;
use Nannyster\Utils\Images;
use Nannyster\Models\Addresses;
use Nannyster\Models\Cities;

class UsersController extends ControllerBase
{

    public function manageAction()
    {

        $this->tag->prependTitle('Manager d\'utilisateurs - ');
        $this->view->setVar('breadcrumbs', array(
            'Administration' => array(
                'controller' => 'administration',
                'action' => 'index'),
            'gestion des utilisateurs' => array(
                'last' => true)
        ));
        $this->assets->addJs('js/jquery.dataTables.min.js');
        $this->assets->addJs('js/jquery.dataTables.bootstrap.js');
        $this->assets->addJs('js/bootbox.min.js');
        $this->assets->addJs('js/users/manage.js');

        $this->view->setVar('users', Users::find());
    }

    /**
     * Users must use this action to change its password
     */
    public function changePasswordAction()
    {
        $this->tag->prependTitle('Changer mon mot de passe - ');
        $user = $this->auth->getIdentity();
        $this->view->setVar('breadcrumbs', array(
            'Profil de ' . ucwords($user->getFirstname()) . ' ' . (($user->getWeddingSurname() != null) ? strtoupper($user->getWeddingSurname()) : strtoupper($user->getSurname())) => array(
                'controller' => 'users',
                'action' => 'view'),
            'Modifier mon mot de passe' => array(
                'last' => true)
        ));
        $form = new ChangePasswordForm();

        if ($this->request->isPost()) {

            if (!$form->isValid($this->request->getPost())) {

                foreach ($form->getMessages() as $message) {
                    $this->flash->error($message);
                }
            }
            else {

                $user->setPassword($this->security->hash($this->request->getPost('password')));
                $user->setMustChangePassword(false);

                $passwordChange = new PasswordChanges();
                $passwordChange->user_id = $user->_id;
                $passwordChange->ip_address = $this->request->getClientAddress();
                $passwordChange->user_agent = $this->request->getUserAgent();

                if (!$user->save()) {
                    $this->flash->error($passwordChange->getMessages());
                }
                else {

                    $this->flash->success('Votre mot de passe a bien été modifié');
                    $this->dispatcher->forward(array(
                        'controller' => 'users',
                        'action' => 'view'
                    ));
                }
            }
        }

        $this->view->form = $form;
    }

    public function viewAction()
    {
        $id = $this->dispatcher->getParam('id');

        $this->assets->addCss('css/select2.css');
        $this->assets->addJs('js/jquery.slimscroll.min.js');
        $this->assets->addJs('js/jquery-ui-1.10.3.full.min.js');
        $this->assets->addJs('js/jquery-ui-1.10.3.custom.min.js');
        $this->assets->addJs('js/jquery.ui.touch-punch.min.js');
        $this->assets->addJs('js/bootbox.min.js');
        $this->assets->addJs('js/jquery.hotkeys.min.js');
        $this->assets->addJs('js/bootstrap-wysiwyg.min.js');
        $this->assets->addJs('js/select2.min.js');
        $this->assets->addJs('js/date-time/bootstrap-datepicker.min.js');
        $this->assets->addJs('js/fuelux/fuelux.spinner.min.js');
        $this->assets->addJs('js/jquery.maskedinput.min.js');
        $this->assets->addJs('js/jquery.easy-pie-chart.min.js');

        $updateAllowed = false;
        if ($id != null) {
            if (self::validateMongoId($id)) {
                //Retrieve user infos
                $user = Users::findById(new \MongoId($id));
            }
        }
        elseif ($this->session->has('auth-identity')) {
            //Retrieve user infos
            $user = $this->auth->getIdentity();

            //Enabled update
            $updateAllowed = true;
        }

        //If user doesn't exists
        if (!$user) {
            $this->flash->error('L\'utilisateur demandé n\'existe pas!');
            $this->response->redirect('dashboard');
            return false;
        }
        //Define usefull js and css if update is allowed
        if ($updateAllowed) {
            $this->assets->addJs('js/bootstrap-editable.js');
            $this->assets->addCss('css/bootstrap-editable.css');
            $this->assets->addJs('js/x-editable/ace-editable.min.js');
            $this->assets->addJs('js/plupload/plupload.js');
            $this->assets->addJs('js/plupload/plupload.flash.js');
            $this->assets->addJs('js/plupload/plupload.html5.js');
            //$this->assets->addJs('css/jquery.fileupload.js');
            $this->assets->addJs('js/users/view.js');
        }

        //Define page title and breadcrumbs
        $this->tag->prependTitle('Profil de ' . $user->getFormatedName() . ' - ');
        $this->view->setVar('breadcrumbs', array('Profil de ' . $user->getFormatedName() => array('last' => true)));
        $this->view->setVar('activeClass', 'profil');
        $this->view->setVar('openTab', 'account');
        
        //Pass vars to view
        $this->view->setVar('updateAllowed', $updateAllowed);
        $this->view->setVar('user', $user);
    }

    public function editAction($id)
    {

        $this->assets->addJs('js/jquery.slimscroll.min.js');
        $this->assets->addJs('js/jquery-ui-1.10.3.custom.min.js');
        $this->assets->addJs('js/jquery.ui.touch-punch.min.js');
        $this->assets->addJs('js/bootbox.min.js');
        $this->assets->addJs('js/jquery.hotkeys.min.js');
        $this->assets->addJs('js/bootstrap-wysiwyg.min.js');
        $this->assets->addJs('js/select2.min.js');
        $this->assets->addJs('js/date-time/bootstrap-datepicker.min.js');
        $this->assets->addJs('js/fuelux/fuelux.spinner.min.js');
        $this->assets->addJs('js/jquery.maskedinput.min.js');
        $this->assets->addJs('js/jquery.easy-pie-chart.min.js');
        $this->assets->addJs('js/jquery.cookie.js');
        $this->assets->addJs('js/bootstrap-editable.js');
        $this->assets->addCss('css/bootstrap-editable.css');
        $this->assets->addJs('js/x-editable/ace-editable.min.js');
        $this->assets->addJs('js/plupload/plupload.js');
        $this->assets->addJs('js/plupload/plupload.flash.js');
        $this->assets->addJs('js/plupload/plupload.html5.js');
        //$this->assets->addJs('css/jquery.fileupload.js');
        //$this->assets->addJs('js/users/view.js');
        $this->assets->addJs('js/users/edit.js');

        if ($id != null) {

            if (self::validateMongoId($id)) {
                //Retrieve user infos
                $user = Users::findById(new \MongoId($id));

                //Update is allowed
                $updateAllowed = true;
            }
            else {
                $this->flash->error('L\'utilisateur demandé n\'existe pas!');
                $this->response->redirect('users/manage');
            }
        }

        //If $id is not defined, we show current logged user
        else {
            $this->flash->error('L\'utilisateur demandé n\'existe pas!');
            $this->response->redirect('users/manage');
        }

        //If user exists
        if ($user != false) {
            $profiles = Profiles::find();
            $profile = Profiles::findById(new \MongoId($user->profile_id));
        }

        //Or not, redirecting to dashboard page with an error message
        else {
            $this->flash->error('L\'utilisateur demandé n\'existe pas!');
            $this->response->redirect('users/manage');
        }

        //Define page title and breadcrumbs
        $this->tag->prependTitle('Profil de ' . $user->getFormatedName() . ' - ');
        $this->view->setVar('breadcrumbs', array(
            'Administration' => array(
                'controller' => 'administration',
                'action' => 'index'),
            'Manager d\'utilisateurs' => array(
                'controller' => 'users',
                'action' => 'manage'),
            'Profil de ' . $user->getFormatedName() => array(
                'last' => true)));

        //Pass vars to view
        $this->view->setVar('updateAllowed', $updateAllowed);
        $this->view->setVar('profiles', $profiles);
        $this->view->setVar('profile', $profile);
        $this->view->setVar('user', $user);
    }

    public function ajaxUsersForNewMailAction()
    {
        if ($this->request->isPost()) {
            if ($this->request->isAjax()) {

                //response view without layout
                $this->view->setRenderLevel(View::LEVEL_NO_RENDER);

                //Define User MongoId to avoid returning his name in users result list
                $arrayMongoId[] = $this->auth->getId();

                $data = $this->request->getPost();
                if (array_key_exists('e', $this->request->getPost()) > 0) {
                    foreach ($data['e']['id'] as $key) {
                        $arrayMongoId[] = new \MongoId($key);
                    }
                    $users = Users::find(array(array('fullname' => array('$regex' => strtolower($data['q'])),
                                    '_id' => array('$nin' => $arrayMongoId),
                                    'active' => true)));
                }
                else {
                    $users = Users::find(array(array('fullname' => array('$regex' => strtolower($data['q'])),
                                    '_id' => array('$nin' => $arrayMongoId),
                                    'active' => true)));
                }
                if (count($users) > 0) {
                    foreach ($users as $user) {
                        echo '<a href="#" id="user-result-span" class="user-result-span" data-id="' . $user->_id . '" data-name="' . $user->getFormatedName() . '">' . $user->getFormatedName() . ' <span class="user-result-ville">(' . (!$user->getAddress()->getCity() ? 'Ville non renseignée' : $user->getAddress()->getCity()->getRealName()) . ')</span></a>';
                    }
                }
                else {
                    echo '<span class="user-no-result-span">Aucun résultat</span>';
                }
                die();
            }
        }
        $this->response->redirect('dashboard');
    }

    public function ajaxProfileEditorAction()
    {
        if ($this->request->isPost()) {
            if ($this->request->isAjax()) {
                $this->view->setRenderLevel(View::LEVEL_NO_RENDER);
                $data = $this->request->getPost();
                if ($data['pk'] == (string) $this->auth->getId()) {
                    $user = Users::findById($this->auth->getId());

                    // Explode data
                    $classInfo = explode('.', $data['name']);
                    if (sizeof($classInfo) > 1) {
                        $data['class'] = $classInfo[0];
                        $data['class_field'] = $classInfo[1];
                        $data['name'] = $classInfo[2];
                    }

                    //Fields array to exclude from strtolower before saving user
                    $arrayField = array('email_notification', 'private_informations', 'directory_visible', 'directory_private_informations', 'banned', 'suspended', 'active', 'mustChangePassword');
                    if (!in_array($data['name'], $arrayField)) {
                        $data['value'] = strtolower($data['value']);
                    }

                    //Fields that need to be converted into boolean
                    $arrayBoolean = array('email_notification', 'private_informations');
                    if (in_array($data['name'], $arrayBoolean)) {
                        switch ($data['value']) {
                            case 'true':
                                $data['value'] = true;
                                break;
                            case 'false':
                                $data['value'] = false;
                                break;
                        }
                    }

                    // Fields that need to be transformed into Nannyster Object
                    $arrayNannysterObject = array(
                        'city' => 'Cities',
                        'birth_city' => 'Cities'
                    );
                    if (isset($data['class_field']) && array_key_exists($data['class_field'], $arrayNannysterObject)) {
                        $class = '\\Nannyster\\Models\\' . $arrayNannysterObject[$data['class_field']];
                        $data['value'] = $class::findById(new \MongoId($data['value']));
                    }
                    elseif (isset($data['name']) && array_key_exists($data['name'], $arrayNannysterObject)) {
                        $class = '\\Nannyster\\Models\\' . $arrayNannysterObject[$data['name']];
                        $data['value'] = $class::findById(new \MongoId($data['value']));
                    }

                    // Fields that need to be transformed into Mongo Object
                    $arrayMongoObject = array(
                        'start_date' => '\\MongoDate',
                        'renew_date' => '\\MongoDate',
                        'birth_date' => '\\MongoDate',
                        'validity_date' => '\\MongoDate'
                    );
                    if (isset($data['class_field']) && array_key_exists($data['class_field'], $arrayMongoObject)) {
                        if ($arrayMongoObject[$data['class_field']] === '\\MongoDate') {
                            $data['value'] = new $arrayMongoObject[$data['class_field']](strtotime($data['value']));
                        }
                        else {
                            $data['value'] = new $arrayMongoObject[$data['class_field']]($data['value']);
                        }
                    }
                    elseif (array_key_exists($data['name'], $arrayMongoObject)) {
                        if ($arrayMongoObject[$data['name']] === '\\MongoDate') {
                            $data['value'] = new $arrayMongoObject[$data['name']](strtotime($data['value']));
                        }
                        else {
                            $data['value'] = new $arrayMongoObject[$data['name']]($data['value']);
                        }
                    }

                    // create linked attribute class
                    if (isset($data['class'])) {
                        $userGetter = 'get' . \Nannyster\Utils\CamelCase::to_camel_case($data['name'], true);
                        if ($user->$userGetter() === null) {
                            $class = new $data['class']();
                            $class->hydrate(array($data['class_field'] => $data['value']));
                            $class->save();
                            $user->hydrate(array($data['name'] => $class));
                        }
                        else {
                            $class = $user->$userGetter();
                            $class->hydrate(array($data['class_field'] => $data['value']));
                            $class->save();
                            $user->hydrate(array($data['name'] => $class));
                        }
                    }
                    else {
                        $user->hydrate(array($data['name'] => $data['value']));
                    }
                    if ($user->save()) {
                        $this->session->set('auth-identity', serialize($user->fetchForSession()));
                        return $this->response->setStatusCode(200, 'OK');
                    }
                    else {
                        return $this->response->setStatusCode(400, 'NOK');
                    }
                }
                else {
                    return $this->response->setStatusCode(400, 'NOK');
                }
            }
        }
        $this->response->redirect('dashboard');
    }

    public function ajaxAdminProfileEditorAction()
    {
        if ($this->request->isPost()) {
            if ($this->request->isAjax()) {
                $this->view->setRenderLevel(View::LEVEL_NO_RENDER);
                $data = $this->request->getPost();
                $user = Users::findById(new \MongoId($data['pk']));

                //Field array to exclude from strtolower before saving user
                $arrayField = array('email_notification', 'private_informations', 'directory_visible', 'directory_private_informations', 'banned', 'suspended', 'active', 'mustChangePassword');
                if (in_array($data['name'], $arrayField)) {
                    $user->$data['name'] = $data['value'];
                }
                else {
                    $user->$data['name'] = strtolower($data['value']);
                }
                if ($user->save()) {
                    return $this->response->setStatusCode(200, 'OK');
                }
                else {
                    return $this->response->setStatusCode(400, 'NOK');
                }
            }
        }
        $this->response->redirect('dashboard');
    }

    public function ajaxImageEditorAction()
    {
        //retrieve user identity
        $user = $this->auth->getIdentity();
        
        if ($this->request->hasFiles()) {
            $this->view->setRenderLevel(View::LEVEL_NO_RENDER);


            //define upload folder
            $uploadFolder = "/public/img/avatars";

            //define full path to upload folder
            $uploadPath = BASE_DIR . $uploadFolder;

            // Proceed each images
            foreach ($this->request->getUploadedFiles() as $file) {

                //Check file type
                switch ($file->getType()) {
                    case 'image/png':
                        $ext = 'png';
                        break;

                    case 'image/jpeg':
                        $ext = 'jpeg';
                        break;

                    case 'image/gif':
                        $ext = 'gif';
                        break;

                    default:
                        die('{"error":true, "message": "Votre photo n\'est pas au bon format. Vous ne pouvez télécharger que des photos aux formats .jpeg, .png ou .gif. Veuillez recommencer", "oldImage": "' . $user['avatar'] . '"}');
                        break;
                }

                if ($file->getError() == 0) {

                    //Define new image name
                    $newImageName = self::uuidV4() . '.' . $ext;

                    //check if file exists in upload folder
                    while (file_exists($uploadPath . '/' . $newImageName)) {
                        //Define a newer image name
                        $newImageName = self::uuidV4() . '.' . $ext;
                    }

                    //create full path with image name
                    $full_image_path = $uploadPath . '/' . $newImageName;

                    //Move the picture to img/avatar folder
                    try {
                        $img = new Images($file);
                        $img->best_fit(800, 800)->save($full_image_path);
                        $img->adaptive_resize(48, 48)->save($uploadPath . '/thumbs/' . $newImageName);

                        //If the user has already upload a custom profile picture, we delete it and its avatar
                        if (file_exists('img/avatars/' . $user->getAvatarFile()) && $user->getAvatarFile() != 'default.jpg') {
                            unlink('img/avatars/' . $user->getAvatarFile());
                        }
                        if (file_exists('img/avatars/thumbs/' . $user->getAvatarFile()) && $user->getAvatarFile() != 'default.jpg') {
                            unlink('img/avatars/thumbs/' . $user->getAvatarFile());
                        }

                        //Save it in mongo
                        $user->setAvatarFile($newImageName)->save();

                        //Updating avatar's value in session
                        $this->session->set('auth-identity', serialize($user->fetchForSession()));

                        die('{"error":false, "message": "Votre photo a bien été téléchargée", "newImage": "' . $newImageName . '"}');
                    }
                    catch (Exception $e) {
                        die('{"error":true, "message": "Il y a eu une erreur lors du téléchargement de votre photo. Veuillez recommencer", "oldImage": "' . $user->getAvatarFile() . '"}');
                    }
                }
                else {
                    die('{"error":true, "message": "Il y a eu une erreur lors du téléchargement de votre photo. Veuillez recommencer", "oldImage": "' . $user->getAvatarFile() . '"}');
                }
            }
        }
        else {
            die('{"error":true, "message": "Votre requête ne contient pas de photo. Veuillez recommencer", "oldImage": "' . $user->getAvatarFile() . '"}');
        }
    }

    /**
     * Retrieve each mother account for a new contract
     */
    public function ajaxMotherUsersForNewContractAction()
    {
        if ($this->request->isPost()) {
            if ($this->request->isAjax()) {
                $this->view->setRenderLevel(View::LEVEL_NO_RENDER);
                $data = $this->request->getPost();

                $users = Users::find(array(array(
                                'civility' => Civilities::findFirst(array(array(
                                        'short_name' => 'Mme')))->getId(),
                                'fullname' => array('$regex' => strtolower($data['q'])),
                                '_id' => array('$nin' => array(new \MongoId($this->auth->getId())))
                )));

                if ($users) {
                    foreach ($users as $user) {
                        echo '<a href="#" id="user-result-span" class="user-result-span" data-id="' . (string) $user->getId() . '" data-firstname="' . ucwords($user->getFirstname()) . '" data-surname="' . strtoupper($user->getSurname()) . '" data-address="' . ucwords($user->getAddress()->getAddresses()) . '" data-zip-code="' . ((!$user->getAddress()->getCity()) ? '' : $user->getAddress()->getCity()->getZipCode()) . '" data-city="' . ((!$user->getAddress()->getCity()) ? '' : ucwords($user->getAddress()->getCity()->getRealName())) . '" data-phone="' . $user->getPhone() . '" data-mobile="' . $user->getMobile() . '" data-email="' . $user->getEmail() . '" data-pajemploi="' . strtoupper($user->getPajemploiNb()) . '" data-avatar="' . $user->getAvatarFile() . '">' . $user->getFormatedName() . ' <span class="user-result-ville">(' . ((!$user->getAddress()->getCity()) ? '' : ucwords($user->getAddress()->getCity()->getRealName())) . ')</span></a>';
                    }
                    return true;
                }
                else {
                    echo '<span class="user-no-result-span">Aucun résultat</span>';
                    return true;
                }
            }
        }
        $this->response->redirect('contracts');
    }

    /**
     * Retrieve each father account for a new contract
     */
    public function ajaxFatherUsersForNewContractAction()
    {
        if ($this->request->isPost()) {
            if ($this->request->isAjax()) {
                $this->view->setRenderLevel(View::LEVEL_NO_RENDER);
                $data = $this->request->getPost();

                $users = Users::find(array(array(
                                'civility' => Civilities::findFirst(array(array(
                                        'short_name' => 'Mr')))->getId(),
                                'fullname' => array('$regex' => strtolower($data['q'])),
                                '_id' => array('$nin' => array(new \MongoId($this->auth->getId())))
                )));

                if ($users) {
                    foreach ($users as $user) {
                        echo '<a href="#" id="user-result-span" class="user-result-span" data-id="' . (string) $user->getId() . '" data-firstname="' . ucwords($user->getFirstname()) . '" data-surname="' . strtoupper($user->getSurname()) . '" data-address="' . ucwords($user->getAddress()->getAddress1()) . '" data-zip-code="' . $user->getAddress()->getCity()->getZipCode() . '" data-city="' . ucwords($user->getAddress()->getCity()->getRealName()) . '" data-phone="' . $user->getPhone() . '" data-mobile="' . $user->getMobile() . '" data-email="' . $user->getEmail() . '" data-pajemploi="' . strtoupper($user->getPajemploiNb()) . '" data-avatar="' . $user->getAvatarFile() . '">' . $user->getFormatedName() . ' <span class="user-result-ville">(' . ucwords($user->getAddress()->getCity()->getRealName()) . ')</span></a>';
                    }
                    return true;
                }
                else {
                    echo '<span class="user-no-result-span">Aucun résultat</span>';
                    return true;
                }
            }
        }
        $this->response->redirect('contracts');
    }

    public function deleteAction($id)
    {
        if ($id != null) {
            if (self::validateMongoId($id)) {
                $user = Users::findById(new \MongoId($id));

                if ($user) {
                    if ($user->delete()) {
                        $this->flash->success('L\'utilisateur a bien été supprimé');
                        return $this->response->redirect('users/manage');
                    }
                }
            }
        }
        $this->flash->error('L\'utilisateur n\'existe pas!');
        $this->response->redirect('users/manage');
    }

    public function ajaxCreateUserAction()
    {
        if ($this->request->isPost()) {
            $this->view->setRenderLevel(View::LEVEL_NO_RENDER);
            
            $data = $this->request->getPost();
            
            $user = new Users();
            $address = new Addresses();
            
            $address->setAddress1(strtolower($data['address']))
                    ->setCity(Cities::findById(new \MongoId($data['city'])))
                    ->save();
            $user->setSurname(strtolower($data['surname']))
                    ->setFirstname(strtolower($data['firstname']))
                    ->setAddress($address)
                    ->setEmail(strtolower($data['email']))
                    ->setPhone($data['phone'])
                    ->setMobile($data['mobile'])
                    ->setCivility(Civilities::findFirst(array(array('short_name' => $data['civility']))))
                    ->setProfile(Profiles::findFirst(array(array('name' => 'Parent'))))
                    ->setCreatedByNanny(true)
                    ->setParent($this->auth->getIdentity())
                    ->setMustChangePassword(true)
                    ->setUncryptedPassword($user->randomPassword())
                    ->setPassword($this->security->hash($user->getUncryptedPassword()))
                    ->save();
            $user->fetchForAjax();
            die(json_encode($user));            
        }
        else {
            $this->response->redirect('dashboard');
        }
    }
    
    public function ajaxCheckUserMailAction(){
        if($this->request->isPost()){
            $data = $this->request->getPost();
            $user = Users::findFirst(array(array(
                'email' => $data['email']
            )));
            if($user){
                $user->fetchForAjax();
                die(json_encode($user));
            }
            else{
                die('true');
            }
        }
    }

}
