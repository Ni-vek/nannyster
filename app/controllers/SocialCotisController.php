<?php

namespace Nannyster\Controllers;

use Nannyster\Models\SocialCotis;

class SocialCotisController extends ControllerBase
{
    public function indexAction(){
        $this->tag->prependTitle('Cotisations sociales - ');
        $this->view->setVar('activeClass', 'administration');
        $this->view->setVar('breadcrumbs', array(
            'Administration' => array(
                'controller' => 'administration',
                'action' => 'index'),
            'Cotisations sociales' => array(
                'last' => true)));
        $this->view->setVar('cotisations', SocialCotis::find());
    }
    
    public function addAction(){
        $this->tag->prependTitle('Cotisations sociales - ');
        $this->view->setVar('activeClass', 'administration');
        $this->view->setVar('breadcrumbs', array(
            'Administration' => array(
                'controller' => 'administration',
                'action' => 'index'),
            'Ajouter une cotisation sociale' => array(
                'last' => true)));
        if($this->request->isPost()){
            $cotisation = new SocialCotis();
            $cotisation->hydrate($this->request->getPost());
            if($cotisation->save()){
                $this->view->disable();
                $this->flash->success('La cotisation a bien été sauvegardée');
                $this->response->redirect('social_cotis');
            }
            else{
                $this->view->disable();
                $this->flash->error('La cotisation n\'a pas été sauvegardée');
                $this->response->redirect('social_cotis');
            }
            
        }
    }
    
    public function deleteAction($id){
        $cotisation = SocialCotis::findById(new \MongoId($id));
        if($cotisation->delete()){
                $this->view->disable();
                $this->flash->success('La cotisation a bien été supprimée');
                $this->response->redirect('social_cotis');
        }
        else{
                $this->view->disable();
                $this->flash->error('La cotisation n\'a pas été supprimée');
                $this->response->redirect('social_cotis');
        }
    }
}