<?php

namespace Nannyster\Controllers;

class SeoController extends ControllerBase
{
	public function indexAction(){

        $this->tag->prependTitle('SEO - ');
        $this->view->setVar('activeClass', 'administration');
        $this->view->setVar('breadcrumbs', array(
            'Administration' => array(
                'controller' => 'administration',
                'action' => 'index'),
            'SEO' => array(
                'last' => true)
        ));
  }
}