<?php

namespace Nannyster\Controllers;

use Phalcon\Mvc\Controller;
use Phalcon\Mvc\Dispatcher;
use Nannyster\Models\Mailboxes;
use Nannyster\Forms\LoginForm;
use Nannyster\Forms\ForgotPasswordForm;

class ControllerBase extends Controller
{

    /**
     * Execute before the router so we can determine if this is a provate controller, and must be authenticated, or a
     * public controller that is open to all...
     *
     * @param Dispatcher $dispatcher
     * @return boolean
     */
    public function beforeExecuteRoute(Dispatcher $dispatcher)
    {
        // Check if app is in maintenance mode
        if ($this->getDI()->getShared('config')->application->isInMaintenance) {
            if ($this->auth->hasIdentity()) {
                if ($this->auth->getProfile()->getName() !== 'Super Administrateur') {
                    if ($dispatcher->getControllerName() !== 'errors' && $dispatcher->getActionName() !== 'maintenance') {
                        $dispatcher->forward(array('controller' => 'errors', 'action' => 'maintenance'));
                        return false;
                    }
                }
            }
            else {
                if ($dispatcher->getControllerName() !== 'errors' && $dispatcher->getActionName() !== 'maintenance') {
                    $dispatcher->forward(array('controller' => 'errors', 'action' => 'maintenance'));
                    return false;
                }
            }
        }
        else {

            $controllerName = $dispatcher->getControllerName();
            $actionName = $dispatcher->getActionName();
            $profile = $this->auth->hasIdentity() ? $this->auth->getProfile()->getName() : 'Guest';
//        var_dump($this->auth->getProfile()->getId());
//        var_dump($controllerName);
//        var_dump($actionName);
//        var_dump($this->acl->getAcl()->isAllowed($this->auth->getProfile()->getId(), $controllerName, $actionName)); die();
            //Check if user has remember me!
            if (!$this->session->has('auth-identity')) {
                if ($this->auth->hasRememberMe()) {
                    if ($actionName !== 'logout') {
                        if ($this->auth->loginWithRememberMe()) {
                            if ($this->acl->isAllowed($profile, $controllerName, $actionName)) {
                                $dispatcher->forward(array('controller' => $controllerName, 'action' => $actionName));
                                return true;
                            }
                            elseif ($this->acl->isAllowed($profile, $controllerName, 'index')) {
                                $dispatcher->forward(array('controller' => $controllerName, 'action' => 'index'));
                                return true;
                            }
                            else {
                                $dispatcher->forward(array('controller' => 'dashboard', 'action' => 'index'));
                                return true;
                            }
                        }
                        else {
                            $dispatcher->forward(array('controller' => 'index', 'action' => 'index'));
                            return false;
                        }
                    }
                }
                elseif ($this->auth->isLocked()) {
                    if ($controllerName !== 'session' && $actionName !== 'lock') {
                        $this->response->redirect('session/lock');
                    }
                }
                else {
                    if (!$this->acl->isAllowed($profile, $controllerName, $actionName)) {
                        $dispatcher->forward(array('controller' => 'index'));
                    }
                    // Special case for the profile view that is the same for a logged user and others
                    else {
                        if ($controllerName === 'users' && $actionName === 'view' && $dispatcher->getParam('id') === null) {
                            $this->flash->error('Vous devez être connecté pour accéder à cette partie du site');
                            $dispatcher->forward(array('controller' => 'index', 'action' => 'index'));
                            return;
                        }
                    }
                }
            }
            else {
                if ($controllerName == 'index') {
                    $dispatcher->forward(array('controller' => 'dashboard', 'action' => 'index'));
                }
            }

            // Check if the user have permission to the current option
            if (!$this->acl->isAllowed($profile, $controllerName, $actionName)) {

                if ($this->session->has('auth-identity')) {
                    if ($controllerName !== 'index') {
                        $dispatcher->forward(array(
                            'controller' => 'errors',
                            'action' => 'notFound'
                        ));
                    }
                    else {
                        $dispatcher->forward(array(
                            'controller' => 'dashboard',
                            'action' => 'index'
                        ));
                    }
                }
                else {
                    $this->flash->error('Vous devez vous connecter pour accéder à cette partie du site');
                    $dispatcher->forward(array(
                        'controller' => 'session',
                        'action' => 'login'
                    ));
                }

                return false;
            }
        }
    }

    public function initialize()
    {
        // Define template
        $this->view->setTemplateBefore('private');

        // Define default profile
        $this->view->profile = $this->auth->hasIdentity() ? $this->auth->getProfile()->getName() : 'Guest';
//     var_dump($this->session); die();
        //Check if user is logged in
        if ($this->session->has('auth-identity')) {
            $identity = $this->auth->getIdentity();
            $this->view->identity = $identity;
            $this->view->newMail = Mailboxes::count(array(array(
                            'user' => $identity->getId(),
                            'unread_messages' => true,
                            'conversation_deleted' => false)));
            $this->view->allMail = Mailboxes::count(array(array(
                            'user' => $identity->getId(),
                            'conversation_deleted' => false)));

            //Check if a profile is complete
            if (($identity->getAddress()->getCity() === null ||
                    ($identity->getMobile() === null && $identity->getPhone() === null) ||
                    ($identity->getProfile()->getName() === 'Nourrice' && $identity->getAgreement() === null))) {
                $this->view->setVar('showAlert', '<div class="alert alert-danger">Veuillez compléter entièrement votre profil afin de pouvoir profiter de tous les services de Nannyster.' . $this->tag->linkTo(array(
                            'users/view',
                            'Aller à mon profil',
                            'class' => 'btn btn-danger btn-xs pull-right'
                        )) . '</div>'
                );
            }

            // Check if a subscription has ended
            if ($this->auth->getIdentity()->getSubscription()->getExpired()) {
                $this->flash->error('<i class="icon-warning-sign"></i> Votre abonnement a expiré! Très peu de fonctionnalités vous sont désormais accessibles. <a href="/subscriptions" class="btn btn-danger btn-xs pull-right"><i class="icon-retweet"></i> Je veux renouveler mon abonnement!</a>');
            }
        }
        else {
            $form = new ForgotPasswordForm();
            $this->view->formForgotPassword = $form;
            $form = new LoginForm();
            $this->view->formLogin = $form;
        }
        //Last part of the title
        $this->tag->setTitle($this->config->application->siteTitle);

        //Basic collection of CSS files
        $this->assets
                ->collection('basicCss')
                ->addCss('css/bootstrap.min.css', true)
                ->addCss('css/jquery.gritter.css', true)
                ->addCss('css/font-awesome.min.css', true);

        //Theme collection of CSS files
        $this->assets
                ->collection('themeCss')
                ->addCss('css/ace-skins.min.css', true)
                ->addCss('css/ace.min.css', true)
                ->addCss('css/ace-customized.css', true);

        //Angular
//        $this->assets
//                ->collection('angular')
//                ->addJs('js/libs/angular/angular.min.js')
//                ->addJs('js/app.js');
        //jQuery
        $this->assets
                ->collection('jQuery')
                ->addJs('js/jquery-2.0.3.min.js');

        //jQuery for IE
        $this->assets
                ->collection('jQueryIE')
                ->addJs('js/jquery-1.10.2.min.js');

        //Theme collection of js files
        $this->assets
                ->collection('themeJs')
                ->addJs('js/bootstrap.min.js')
                ->addJs('js/theme-elements.min.js')
                ->addJs('js/theme.min.js')
                ->addJs('js/theme-extra.min.js')
                ->addJs('js/jquery.gritter.min.js')
                ->addJs('js/jquery.timeago.js')
                ->addJs('js/jquery.cookie.js')
                ->addJs('js/common.js');
    }

    public static function validateMongoId($id)
    {
        if (preg_match('/^[0-9a-z]{24}$/', $id)) {
            return true;
        }
        else {
            return false;
        }
    }

    public static function returnDateTime(\MongoDate $date = null, $what = null)
    {
        if ($date !== null) {
            if ($what === 'date') {
                return date('d/m/Y', $date->sec);
            }
            elseif ($what === 'time') {
                return date('H:i', $date->sec);
            }
            elseif ($what === 'fulltime') {
                return date('H:i:s', $date->sec);
            }
            elseif ($what === 'fulldatetime') {
                return date('d/m/Y à H:i:s', $date->sec);
            }
            elseif ($what === 'timeago') {
                $diff = (time() - $date->sec);
                if ($diff < 60) {
                    return 'A l\'instant';
                }
                elseif ($diff >= 60 && $diff < 3600) {
                    $ext = (gmdate('i', $diff) > 1 ? ' minutes' : ' minute');
                    $min = (gmdate('i', $diff) < 10 ? substr(gmdate('i', $diff), 1, 1) : gmdate('i', $diff));
                    return '<span data-rel="tooltip" data-original-title="' . self::returnDateTime($date) . '">Il y a ' . $min . $ext . '</span>';
                }
                elseif ($diff >= 3600 && $diff < 86400) {
                    return '<span data-rel="tooltip" data-original-title="' . self::returnDateTime($date) . '">Il y a ' . gmdate('H', $diff) . ' h' . gmdate('i', $diff) . ' min' . '</span>';
                }
                elseif ($diff >= 86400 && $diff < 604800) {
                    $ext = (gmdate('j', $diff) > 1 ? ' jours' : ' jour');
                    return '<span data-rel="tooltip" data-original-title="' . self::returnDateTime($date) . '">Il y a ' . gmdate('j', $diff) . $ext . '</span>';
                }
                elseif ($diff >= 604800 && $diff < 2592000) {
                    $ext = (ceil(gmdate('j', $diff) / 7) > 1 ? ' semaines' : ' semaine');
                    return '<span data-rel="tooltip" data-original-title="' . self::returnDateTime($date) . '">Il y a environ ' . ceil(gmdate('j', $diff) / 7) . $ext . '</span>';
                }
                elseif ($diff >= 2592000 && $diff < 31104000) {
                    return '<span data-rel="tooltip" data-original-title="' . self::returnDateTime($date) . '">Il y a environ ' . ceil(gmdate('j', $diff) / 30) . ' mois</span>';
                }
                elseif ($diff >= 31104000) {
                    $ext = (ceil(gmdate('j', $diff) / 365) > 1 ? ' années' : ' année');
                    return '<span data-rel="tooltip" data-original-title="' . self::returnDateTime($date) . '">Il y a environ ' . ceil(gmdate('j', $diff) / 365) . $ext . '</span>';
                }
            }
            elseif ($what === 'timeagowithouttooltip') {
                $diff = (time() - $date->sec);
                if ($diff < 60) {
                    return 'A l\'instant';
                }
                elseif ($diff >= 60 && $diff < 3600) {
                    $ext = (gmdate('i', $diff) > 1 ? ' minutes' : ' minute');
                    $min = (gmdate('i', $diff) < 10 ? substr(gmdate('i', $diff), 1, 1) : gmdate('i', $diff));
                    return 'Il y a ' . $min . $ext;
                }
                elseif ($diff >= 3600 && $diff < 86400) {
                    return 'Il y a ' . gmdate('H', $diff) . ' h' . gmdate('i', $diff) . ' min';
                }
                elseif ($diff >= 86400 && $diff < 604800) {
                    $ext = (gmdate('j', $diff) > 1 ? ' jours' : ' jour');
                    return 'Il y a ' . gmdate('j', $diff) . $ext;
                }
                elseif ($diff >= 604800 && $diff < 2592000) {
                    $ext = (ceil(gmdate('j', $diff) / 7) > 1 ? ' semaines' : ' semaine');
                    return 'Il y a environ ' . ceil(gmdate('j', $diff) / 7) . $ext;
                }
                elseif ($diff >= 2592000 && $diff < 31104000) {
                    return 'Il y a environ ' . ceil(gmdate('j', $diff) / 30) . ' mois';
                }
                elseif ($diff >= 31104000) {
                    $ext = (ceil(gmdate('j', $diff) / 365) > 1 ? ' années' : ' année');
                    return 'Il y a environ ' . ceil(gmdate('j', $diff) / 365) . $ext;
                }
            }
            else {
                return date('d/m/Y à H:i', $date->sec);
            }
        }
        else {
            return null;
        }
    }

    #---------------------------------------------------------------------------------------
    # FUNCTION: datediff($interval, $datefrom, $dateto, $using_timestamps = false)
    # DATE CREATED: Mar 31, 2005
    # AUTHOR: I Love Jack Daniels
    # PURPOSE: Just like the DateDiff function found in Visual Basic
    #	$interval can be:
    #	yyyy   - Number of full years
    #	q	  - Number of full quarters
    #	m	  - Number of full months
    #	y	  - Difference between day numbers  (eg 1st Jan 2004 is "1", the first day. 2nd Feb 2003 is "33". The datediff is "-32".)
    #	d	  - Number of full days
    #	w	  - Number of full weekdays
    #	ww	 - Number of full weeks
    #	h	  - Number of full hours
    #	n	  - Number of full minutes
    #	s	  - Number of full seconds (default)
    #---------------------------------------------------------------------------------------

    public static function dateDiff($interval, $datefrom, $dateto, $using_timestamps = false)
    {
        if (!$using_timestamps) {
            $datefrom = strtotime($datefrom, 0);
            $dateto = strtotime($dateto, 0);
        }
        $difference = $dateto - $datefrom; // Difference in seconds
        switch ($interval) {
            case 'yyyy': $years_difference = floor($difference / 31536000);
                if (mktime(date("H", $datefrom), date("i", $datefrom), date("s", $datefrom), date("n", $datefrom), date("j", $datefrom), date("Y", $datefrom) + $years_difference) > $dateto) {
                    $years_difference--;
                }
                if (mktime(date("H", $dateto), date("i", $dateto), date("s", $dateto), date("n", $dateto), date("j", $dateto), date("Y", $dateto) - ($years_difference + 1)) > $datefrom) {
                    $years_difference++;
                }
                $datediff = $years_difference;
                break;
            case "q": $quarters_difference = floor($difference / 8035200);
                while (mktime(date("H", $datefrom), date("i", $datefrom), date("s", $datefrom), date("n", $datefrom) + ($quarters_difference * 3), date("j", $dateto), date("Y", $datefrom)) < $dateto) {
                    $months_difference++;
                }
                $quarters_difference--;
                $datediff = $quarters_difference;
                break;
            case "m": $months_difference = floor($difference / 2678400);
                while (mktime(date("H", $datefrom), date("i", $datefrom), date("s", $datefrom), date("n", $datefrom) + ($months_difference), date("j", $dateto), date("Y", $datefrom)) < $dateto) {
                    $months_difference++;
                }
                $months_difference--;
                $datediff = $months_difference;
                break;
            case 'y': $datediff = date("z", $dateto) - date("z", $datefrom);
                break;
            case "d": $datediff = floor($difference / 86400);
                break;
            case "w": $days_difference = floor($difference / 86400);
                $weeks_difference = floor($days_difference / 7); // Complete weeks
                $first_day = date("w", $datefrom);
                $days_remainder = floor($days_difference % 7);
                $odd_days = $first_day + $days_remainder; // Do we have a Saturday or Sunday in the remainder?
                if ($odd_days > 7) {
                    $days_remainder--;
                }
                if ($odd_days > 6) {
                    $days_remainder--;
                }
                $datediff = ($weeks_difference * 5) + $days_remainder;
                break;
            case "ww": $datediff = floor($difference / 604800);
                break;
            case "h": $datediff = floor($difference / 3600);
                break;
            case "n": $datediff = floor($difference / 60);
                break;
            default: $datediff = $difference;
                break;
        }
        return $datediff;
    }

    public static function returnMonthName($id, $short = false, $lang = 'fr')
    {
        $month = array(
            'fr' => array(
                'full' => array(
                    'Janvier',
                    'Février',
                    'Mars',
                    'Avril',
                    'Mai',
                    'Juin',
                    'Juillet',
                    'Août',
                    'Septembre',
                    'Octobre',
                    'Novembre',
                    'Décembre'
                ),
                'short' => array(
                    'Jan',
                    'Fév',
                    'Mars',
                    'Avr',
                    'Mai',
                    'Juin',
                    'Jull',
                    'Août',
                    'Sept',
                    'Oct',
                    'Nov',
                    'Déc'
                )
            )
        );
        if($short){
            return $month[$lang]['short'][--$id];
        }
        else{
            return $month[$lang]['full'][--$id];
        }
    }

    public static function uuidV4()
    {
        return sprintf('%04x%04x-%04x-%04x-%04x-%04x%04x%04x',
                // 32 bits for "time_low"
                mt_rand(0, 0xffff), mt_rand(0, 0xffff),
                // 16 bits for "time_mid"
                mt_rand(0, 0xffff),
                // 16 bits for "time_hi_and_version",
                // four most significant bits holds version number 4
                mt_rand(0, 0x0fff) | 0x4000,
                // 16 bits, 8 bits for "clk_seq_hi_res",
                // 8 bits for "clk_seq_low",
                // two most significant bits holds zero and one for variant DCE1.1
                mt_rand(0, 0x3fff) | 0x8000,
                // 48 bits for "node"
                mt_rand(0, 0xffff), mt_rand(0, 0xffff), mt_rand(0, 0xffff)
        );
    }

}
