<?php

namespace Nannyster\Controllers;

class TimelineController extends ControllerBase
{

	public function indexAction(){
        $this->tag->prependTitle('Timeline - ');
        $this->view->setVar('activeClass', 'timeline');
        $this->view->setVar('breadcrumbs', array('Timeline' => array('last' => true)));

	}
}