<?php
namespace Nannyster\Controllers;

use Nannyster\Models\EmailConfirmations;
use Nannyster\Models\ResetPasswords;
use Nannyster\Models\Users;
use Nannyster\Models\Tickets;

/**
 * UserControlController
 * Provides help to users to confirm their passwords or reset them
 */
class UserControlController extends ControllerBase
{

    public function initialize()
    {
        if ($this->session->has('auth-identity')) {
            $this->view->setTemplateBefore('private');
        }
    }

    /**
     * Confirms an e-mail, if the user must change thier password then changes it
     */
    public function confirmEmailAction()
    {
        $code = $this->dispatcher->getParam('code');

        $confirmation = EmailConfirmations::findFirst(array(array('code' => $code)));
        
        if (!$confirmation) {
            $this->flashSession->error('Ce lien de validation n\'est pas valide!');
            return $this->response->redirect('index');
        }

        if ($confirmation->getConfirmed()) {
            $this->flashSession->warning('Cette adresse email a déjà été validée!');
            return $this->response->redirect('index');
        }

        $user = Users::findById($confirmation->getUser()->getId());

        $confirmation->setConfirmed(true);

        $user->setActive(true);

        /**
         * Change the confirmation to 'confirmed' and update the user to 'active'
         */

        if (!$user->save()) {
            $this->flashSession->warning('Une erreur est survenue. Le support ' . $this->config->application->siteTitle . ' en a été averti et est déjà en train de traiter le problème. Vous serez averti dès que le problème sera résolu.');

            $this->logger->error('Unable to save user model during an email confirmation', array('EmailConfirmation' => (string) $confirmation->getId()));
            
            return $this->response->redirect('index');
        }
        else{
            $confirmation->save();
        }

        /**
         * Identify the user in the application
         */
        $this->auth->authUserById($user->getId());

        /**
         * Check if the user must change his/her password
         */
        if ($user->getMustChangePassword()) {

            $this->flashSession->success('Votre email a bien été confirmé. Maintenant vous devez changer votre mot de passe');

            return $this->dispatcher->forward(array(
                'controller' => 'users',
                'action' => 'changePassword'
            ));
        }

        $this->flashSession->success('Votre email a bien été confirmé.');

        return $this->response->redirect('dashboard');
    }

    public function resetPasswordAction()
    {
        $code = $this->dispatcher->getParam('code');

        $resetPassword = ResetPasswords::findFirst(array(array('code' => $code)));
        if (!$resetPassword) {
            $this->flash->error('Ce lien de réinitialisation n\'est pas valide.');
            return $this->dispatcher->forward(array(
                'controller' => 'index',
                'action' => 'index'
            ));
        }

        if ($resetPassword->reset != 'N') {
            $this->flash->error('Votre mot de passe à déjà été changé avec ce lien de réinitialisation. Si vous avez oublié votre mot de passe, merci de faire une nouvelle demande de réinitialisation.');
            return $this->dispatcher->forward(array(
                'controller' => 'index',
                'action' => 'index'
            ));
        }

        $resetPassword->reset = 'Y';

        /**
         * Change the confirmation to 'reset'
         */
        if (!$resetPassword->save()) {

            foreach ($resetPassword->getMessages() as $message) {
                $this->flash->error($message);
            }

            return $this->dispatcher->forward(array(
                'controller' => 'index',
                'action' => 'index'
            ));
        }

        /**
         * Identify the user in the application
         */
        $this->auth->authUserById($resetPassword->user_id);

        //$this->flash->success('Vous pouvez maintenant définir votre nouveau mot de passe');

        return $this->response->redirect('users/changePassword');
    }
}
