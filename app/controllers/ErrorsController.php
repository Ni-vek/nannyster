<?php

namespace Nannyster\Controllers;

class ErrorsController extends ControllerBase
{

    public function notFoundAction()
    {
        $this->tag->prependTitle('404 Page non trouvée - ');
        $this->view->setVar('breadcrumbs', array('404 Page non trouvée' => array('last' => true)));
    }

    public function uncaughtExceptionAction()
    {
        $this->tag->prependTitle('500 Erreur interne - ');
        $this->view->setVar('breadcrumbs', array('500 Erreur interne' => array('last' => true)));
    }
    
    public function maintenanceAction(){
        $this->tag->prependTitle('Maintenance - ');
        $this->view->setRenderLevel(\Phalcon\Mvc\View::LEVEL_ACTION_VIEW);
    }

}
