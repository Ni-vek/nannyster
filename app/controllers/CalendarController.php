<?php

namespace Nannyster\Controllers;

use Phalcon\Mvc\View;
use Phalcon\Mvc\Controller;
use Phalcon\Mvc\Dispatcher;
use Nannyster\Auth\Auth;
use Nannyster\Models\Mailboxes;
use Nannyster\Models\Contracts;
use Nannyster\Models\Calendar;
use Nannyster\Forms\LoginForm;
use Nannyster\Forms\ForgotPasswordForm;

class CalendarController extends ControllerBase
{
	public function indexAction(){
		$this->assets->addJs('js/bootbox.min.js')
		             ->addJs('js/fullcalendar.min.js')
		             ->addJs('js/jquery-ui-1.10.3.custom.min.js')
		             ->addJs('js/jquery.ui.touch-punch.min.js')
		             ->addJs('js/bootbox.min.js')
		             ->addJs('js/bootstrap-wysiwyg.min.js')
		             ->addJs('js/calendar/index.js');
        $this->assets->addCss('css/fullcalendar.css');

		$this->tag->prependTitle('Planning - ');
      	$this->view->setVar('activeClass', 'calendar');
	    $this->view->setVar('breadcrumbs', array(
	        'Planning' => array(
	            'last' => true)
	    ));

	    $identity = $this->auth->getIdentity();
        $profile = $this->auth->getProfile();

	    if($profile->getName() != 'Parent'){
		    $contracts = Contracts::find(array(array(
		    	'nanny' => $this->auth->getId()))
		    );
		    $allowUpdate = true;
	    }
	    if($profile->getName() != 'Nourrice' && $identity->getCivility()->getShortName() == 'Mme'){
		    $contracts = Contracts::find(array(
					  array('mother' => $this->auth->getId())
				)
		    );
		    $allowUpdate = false;
	    }
	    if($profile->getName() != 'Nourrice' && $identity->getCivility()->getShortName() == 'Mr'){
		    $contracts = Contracts::find(array(
					  array('father' => $this->auth->getId())
				)
		    );
		    $allowUpdate = false;
	    }
	    $this->view->setVar('contracts', $contracts);

	}

	public function ajaxAddEventAction(){
		if($this->request->isPost()){
			if($this->request->isAjax()){
                $this->view->setRenderLevel(View::LEVEL_NO_RENDER);
                $data = $this->request->getPost();
                
                if(!self::validateMongoId($data['_id'])){
                	$data['_id'] = null;
                }

                $calendar = new Calendar();

                // Fields that need to be converted into MongoDate
                $array_mongo_object = array(
                    '_id' => '\\MongoId',
                    'creator_id' => '\\MongoId'
                );
                
                foreach ($data as $key => $val) {
                    if(!empty($val)){
                        if (array_key_exists($key, $array_mongo_object)) {
                            switch ($array_mongo_object[$key]) {
                                case '\\MongoDate':
                                    $data[$key] = new \MongoDate($val);
                                    break;
                                case '\\MongoId':
                                    if (self::validateMongoId($data[$key])) {
                                        $data[$key] = new \MongoId($data[$key]);
                                    }
                                    else {
                                        unset($data[$key]);
                                    }
                                    break;
                                default:
                                    $data[$key] = new $array_mongo_object[$key]($val);
                                    break;
                            }
                        }
                    }
                    else {
                        unset($data[$key]);
                    }
                }
                
                $calendar->hydrate($data);

                if($calendar->save()){
                	die($calendar->getId()); 
                }
				else{
					return $this->response->setStatusCode(400, 'NOK'); 
				}
			}
		}
	}

	public function ajaxCalendarRemoverAction(){
		if($this->request->isPost()){
			if($this->request->isAjax()){
				$this->view->setRenderLevel(View::LEVEL_NO_RENDER);
				$data = $this->request->getpost();
				$this->response->setHeader('Content-Type', 'application/json');

				if(self::validateMongoId($data['id'])){
					$calendar = Calendar::findById(new \MongoId($data['id']));

					if($calendar){
						if($calendar->delete()){
							echo '{"error" : false}';
							return true;
						}
					}
					else{
						echo '{"error" : true, "message" : "L\'évènement n\'existe pas!"}';
						return false;
					}
				}
				echo '{"error" : false, "message" : "Invalid Id"}';
			}
		}
		$this->response->redirect('calendar');
	}

	public function ajaxCalendarLockerUnlockerAction(){
		if($this->request->isPost()){
			if($this->request->isAjax()){
				$this->view->setRenderLevel(View::LEVEL_NO_RENDER);
				$data = $this->request->getpost();
				$this->response->setHeader('Content-Type', 'application/json');

				if(self::validateMongoId($data['_id'])){
					$calendar = Calendar::findById(new \MongoId($data['_id']));

					if($calendar){
						if($data['editable'] == 'false'){
							$valEditable = false;
						}
						if($data['editable'] == 'true'){
							$valEditable = true;
						}
						$calendar->editable = $valEditable;
						if($calendar->save()){
							echo '{"error" : false, "message" : "OK"}';
							return true;
						}
					}
					else{
						echo '{"error" : true, "message" : "L\'évènement n\'existe pas!"}';
						return false;
					}
				}
				echo '{"error" : false, "message" : "Invalid Id"}';
				return false;
			}
		}
		$this->response->redirect('calendar');
	}

	public function ajaxCalendarFinderAction(){
		if($this->request->isPost()){
			if($this->request->isAjax()){
				$this->view->setRenderLevel(View::LEVEL_NO_RENDER);
				$data = $this->request->getPost();
				$identity = $this->auth->getIdentity();
                $profile = $this->auth->getProfile();
			    if($profile->getName() == 'Nourrice'){
				    $contracts = Contracts::find(array(array(
				    	'nanny' => $this->auth->getId()))
				    );
			    }
			    if($profile->getName() != 'Nourrice' && $identity->getCivility()->getShortName() == 'Mme'){
				    $contracts = Contracts::find(array(
							  array('mother' => $this->auth->getId())
						)
				    );
			    }
			    if($profile->getName() != 'Nourrice' && $identity->getCivility()->getShortName() == 'Mr'){
				    $contracts = Contracts::find(array(
							  array('father' => $this->auth->getId())
						)
				    );
			    }

			    if($contracts){

			    	$contractsId = array();

			    	foreach ($contracts as $contract) {
			    		$contractsId[] = (string) $contract->getId();
			    	}

			    	$datas = Calendar::find(array(array(
						'contract_id' => array(
							'$in' => $contractsId
						),
						'start' => array('$gte' => (int) $data['start'], '$lte' => (int) $data['end']))
					));

					for ($i = 0; $i < sizeof($datas); $i++) { 
						$datas[$i]->_id = (string) $datas[$i]->_id;
					}
					$this->response->setHeader("Content-Type", "application/json");
					echo json_encode($datas);
					return true;
			    }
			    echo json_encode(array());
			    return false;

			}
		}
		$this->response->redirect('calendar');
	}
}