<?php

namespace Nannyster\Controllers;

use Phalcon\Mvc\View;
use Nannyster\Models\Conversations;
use Nannyster\Models\Mailboxes;
use Nannyster\Models\Messages;
use Nannyster\Models\Users;

class MailboxController extends ControllerBase
{

    public function indexAction()
    {
        $this->tag->prependTitle('Messagerie - ');
        $this->view->setVar('activeClass', 'mailbox');
        $this->view->setVar('breadcrumbs', array('Messagerie' => array('last' => true)));
        
        $this->assets->addJs('js/mailboxes/index.js');
        $this->assets->addJs('js/bootstrap-tag.min.js');
        $this->assets->addJs('js/jquery.hotkeys.min.js');
        $this->assets->addJs('js/bootstrap-wysiwyg.min.js');
        $this->assets->addJs('js/jquery-ui-1.10.3.custom.min.js');
        $this->assets->addJs('js/jquery.ui.touch-punch.min.js');
        $this->assets->addJs('js/jquery.slimscroll.min.js');
        $this->assets->addJs('js/bootbox.min.js');

        //find each "mailboxes" of this logged user
        $mailboxes = Mailboxes::find(array(
            array('user' => $this->auth->getId(),
                  'conversation_deleted' => false)
        ));

        //find "conversations" joined to these mailboxes
//        $mailboxesArray = array();
//        foreach ($mailboxes as $mailboxe) {
//            $mailboxesArray[] = $mailboxe->getConversation();
//        }
//        
        uasort($mailboxes, function ($a, $b){
            if($a->getConversation()->getLastMessageDate() == $b->getConversation()->getLastMessageDate()){
                return 0;
            }
            return ($a->getConversation()->getLastMessageDate() > $b->getConversation()->getLastMessageDate()) ? -1 : 1;
        });
//        $conversations = Conversations::find(array(
//            array('_id' => array(
//                '$in' => $mailboxesArray)),
//            'sort' => array('last_message_date' => -1)
//        ));
        $users = array();
        //finding each user and usefull informations in the conversations
        for ($i = 0; $i < sizeof($mailboxes); $i++) { 
            //define the conversation (subject and last_message_date)
            $users[(string) $mailboxes[$i]->getConversation()->getId()] = Mailboxes::find(array(array(
                'conversation' => $mailboxes[$i]->getConversation()->getId(),
                'user' => array(
                    '$nin' => array(
                        $this->auth->getId()
                    )
                )
            )));
//            $myConversations[$i]['conversation'] = $conversations[$i];
//
//            //find all users of the conversation except me
//            $users = Mailboxes::find(array(
//                array('conversation_id' => $conversations[$i]->_id,
//                    'user_id' => 
//                        array('$nin' => array($this->auth->getId()))
//            )));

//            //define the mailbox for me for unread_message var
//            $myConversations[$i]['mailbox'] = Mailboxes::find(array(
//                array('conversation_id' => $conversations[$i]->_id,
//                    'user_id' => $this->auth->getId())));
//
//            //for each users, find their informations
//            foreach ($users as $val) {
//                $myConversations[$i]['users'][] = Users::findFirst(array(
//                    array('_id' => $val->user_id)));
//            }
        }
        $this->view->setVar('mailboxes', $mailboxes);
        $this->view->setVar('users', $users);
    }

    public function ajaxSendNewMailAction(){
        if($this->request->isPost()){
            if($this->request->isAjax()){
                $this->view->setRenderLevel(View::LEVEL_NO_RENDER);
                $data = $this->request->getPost();

                //Create a new conversation and save it, retrieving new MongoId from DB
                $conversation = new Conversations();
                $conversation->setSubject(htmlspecialchars($data['subject'][0]))
                             ->setLastMessageDate(new \MongoDate())
                             ->save();

                //Create a new message and save it
                $message = new Messages();
                $message->setUser($this->auth->getIdentity())
                        ->setConversation($conversation)
                        ->setMessage($data['message'][0])
                        ->setDate(new \MongoDate())
                        ->save();

                //Create a new mailboxe for each user concern by the message
                foreach ($data['to'] as $value) {
                    $name = explode('-', $value);
                    $mailbox = new Mailboxes();
                    $mailbox->setUser($userTo = Users::findById(new \MongoId($name[0])))
                            ->setConversation($conversation)
                            ->setUnreadMessages(true)
                            ->setConversationDeleted(false);
                    if($mailbox->save()){
                        if($userTo->getEmailNotifications()){
                            $this->mail->send(
                                array(
                                    'email' => $userTo->getEmail(),
                                    'name' => $userTo->getFormatedName(),
                                    'type' => 'to'
                                ),
                                'Nouveau message de '.$this->auth->getIdentity()->getFormatedName(),
                                'newMessage',
                                array(
                                    'mailboxUrl' => '/mailbox',
                                    'from' => $this->auth->getIdentity()->getFormatedName(),
                                    'subject' => $conversation->getSubject()
                                ),
                                'Nouvelle reponse'
                            );
                        }
                    }
                }

                //And finally create and save a new mailbox for the sender
                $mailbox = new Mailboxes();
                $mailbox->setUser($this->auth->getIdentity())
                        ->setConversation($conversation)
                        ->setUnreadMessages(false)
                        ->setConversationDeleted(false)
                        ->save();

                //Create a new line to return to message list
                echo '<div class="message-item" data-id="'.$conversation->getId().'">
                            <label class="inline" id="label-emails">
                                <input type="checkbox" class="ace" />
                                <span class="lbl"></span>
                            </label>
                            <span class="sender">';

                //Insert in users in previous line
                if(count($data['to']) == 1){
                    $name = explode('-', $data['to'][0], 2);
                    echo $name[1];
                }
                else {
                    $userFirst = explode('-', $data['to'][0], 2);
                    $tooltip = '';
                    for ($i = 1; $i < count($data['to']); $i++) {
                        $user = explode('-', $data['to'][$i], 2);
                        $tooltip .= $user[1] . '<br/>';
                    }
                    echo $userFirst[1] . ' et <a title data-rel="tooltipAjax" data-original-title="' . $tooltip . '">' . (count($data['to']) - 1) . ' autre' . ((count($data['to']) - 1) > 1 ? 's' : '') . '</a>';
                }

                echo   '</span>
                            <div class="time">
                                <i class="icon-time orange"></i>
                                <span class="timeago" title="' . date('Y-m-d\TH:i:s') . '">
                                    ' . date('d-m-Y \à H:i') . '
                                </span>
                            </div>
                            <span class="summary">
                                <span class="text">' . $conversation->getSubject() . '</span>
                            </span>
                        </div>
                        <script type="text/javascript">
                            $(\'[data-rel="tooltipAjax"]\').tooltip({html: true});
                            // French shortened
                            jQuery.timeago.settings.strings = {
                                prefixAgo: "il y a",
                                prefixFromNow: "d\'ici",
                                seconds: "moins d\'une minute",
                                minute: "une minute",
                                minutes: "%d minutes",
                                hour: "une heure",
                                hours: "%d heures",
                                day: "un jour",
                                days: "%d jours",
                                month: "un mois",
                                months: "%d mois",
                                year: "un an",
                                years: "%d ans"
                            };
                            $(\'.timeago\').timeago();
                        </script>';

                return true;
            }
        }
        $this->response->redirect('mailbox');
    }

    public function ajaxMessageViewAction(){
        if($this->request->isPost()){
            if($this->request->isAjax()){
                $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
                $data = $this->request->getPost();

                //Convert $data['id'] into a MongoId Object
                $data['id'] = new \MongoId($data['id']);
                
                //Find the conversation's subject
                $conversation = Conversations::findById($data['id']);

                //Find all messages from the conversation
//                $conversation['messages'] = Messages::find(array(array(
//                    'conversation_id' => $data['id']),
//                    'sort' => array('date' => -1)
//                ));

                

                //Find all contributors mailboxes from the conversation (except me)
                $mailboxes = Mailboxes::find(array(array(
                    'conversation' => $data['id'],
                    'user' => array(
                        '$nin' => array($this->auth->getId())))
                ));

                //Find my mailboxe from the conversation
                $myMailbox = Mailboxes::findFirst(array(array(
                    'conversation' => $data['id'],
                    'user' => $this->auth->getId())
                ));

                //Find full identity for the previous
//                for ($i = 0; $i < sizeof($mailboxes); $i++) { 
//                    $users[$i] = Users::findFirst(array(array(
//                        '_id' => $conversation['contributors'][$i]->user_id)));
//                }

                //Check if we have add js part to remove each informations of the unread message
                if($myMailbox->getUnreadMessages()){
                    $addJS = true;
                    $myMailbox->setUnreadMessages(false)->save();
                }
                else{
                    $addJS = false;
                }

                //Pass $conversation ans $addJS array to view
                $this->view->setVar('addJS', $addJS);
                $this->view->setVar('conversation', $conversation);
                $this->view->setVar('mailboxes', $mailboxes);
                $this->view->setVar('myMailbox', $myMailbox);
                return true;
            }
        }
        $this->response->redirect('mailbox');
    }

    public function ajaxMessageDeleterAction(){
        if($this->request->isPost()){
            if($this->request->isAjax()){
                $this->view->setRenderLevel(View::LEVEL_NO_RENDER);
                $data = $this->request->getPost();

                //Find mailbox
                $mailbox = Mailboxes::findFirst(array(array(
                    'conversation' => new \MongoId($data['id']),
                    'user' => $this->auth->getId())
                ));

                //Define mailbox as deleted
                $mailbox->setConversationDeleted(true);

                //Saving mailbox
                if($mailbox->save()){
                    return true;
                }
                return false;
            }
        }
        $this->response->redirect('mailbox');
    }

    public function ajaxMutipleRemoverAction(){
        if($this->request->isPost()){
            if($this->request->isAjax()){
                $this->view->setRenderLevel(View::LEVEL_NO_RENDER);
                $data = $this->request->getPost();

                //Find each conversation selected to remove
                foreach ($data['conversations'] as $key) {
                    //Find mailbox
                    $mailbox = Mailboxes::findFirst(array(array(
                        'conversation' => new \MongoId($key),
                        'user' => $this->auth->getId())
                    ));

                    //Define mailbox as deleted
                    $mailbox->setConversationDeleted(true);

                    //Saving mailbox
                    $mailbox->save();
                }
                return true;
            }
        }
    }

    public function ajaxMailboxResponseSenderAction(){
        if($this->request->isPost()){
            if($this->request->isAjax()){
                $this->view->setRenderLevel(View::LEVEL_NO_RENDER);
                $data = $this->request->getPost();

                //Create a new message ans save it
                $message = new Messages();
                $message->setUser($this->auth->getIdentity())
                        ->setConversation($conversation = Conversations::findById(new \MongoId($data['id'])))
                        ->setMessage($data['message'])
                        ->setDate(new \MongoDate())
                        ->save();

                //Update conversation, set new last_message_date
                $conversation->setLastMessageDate(new \MongoDate())
                             ->save();

                //Update each user mailbox
                //Find each mailbox
                $mailboxes = Mailboxes::find(array(array(
                    'conversation' => new \MongoId($data['id']),
                    'user' => array(
                        '$nin' => array($this->auth->getId())
                    ))
                ));

                //Update each mailboxsave it
                foreach ($mailboxes as $mailbox) {
                    $mailbox->setUnreadMessages(true);
                    if($mailbox->save()){
                        $userTo = $mailbox->getUser();
                        if($userTo->getEmailNotifications() && $mailbox->getUnreadMessages() === true){
                            $this->mail->send(
                                array(
                                    'email' => $userTo->getEmail(),
                                    'name' => $userTo->getFormatedName(),
                                    'type' => 'to'
                                ),
                                'Nouvelle réponse de '.$this->auth->getIdentity()->getFormatedName(),
                                'newResponse',
                                array(
                                    'mailboxUrl' => '/mailbox',
                                    'from' => $this->auth->getIdentity()->getFormatedName(),
                                    'subject' => $conversation->getSubject()
                                ),
                                'Nouvelle reponse'
                            );
                        }
                    }
                }
                
                
                echo '<div class="itemdiv dialogdiv">
                          <div class="user">
                            <img alt="'.$this->auth->getIdentity()->getFormatedName().'" src="/img/avatars/thumbs/'.$this->auth->getIdentity()->getAvatarFile().'" />
                          </div>

                          <div class="body">
                            <div class="time">
                              <i class="icon-time orange"></i>
                              <span class="timeago" title="'.date('Y-m-d\TH:i:s', $message->getDate()).'">'.self::returnDateTime($message->getDate()).'</span>
                            </div>

                            <div class="name"><a href="/members/users/view/'.(string) $this->auth->getId().'">'.$this->auth->getIdentity()->getFormatedName().'</a></div>
                            <div class="text">'.$message->message.'</div>
                            <!--
                            <div class="tools">
                              <a href="#" class="btn btn-minier btn-info">
                                <i class="icon-only icon-share-alt"></i>
                              </a>
                            </div>-->
                          </div>
                        </div>
                        <script type="text/javascript">$(\'.timeago\').timeago();</script>';
                return true;
            }
        }
        $this->response->redirect('mailbox');
    }

    public function quickAction(){
        $id = $this->dispatcher->getParam('id');
        $name = $this->dispatcher->getParam('name');

        die($name.' : '.$id);
    }
}

