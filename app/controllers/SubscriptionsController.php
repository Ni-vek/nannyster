<?php

namespace Nannyster\Controllers;

use Nannyster\Models\SubscriptionsBase;

class SubscriptionsController extends ControllerBase
{

    public function indexAction()
    {
        $this->tag->prependTitle('Abonnements - ');
        $this->view->setVar('breadcrumbs', array(
            'Abonnements' => array(
                'last' => true)
        ));
        $this->view->setVar('activeClass', 'subscriptions');
        $this->view->setVar('openTab', 'account');
    }

    public function manageAction()
    {
        $this->tag->prependTitle('Subscriptions Manager - ');
        $this->view->setVar('breadcrumbs', array(
            'Subscriptions Manager' => array(
                'last' => true)
        ));
        $this->view->setVar('activeClass', 'subscriptions');
        $this->view->setVar('openTab', 'account');
        $this->assets->addJs('js/jquery.dataTables.min.js');
        $this->assets->addJs('js/jquery.dataTables.bootstrap.js');
        $this->assets->addJs('js/bootbox.min.js');
        $this->assets->addJs('js/subscriptions/manage.js');

        $this->view->setVar('subscriptions', SubscriptionsBase::find());
    }

    public function buyAction($id)
    {
        $this->tag->prependTitle('Achat d\'abonnement - ');
        $this->view->setVar('breadcrumbs', array(
            'Abonnements' => array(
                'controller' => 'subscriptions',
                'action' => 'index'),
            'Achat' => array(
                'last' => true)
        ));
        $this->assets->addJs('js/subscriptions/buy.js')
                ->addJs('js/jquery.validate.min.js')
                ->addJs('js/additional-methods.min.js')
                ->addJs('js/bootbox.min.js');
        $this->view->setVar('activeClass', 'subscriptions');
        $this->view->setVar('openTab', 'account');
        if (self::validateMongoId($id)) {

            $subscription = SubscriptionsBase::findById(new \MongoId($id));

            if ($subscription) {
                $this->view->subscription = $subscription;
            }
            else {
                $this->flash->error('L\'identifiant de l\'abonnement n\'existe pas.');
                return $this->response->redirect('subscriptions');
            }
        }
        else {
            $this->flash->error('L\'identifiant de l\'abonnement n\'est pas valide.');
            return $this->response->redirect('subscriptions');
        }
    }

    public function editAction($id)
    {
        
    }

    public function createAction()
    {
        
    }

    public function deleteAction($id)
    {
        
    }

}
