<?php

namespace Nannyster\Controllers;

class GalleryController extends ControllerBase
{
	public function indexAction(){
		$this->tag->prependTitle('Galerie - ');
      	$this->view->setVar('activeClass', 'gallery');
	    $this->view->setVar('breadcrumbs', array(
	        'Galerie' => array(
	            'last' => true)
	    ));
		$this->assets->addJs('js/jquery.colorbox-min.js');
		$this->assets->addJs('js/gallery/index.js');
		$this->assets->addCss('css/colorbox.css');
	}
}