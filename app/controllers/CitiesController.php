<?php

namespace Nannyster\Controllers;

use Phalcon\Http\Response;
use Nannyster\Models\Cities;

class CitiesController extends ControllerBase
{

    public function ajaxCitiesFinderAction()
    {
        if ($this->request->isAjax()) {
            $this->view->disable();
            $datas = $this->request->getPost();

            $cities = Cities::find(array(
                        array(
                            'slug' => array('$regex' => \Nannyster\Utils\Slug::generate($datas['data']['q']))),
                        '$order' => array('real_name' => 1)
            ));
            $cities_array = array(
                'q' => $datas['data']['q'],
                'results' => array());
            if ($cities) {
                foreach ($cities as $city) {
                    $cities_array['results'][(string) $city->getId()] = $city->getRealName() . ' (' . $city->getZipCode() . ')';
                }
            }

            //Create a response instance
            $response = new Response();

            //Set the content of the response
            $response->setContent(json_encode($cities_array));

            //Return the response
            return $response;
        }
        else {
            $this->flash->warning('Requête non autorisée!');
            $this->response->redirect('dashboard');
        }
    }

}
