<?php

namespace Nannyster\Controllers;

use Phalcon\Mvc\View;
use Nannyster\Models\SocialCotis;
use Nannyster\Models\Contracts;
use Nannyster\Models\Users;
use Nannyster\Pdf\Pdf;

class ContractsController extends ControllerBase
{

    public function indexAction()
    {
        $this->assets->addJs('js/contracts/index.js');
        $this->assets->addJs('js/bootbox.min.js');
        $this->tag->prependTitle('Contrats - ');
        $this->view->setVar('activeClass', 'contracts');
        $this->view->setVar('breadcrumbs', array(
            'Contrats' => array(
                'last' => true)
        ));

        $user = $this->auth->getIdentity();

        if ($user->getProfile()->getName() === 'Nourrice') {
            $contracts = Contracts::find(array(array(
                            'nanny' => $this->auth->getId()))
            );
            $allowUpdate = true;
        }
        if ($user->getProfile()->getName() != 'Nourrice' && $user->getCivility()->getShortName() == 'Mme') {
            $contracts = Contracts::find(array(
                        array('mother' => $this->auth->getId())
                            )
            );
            $allowUpdate = false;
        }
        if ($user->getProfile()->getName() != 'Nourrice' && $user->getCivility()->getShortName() == 'Mr') {
            $contracts = Contracts::find(array(
                        array('father' => $this->auth->getId())
                            )
            );
            $allowUpdate = false;
        }
        $this->view->setVar('contracts', $contracts);
    }

    public function deleteAction($id)
    {
        if ($id != null) {
            if (self::validateMongoId($id)) {
                $contract = Contracts::findById(new \MongoId($id));

                if ($contract) {
                    if ($contract->delete()) {
                        $this->flash->success('Le contrat a bien été supprimé');
                    }
                    else {
                        $this->flash->error('Aucun contrat trouvé');
                    }
                }
                else {
                    $this->flash->error('Aucun contrat trouvé');
                }
                return $this->response->redirect('contracts/index');
            }
        }
        $this->flash->error('Le contrat n\'existe pas!');
        $this->response->redirect('contracts/index');
    }

    public function ajaxCreateAction()
    {
        if ($this->request->isPost()) {
            if ($this->request->isAjax()) {
                $data = $this->request->getPost();

                //Disable view render
                $this->view->setRenderLevel(View::LEVEL_NO_RENDER);

                //Create new contract
                $contract = new Contracts();

                //Unset _id if empty
                if (empty($data['_id'])) {
                    unset($data['_id']);
                }

                // Fields that need to be converted into MongoDate
                $array_mongo_object = array(
                    'child_birth_date' => '\\MongoDate',
                    'contract_start_date' => '\\MongoDate',
                    '_id' => '\\MongoId',
                    'mother' => '\\MongoId',
                    'father' => '\\MongoId',
                    'nanny' => '\\MongoId'
                );

                // Public holidays
                $public_holidays = array('premier_janvier', 'paques', 'huit_mai', 'ascension', 'pentecote', 'quatorze_juillet', 'quinze_aout', 'toussaint', 'onze_nov', 'noel');

                // Field to remove
                $array_to_remove = array('breakfast_costs_value', 'lunch_costs_value', 'snack_costs_value', 'diner_costs_value');

                foreach ($data as $key => $val) {
                    if (!empty($val)) {
                        if (array_key_exists($key, $array_mongo_object)) {
                            switch ($array_mongo_object[$key]) {
                                case '\\MongoDate':
                                    $date = explode('/', $data[$key]);
                                    $data[$key] = new \MongoDate(strtotime($date[2] . '-' . $date[1] . '-' . $date[0]));
                                    break;
                                case '\\MongoId':
                                    if (self::validateMongoId($data[$key])) {
                                        $data[$key] = new \MongoId($data[$key]);
                                    }
                                    else {
                                        unset($data[$key]);
                                    }
                                    break;
                                default:
                                    $data[$key] = new $array_mongo_object[$key]($val);
                                    break;
                            }
                        }
                        elseif (in_array($key, $public_holidays)) {
                            if ($data[$key] === 'on') {
                                $data[$key] = true;
                            }
                            else {
                                $data[$key] = false;
                            }
                        }
                        elseif (in_array($key, $array_to_remove)) {
                            unset($data[$key]);
                        }
                    }
                    else {
                        unset($data[$key]);
                    }
                }

                if (!empty($data['mother']) && self::validateMongoId($data['mother'])) {
                    $user_object = Users::findById(new \MongoId($data['mother']));
                    if ($user_object) {
                        $data['mother_object'] = $user_object->fetchForSession();
                    }
                    else {
                        unset($data['mother']);
                    }
                }

                if (!empty($data['father']) && self::validateMongoId($data['father'])) {
                    $user_object = Users::findById(new \MongoId($data['father']));
                    if ($user_object) {
                        $data['father_object'] = $user_object->fetchForSession();
                    }
                    else {
                        unset($data['father']);
                    }
                }

                if (!empty($data['nanny']) && self::validateMongoId($data['nanny'])) {
                    $user_object = Users::findById(new \MongoId($data['nanny']));
                    if ($user_object) {
                        $data['nanny_object'] = $user_object->fetchForSession();
                    }
                    else {
                        unset($data['nanny']);
                    }
                }
                //Hydrate object
                $contract->hydrate($data);

                if ($contract->save()) {
                    $this->response->setStatusCode(200, 'OK');
                    die($contract->getId());
                }
                else {
                    $this->response->setStatusCode(500, 'NOK');
                    die('Une erreur est survenue');
                }
            }
        }
        $this->response->redirect('contracts/create');
    }

    public function viewAction()
    {
        $this->tag->prependTitle('Contrat avec ##### Evan ##### - ');
        if ($this->auth->getProfile() != 'Parent') {
            $this->view->setVar('openTab', 'contracts');
            $this->view->setVar('activeClass', 'view-contracts');
        }
        else {
            $this->view->setVar('activeClass', 'contracts');
        }
        $this->view->setVar('breadcrumbs', array(
            'Contrats' => array(
                'controller' => 'contracts',
                'action' => 'index'),
            'Contrat avec ##### Evan #####' => array(
                'last' => true)
        ));
    }

    public function createAction()
    {
        $this->tag->prependTitle('Créer un contrat - ');
        $this->view->setVar('activeClass', 'contracts');
        $this->view->setVar('breadcrumbs', array(
            'Contrats' => array(
                'controller' => 'contracts',
                'action' => 'index'),
            'Créer un contrat' => array(
                'last' => true)
        ));
        $user = $this->auth->getIdentity();
        if ($user->getAddress()->getCity()) {
            $dept = $user->getAddress()->getCity()->getDepartement();
            if ($dept == 57 || $dept == 67 || $dept == 68) {
                $cotisations = SocialCotis::find(array(array(
                                'type' => array('$nin' => array('general'))
                )));
            }
        }
        else {
            $cotisations = SocialCotis::find(array(array(
                            'type' => array('$nin' => array('alsace'))
            )));
        }
        $this->view->setVar('cotisations', $cotisations);

        for ($i = 0; $i < sizeof($cotisations); $i++) {
            $cotisationsJson[$i]['base'] = $cotisations[$i]->getBase();
            $cotisationsJson[$i]['taux'] = $cotisations[$i]->getEmployeeRate();
        }

        $this->view->setVar('cotisationsJson', json_encode($cotisationsJson));

        $this->assets->addJs('js/contracts/create.js')
                ->addJs('js/fuelux/fuelux.wizard.min.js')
                ->addJs('js/fuelux/fuelux.spinner.min.js')
                ->addJs('js/jquery.validate.min.js')
                ->addJs('js/additional-methods.min.js')
                ->addJs('js/jquery.maskedinput.min.js')
                ->addJs('js/chosen.jquery.js')
                ->addJs('js/chosen.ajaxaddition.jquery.js')
                ->addJs('js/bootbox.min.js')
                ->addJs('js/bootstrap-wysiwyg.min.js')
                ->addJs('js/jquery.hotkeys.min.js')
                ->addJs('js/jquery.slimscroll.min.js')
                ->addJs('js/jquery.ui.touch-punch.min.js')
                ->addCss('css/jquery-ui-1.10.3.custom.min.css')
                ->addCss('css/chosen.css');
    }

    public function printAction($contract_id)
    {
        if (\Nannyster\Controllers\ControllerBase::validateMongoId($contract_id)) {
            $contract = Contracts::findById(new \MongoId($contract_id));
            if ($contract) {
                if (($contract->getMother() && $contract->getMother()->getId() == $this->auth->getId()) ||
                        ($contract->getFather() && $contract->getFather()->getId() == $this->auth->getId()) ||
                        ($contract->getNanny() && $contract->getNanny()->getId() == $this->auth->getId())) {

                    $this->view->disable();

                    $html = $this->view->getRender('contracts', 'print', array('contract' => $contract));
                    $pdf_name = 'Contrat ' . ucfirst($contract->getChildFirstname()) . ' ' . strtoupper($contract->getChildSurname()) . ' - ' . $contract->getNanny()->getFormatedName();

                    $pdf = new Pdf();
                    $pdf->createPdf($html, $pdf_name);
                }
                else {
                    $this->flash->error('Le contrat à imprimer n\'existe pas! lablalb');
                    $this->dispatcher->forward(array('controller' => 'contracts', 'action' => 'index'));
                }
            }
            else {
                $this->flash->error('Le contrat à imprimer n\'existe pas!');
                $this->dispatcher->forward(array('controller' => 'contracts', 'action' => 'index'));
            }
        }
        else {
            $this->flash->error('Le numéro de contrat à imprimer n\'est pas valide!');
            $this->dispatcher->forward(array('controller' => 'contracts', 'action' => 'index'));
        }
    }

    public function downloadAction($contract_id)
    {
        if (\Nannyster\Controllers\ControllerBase::validateMongoId($contract_id)) {
            $contract = Contracts::findById(new \MongoId($contract_id));
            if ($contract) {
                if ($contract->getMother()->getId() == $this->auth->getId() ||
                        $contract->getFather()->getId() == $this->auth->getId() ||
                        $contract->getNanny()->getId() == $this->auth->getId()) {

                    $this->view->disable();

                    $html = $this->view->getRender('contracts', 'print', array('contract' => $contract));
                    $pdf_name = 'Contrat ' . ucfirst($contract->getChildFirstname()) . ' ' . strtoupper($contract->getChildSurname()) . ' - ' . $contract->getNanny()->getFormatedName();

                    $pdf = new Pdf();
                    $pdf->downloadPdf($html, $pdf_name);
                }
                else {
                    $this->flash->error('Le contrat à imprimer n\'existe pas! lablalb');
                    $this->dispatcher->forward(array('controller' => 'contracts', 'action' => 'index'));
                }
            }
            else {
                $this->flash->error('Le contrat à imprimer n\'existe pas!');
                $this->dispatcher->forward(array('controller' => 'contracts', 'action' => 'index'));
            }
        }
        else {
            $this->flash->error('Le numéro de contrat à imprimer n\'est pas valide!');
            $this->dispatcher->forward(array('controller' => 'contracts', 'action' => 'index'));
        }
    }

    public static function contractFieldsGenerator($data, $type = 'text')
    {
        if (!empty($data)) {
            return $data;
        }
        else {
            switch ($type) {
                case 'text':
                    return '.......................................................................';
                case 'textarea':
                    return '...............................................................................................................................................................<br>'
                            . '...............................................................................................................................................................<br>'
                            . '...............................................................................................................................................................<br>'
                            . '...............................................................................................................................................................<br>'
                            . '...............................................................................................................................................................<br>'
                            . '...............................................................................................................................................................';
                case 'textlittle':
                    return '.................................';
                case 'address':
                    return '..............................................................................................................................................';
                case 'phone':
                    return '|___|___|___|___|___|___|___|___|___|___|';
                case 'date':
                    return '|___|___|   |___|___|   |___|___|___|___|';
                case 'socialsecu':
                    return '|___|   |___|___|   |___|___|   |___|___|   |___|___|___|   |___|___|___|   |___|___|';
                default:
                    return '.......................................................................';
            }
        }
    }

}
