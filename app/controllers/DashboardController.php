<?php

namespace Nannyster\Controllers;

use Nannyster\Utils\SalaryCalculator;

class DashboardController extends ControllerBase
{

    public function indexAction()
    {
        $this->tag->prependTitle('Tableau de bord - ');
        $this->view->setVar('activeClass', 'dashboard');
        
        $sc = new SalaryCalculator($this->auth->getIdentity());
        
	    $this->view->setVar('contracts', $sc->getContracts());
//	    $this->view->setVar('allContracts', $allContracts);
	    $this->view->setVar('nbChildren', $sc->getNbChildren());
	    $this->view->setVar('todayCalendars', $sc->getTodayCalendars());
	    $this->view->setVar('timeGard', $sc->getTimeGard());
	    $this->view->setVar('curentTimeGard', $sc->getCurentTimeGard());
	    $this->view->setVar('indemnityCost', $sc->getIndemnityCost());
	    $this->view->setVar('currentIndemnityCost', $sc->getCurrentIndemnityCost());
	    $this->view->setVar('salary', $sc->getSalary());
	    $this->view->setVar('monthSalary', $sc->getMonthSalary());
    }

}

