<?php

namespace Nannyster\Controllers;

class FaqController extends ControllerBase
{

	public function indexAction(){
        $this->tag->prependTitle('FAQ - ');
        $this->view->setVar('activeClass', 'faq');
        $this->view->setVar('breadcrumbs', array('FAQ' => array('last' => true)));

	}
}