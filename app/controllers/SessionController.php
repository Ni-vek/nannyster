<?php

namespace Nannyster\Controllers;

use Phalcon\Mvc\View;
use Nannyster\Forms\LoginForm;
use Nannyster\Forms\SignUpForm;
use Nannyster\Forms\ForgotPasswordForm;
use Nannyster\Models\Users;
use Nannyster\Models\ResetPasswords;
use Nannyster\Models\Profiles;
use Nannyster\Models\Civilities;

class SessionController extends ControllerBase
{

    /**
     * Login a user
     */
    public function loginAction()
    {
        $this->tag->prependTitle('Connexion - ');
        $this->view->setVar('activeClass', 'login');
        $this->view->setVar('breadcrumbs', array('Connexion' => array('last' => true)));
        $loginForm = new LoginForm();
        $passwordForm = new ForgotPasswordForm();
        $signupForm = new SignUpForm();
        if ($this->request->isPost()) {
            try {
                //Request is not valid
                if (!$loginForm->isValid($this->request->getPost())) {
                    $error = '<ul>';
                    foreach ($loginForm->getMessages() as $message) {
                        $error.= '<li>' . $message . '</li>';
                    }
                    $error .= '</ul>';
                    $this->flash->error($error);
                }
                // Request is valid
                else {
                    $this->auth->check(array(
                        'email' => $this->request->getPost('email'),
                        'password' => $this->request->getPost('password'),
                        'remember' => $this->request->getPost('remember')
                    ));
                    //Redirection to the change password page if needed
                    if ($this->auth->getIdentity()->getMustChangePassword()) {
                        return $this->response->redirect('users/changePassword');
                    }
                    //Classical redirection after login
                    elseif(strpos($this->request->getHTTPReferer(), '/session/') !== false){
                        return $this->response->redirect('dashboard');
                    }
                    //Redirection after login on a specific page that have been called before login
                    else{
                        return $this->response->redirect($this->request->getHTTPReferer());
                    }
                }
            }
            catch (\Exception $e) {
                $this->flash->error($e->getMessage());
            }
        }
        $this->view->loginForm = $loginForm;
        $this->view->passwordForm = $passwordForm;
        $this->view->signupForm = $signupForm;
    }

    /**
     * Allow a user to signup to the system
     */
    public function signupAction()
    {
        $this->tag->prependTitle('Connexion - ');
        $this->view->setVar('activeClass', 'login');
        $this->view->setVar('breadcrumbs', array('Connexion' => array('last' => true)));
        $loginForm = new LoginForm();
        $passwordForm = new ForgotPasswordForm();
        $signupForm = new SignUpForm();
        if ($this->request->isPost()) {
            //Form is valid
            if (!$signupForm->isValid($this->request->getPost())) {
                $error = '<ul>';
                foreach ($signupForm->getMessages() as $message) {
                    $error.= '<li>' . $message . '</li>';
                }
                $error .= '</ul>';
                $this->flash->error($error);
            }
            else {
//                $answer = \Nannyster\Recaptcha\Recaptcha::check(
//                    '6Ld3pfASAAAAAC-7-IMK9AE2fKly6PFqBx-t2lEJ',
//                    $_SERVER['REMOTE_ADDR'],
//                    $this->request->getPost('recaptcha_challenge_field'),
//                    $this->request->getPost('recaptcha_response_field')
//                );
//                if ($answer) {
//                
                //Hydrates this new user with post data
                $datas = $this->request->getPost();
                $datas['password'] = $this->security->hash($datas['password']);
                $datas['profile'] = Profiles::findById(new \MongoId($datas['profile']));
                $datas['civility'] = Civilities::findById(new \MongoId($datas['civility']));
                unset($datas['confirm_password']);
                unset($datas['recaptcha_response_field']);
                unset($datas['recaptcha_challenge_field']);
                unset($datas['csrfSignUp']);

                $user = new Users();
                $user->hydrate($datas);

                if ($user->save()) {
                    return $this->response->redirect('index');
                }
                $this->flashSession->error($user->getMessages());
                return $this->dispatcher->forward(array('controller' => 'index', 'action' => 'index'));
//                } else {
//                    $this->flash->error('Les mots de vérification ne correspondent pas avec ceux de l\'image');
//                }
            }
        }
        $this->view->loginForm = $loginForm;
        $this->view->passwordForm = $passwordForm;
        $this->view->signupForm = $signupForm;
    }

    /**
     * Shows the forgot password form
     */
    public function passwordAction()
    {
        $this->tag->prependTitle('Connexion - ');
        $this->view->setVar('activeClass', 'login');
        $this->view->setVar('breadcrumbs', array('Connexion' => array('last' => true)));
        $loginForm = new LoginForm();
        $passwordForm = new ForgotPasswordForm();
        $signupForm = new SignUpForm();

        if ($this->request->isPost()) {

            if ($passwordForm->isValid($this->request->getPost()) == false) {
                foreach ($passwordForm->getMessages() as $message) {
                    $this->flash->error($message);
                }
            }
            else {

                $user = Users::findFirst(array(array('email' => $this->request->getPost('email'))));
                if (!$user) {
                    $this->flash->error('Il n\'y a aucun compte associé à cet email');
                    return $this->dispatcher->forward(array('controller' => 'index', 'action' => 'index'));
                }
                else {

                    $resetPassword = new ResetPasswords();

                    // Generate a random confirmation code
                    $resetPassword->code = preg_replace('/[^a-zA-Z0-9]/', '', base64_encode(openssl_random_pseudo_bytes(24)));

                    $resetPassword->user_id = $user->_id;
                    if ($resetPassword->save()) {
                        $this->mail->send(
                                array(
                            'email' => $user->email,
                            'name' => ucwords($user->firstname) . ' ' . (($user->wedding_surname != null) ? strtoupper($user->wedding_surname) : strtoupper($user->surname)),
                            'type' => 'to'
                                ), 'Réinitialisation de votre mot de passe', 'reset', array('resetUrl' => '/reset-password/' . $resetPassword->code . '/' . $user->email), 'Réinitialisation du mot de passe'
                        );
                        $this->flash->notice('Les informations pour réinitialiser votre mot de passe ont été envoyée à l\'adresse ' . $user->email);
                        return $this->response->redirect('index');
                    }
                    else {
                        foreach ($resetPassword->getMessages() as $message) {
                            $this->flash->error($message);
                        }
                    }
                }
            }
        }

        $this->view->loginForm = $loginForm;
        $this->view->passwordForm = $passwordForm;
        $this->view->signupForm = $signupForm;
    }

    /**
     * Closes the session
     */
    public function logoutAction()
    {
        $this->auth->remove();
        return $this->response->redirect('index');
    }

    /**
     * Lock the session
     */
    public function lockAction()
    {
        if ($this->auth->isLocked()) {
            if ($this->auth->getIdentity() !== null) {
                $this->auth->lock();
            }
            $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
            $this->view->setVar('cookie', unserialize($this->cookies->get('NSC')->getValue()));
        }
        else {
            $this->response->redirect('index');
        }
    }

    /**
     * Unlock a session
     */
    public function unlockAction()
    {
        $this->auth->remove();
        return $this->response->redirect('index');
    }

    /**
     * Check if session is alive
     */
    public function checkAction()
    {
        if ($this->session->has('auth-identity')) {
            die('1');
        }
        else {
            die('0');
        }
    }

}
