<?php

namespace Nannyster\Controllers;

use Nannyster\Models\Invoices;
use Nannyster\Pdf\Pdf;

class InvoicesController extends ControllerBase
{

    public function indexAction()
    {
        $this->tag->prependTitle('Factures - ');
        $this->view->setVar('breadcrumbs', array(
            'Factures' => array(
                'last' => true)
        ));
        $this->assets->addJs('js/jquery.dataTables.min.js');
        $this->assets->addJs('js/jquery.dataTables.bootstrap.js');
//        $this->assets->addJs('js/invoices/index.js');
        $this->view->setVar('activeClass', 'invoices');
        $this->view->setVar('openTab', 'account');
        $this->view->invoices = Invoices::find(array(array(
            'user_id' => $this->auth->getId()
        )));
    }

    public function printAction($id)
    {
        $this->view->setVar('activeClass', 'invoices');
        $this->view->setVar('openTab', 'account');

        if (parent::validateMongoId($id)) {
            $invoice = Invoices::findById(new \MongoId($id));
            if ($invoice) {
                if ($invoice->getUserId() == $this->auth->getId()) {
                    
                    $this->view->disable();

                    $html = $this->view->getRender('invoices', 'print', array('invoice' => $invoice));
                    $pdf_name = 'Facture #' . ((string) $invoice->getId()) . ' - ' . $invoice->getUser()->getFormatedName();

                    $pdf = new Pdf();
                    $pdf->createPdf($html, $pdf_name);
                }
                else {
                    $this->view->disable();
                    $this->flash->error('La facture n\'existe pas!');
                    $this->response->redirect('invoices');
                }
            }
            else {
                $this->view->disable();
                $this->flash->error('La facture n\'existe pas!');
                $this->response->redirect('invoices');
            }
        }
        else {
            $this->view->disable();
            $this->flash->error('La facture n\'existe pas!');
            $this->response->redirect('invoices');
        }
    }

    public function downloadAction($id)
    {
        $this->view->setVar('activeClass', 'invoices');
        $this->view->setVar('openTab', 'account');

        if (parent::validateMongoId($id)) {
            $invoice = Invoices::findById(new \MongoId($id));
            if ($invoice) {
                if ($invoice->getUserId() == $this->auth->getId()) {
                    
                    $this->view->disable();

                    $html = $this->view->getRender('invoices', 'print', array('invoice' => $invoice));
                    $pdf_name = 'Facture #' . ((string) $invoice->getId()) . ' - ' . $invoice->getUser()->getFormatedName();

                    $pdf = new Pdf();
                    $pdf->downloadPdf($html, $pdf_name);
                }
                else {
                    $this->view->disable();
                    $this->flash->error('La facture n\'existe pas!');
                    $this->response->redirect('invoices');
                }
            }
            else {
                $this->view->disable();
                $this->flash->error('La facture n\'existe pas!');
                $this->response->redirect('invoices');
            }
        }
        else {
            $this->view->disable();
            $this->flash->error('La facture n\'existe pas!');
            $this->response->redirect('invoices');
        }
    }

}
