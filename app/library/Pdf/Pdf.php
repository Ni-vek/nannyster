<?php

namespace Nannyster\Pdf;

require_once BASE_DIR . '/vendor/autoload.php';

class Pdf
{

    /**
     * The pdf object
     * 
     * @var \mPDF
     */
    private $pdf = null;

    public function __construct()
    {
        $this->setPdf(new \mPDF());
    }

    /**
     * Return the pdf object
     * 
     * @return \mPDF
     */
    public function getPdf()
    {
        return $this->pdf;
    }

    /**
     * Srt the pdf object
     * 
     * @param \mPDF $pdf
     * @return \mPDF
     */
    public function setPdf(\mPDF $pdf, $footer = null)
    {
        $this->pdf = $pdf;
        return $this;
    }

    public function createPdf($html, $name)
    {
        $this->_buildPdf($html);
        $this->getPdf()->Output($name, 'I');
    }

    public function downloadPdf($html, $name)
    {
        $this->_buildPdf($html);
        $this->getPdf()->Output($name, 'D');
    }

    protected function _buildPdf($html)
    {
        $this->getPdf()->WriteHTML($html);
    }

}
