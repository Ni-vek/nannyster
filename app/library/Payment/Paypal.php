<?php

namespace Nannyster\Payment;

require BASE_DIR . '/vendor/autoload.php';

use PayPal\Rest\ApiContext;
use PayPal\Auth\OAuthTokenCredential;
use PayPal\Api\Address;
use PayPal\Api\Amount;
use PayPal\Api\Payer;
use PayPal\Api\PayerInfo;
use PayPal\Api\Payment;
use PayPal\Api\CreditCard;
use PayPal\Api\FundingInstrument;
use PayPal\Api\RedirectUrls;
use PayPal\Api\Transaction;
use PayPal\Api\ItemList;

class Paypal
{

    
    /**
     * The phalcon config from DI
     * 
     * @var \Phalcon\Config
     */
    private $config;
    
    /**
     * The phalcon logger from DI
     * 
     * @var \Phalcon\Logger\Adapter\File 
     */
    private $logger;
    
    /**
     * The paypal api context
     * 
     * @var \PayPal\Rest\ApiContext 
     */
    private $api_context;
    
    /**
     * The paypal payer
     * 
     * @var \PayPal\Api\Payer 
     */
    private $payer;
    
    /**
     * The payer info
     * 
     * @var \PayPal\Api\PayerInfo 
     */
    private $payer_info;
    
    
    /**
     * The payer address
     * 
     * @var \PayPal\Api\Address 
     */
    private $addr;
    
    /**
     * The credit card
     * 
     * @var \PayPal\Api\CreditCard 
     */
    private $card;
    
    /**
     * The funding instrument
     * 
     * @var \PayPal\Api\FundingInstrument 
     */
    private $fi;
    
    /**
     * The sale amount
     * 
     * @var \PayPal\Api\Amount 
     */
    private $amount;
    
    /**
     * The paypal transaction
     * 
     * @var \PayPal\Api\Transaction 
     */
    private $transaction;
    
    /**
     * The redirection URL
     * 
     * @var \PayPal\Api\RedirectUrls 
     */
    private $redirectUrls;
    
    /**
     * The paypal payment
     * 
     * @var \PayPal\Api\Payment 
     */
    private $payment;
    
    /**
     * The item list ordered
     * 
     * @var \PayPal\Api\ItemList
     */
    private $item_list;

    public function __construct()
    {
        $this->config = \Phalcon\DI::getDefault()->getShared('config');
        $this->logger = \Phalcon\DI::getDefault()->getShared('logger');

        // Create API context
        $this->api_context = new ApiContext(new OAuthTokenCredential(
                $this->config->paypal->clientId, $this->config->paypal->secret
        ));
        $this->api_context->setConfig(array(
            'mode' => $this->config->paypal->mode,
            'http.ConnectionTimeOut' => 30,
            'log.LogEnabled' => true,
            'log.FileName' => $this->config->paypal->logPath,
            'log.LogLevel' => $this->config->paypal->logLevel
        ));
    }
    
    public function createPaymentWithPaypal(\Nannyster\Models\SubscriptionsBase $subscription, \Nannyster\Models\Users $user){
        $this->setAddress($user)
                ->setPayer('paypal', $user)
                ->setAmount($subscription->getPrice())
                ->setItemList($subscription->getId())
                ->setTransaction($subscription->getDescription())
                ->setRedirectUrl()
                ->setPayment()
                ->createPayment();
    }
    
    public function createPaymentWithCreditCard(\Nannyster\Models\SubscriptionsBase $subscription, \Nannyster\Models\Users $user, $credit_card){
        return $this->setAddress($user)
                ->setCreditCard($credit_card)
                ->setFundingInstrument()
                ->setPayer('credit_card', $user)
                ->setAmount($subscription->getPrice())
                ->setTransaction($subscription->getDescription())
                ->setPayment()
                ->createPayment();
    }
    
    public function getPayement($payment_id){
        
        // ### Retrieve payment
        // Retrieve the payment object by calling the
        // static `get` method
        // on the Payment class by passing a valid
        // Payment ID
        // (See bootstrap.php for more on `ApiContext`)
        try {
            return Payment::get($payment_id, $this->api_context);
        }
        catch (\PPConnectionException $ex) {
            echo "Exception:" . $ex->getMessage() . PHP_EOL;
            var_dump($ex->getData());
            exit(1);
        }
    }
    
    private function setAddress(\Nannyster\Models\Users $user){
        // ### Address
        // Base Address object used as shipping or billing
        // address in a payment. [Optional]
        
        $this->addr = new Address();
        $this->addr->setLine1($user->getAddress()->getAddresses());
        $this->addr->setCity($user->getAddress()->getCity()->getRealName());
        $this->addr->setPostalCode($user->getAddress()->getCity()->getZipCode());
        $this->addr->setCountryCode('FR');
        $this->addr->setPhone($user->getMobile());
        return $this;
    }
    
    private function setCreditCard($credit_card){
        // ### CreditCard
        // A resource representing a credit card that can be
        // used to fund a payment.
        
        $this->card = new CreditCard();
        $this->card->setType($credit_card['type']);
        $this->card->setNumber($credit_card['number']);
        $this->card->setExpire_month($credit_card['expire_month']);
        $this->card->setExpire_year($credit_card['expire_year']);
        $this->card->setCvv2($credit_card['cvv2']);
        $this->card->setFirst_name($credit_card['firstname']);
        $this->card->setLast_name($credit_card['surname']);
        $this->card->setBilling_address($this->addr);
        return $this;
    }
    
    private function setFundingInstrument(){
        // ### FundingInstrument
        // A resource representing a Payer's funding instrument.
        // Use a Payer ID (A unique identifier of the payer generated
        // and provided by the facilitator. This is required when
        // creating or using a tokenized funding instrument)
        // and the `CreditCardDetails`
        
        $this->fi = new FundingInstrument();
        $this->fi->setCreditCard($this->card);
        return $this;
    }

    private function setPayer($method, \Nannyster\Models\Users $user)
    {
        // ### Payer
        // A resource representing a Payer that funds a payment
        // Use the List of `FundingInstrument` and the Payment Method
        // as 'credit_card'

        $this->payer = new Payer();
        if($method === 'credit_card'){
            $this->payer_info = new PayerInfo();
            $this->payer_info->setEmail($user->getEmail())
                    ->setFirstName($user->getFirstname())
                    ->setLastName($user->getSurname())
                    ->setShippingAddress($this->addr)
                    ->setPayerId((string) $user->getId());
            $this->payer->setPayerInfo($this->payer_info);
        }
        $this->payer->setPaymentMethod($method);
        if($method === 'credit_card'){
            $this->payer->setFundingInstruments(array($this->fi));
        }
        return $this;
    }

    private function setAmount($amount)
    {
        // ### Amount
        // Let's you specify a payment amount.

        $this->amount = new Amount();
        $this->amount->setCurrency($this->config->paypal->currency);
        $this->amount->setTotal(number_format($amount, 2));
        return $this;
    }
    
    private function setItemList($item){
        $this->item_list = new ItemList();
        $this->item_list->setItems((string) $item);
        return $this;
    }

    private function setTransaction($description)
    {
        // ### Transaction
        // A transaction defines the contract of a
        // payment - what is the payment for and who
        // is fulfilling it. Transaction is created with
        // a `Payee` and `Amount` types

        $this->transaction = new Transaction();
        $this->transaction->setAmount($this->amount);
        $this->transaction->setDescription($description);
        if($this->payer->getPaymentMethod() === 'paypal'){
            $this->transaction->setItemList($this->item_list);
        }
        
        return $this;
    }

    private function setRedirectUrl()
    {
        // ### Redirect urls
        // Set the urls that the buyer must be redirected to after 
        // payment approval/ cancellation.

        $baseUrl = $this->config->application->protocol . $this->config->application->publicUrl;
        $this->redirectUrls = new RedirectUrls();
        $this->redirectUrls->setReturn_url($baseUrl . $this->config->paypal->redirectSuccessUrl);
        $this->redirectUrls->setCancel_url($baseUrl . $this->config->paypal->redirectCancelUrl);
        return $this;
    }

    private function setPayment()
    {
        // ### Payment
        // A Payment Resource; create one using
        // the above types and intent as 'sale'
        $this->payment = new Payment();
        $this->payment->setIntent("sale")
                ->setPayer($this->payer)
                ->setTransactions(array($this->transaction));
        if($this->payer->getPaymentMethod() === 'paypal'){
            $this->payment->setRedirect_urls($this->redirectUrls);
        }
        
        return $this;
    }

    private function createPayment()
    {
        // ### Create Payment
        // Create a payment by posting to the APIService
        // using a valid apiContext.
        // (See bootstrap.php for more on `ApiContext`)
        // The return object contains the status and the
        // url to which the buyer must be redirected to
        // for payment approval
        try {
            $this->payment->create($this->api_context);
            if($this->payer->getPaymentMethod() === 'credit_card'){
                return $this->payment;
            }
            if($this->payer->getPaymentMethod() === 'paypal'){
                $this->_doRedirect();
            }
            
        }
        catch (\Exception $ex) {
            $this->logger->error('Error fetching credit card : ' . $ex->getMessage());
            return false;
//            echo "Exception: " . $ex->getMessage() . PHP_EOL;
//            var_dump($ex->getData());
//            exit(1);
        }
    }

    private function _doRedirect()
    {
        
        // ### Redirect buyer to paypal
        // Retrieve buyer approval url from the `payment` object.
        foreach ($this->payment->getLinks() as $link) {
            if ($link->getRel() == 'approval_url') {
                $redirectUrl = $link->getHref();
            }
        }
        // It is not really a great idea to store the payment id
        // in the session. In a real world app, please store the
        // payment id in a database.
        $_SESSION['paymentId'] = $this->payment->getId();
        if (isset($redirectUrl)) {
            header("Location: $redirectUrl");
            exit;
        }
    }

}
