<?php

namespace Nannyster\Mail;

require_once BASE_DIR . '/vendor/autoload.php';

use Phalcon\Mvc\User\Component;
use Phalcon\Mvc\View;

/**
 * Nannyster\Mail\Mail
 * Sends e-mails based on pre-defined templates
 */
class Mail extends Component
{

    /**
     * Applies a template to be used in the e-mail
     *
     * @param string $name
     * @param array $params
     */
    public function getTemplate($name, $params)
    {
        $parameters = array_merge(array(
            'publicUrl' => $this->config->application->publicUrl
                ), $params);

        return $this->view->getRender('emailTemplates', $name, $parameters, function ($view) {
                    $view->setRenderLevel(View::LEVEL_LAYOUT);
                });

        return $view->getContent();
    }

    /**
     * Sends e-mails via AmazonSES based on predefined templates
     *
     * @param array $to
     * @param string $subject
     * @param string $name
     * @param array $params
     */
    public function send($to, $subject, $name, $params, $tags)
    {

        // Settings
        $mailSettings = $this->config->mail;

        $template = $this->getTemplate($name, $params);
//try {

        $mandrill = new \Mandrill($mailSettings->mandrillApiKey);
        $message = array(
            'html' => $template,
            'subject' => $subject,
            'from_email' => $mailSettings->fromEmail,
            'from_name' => $mailSettings->fromName,
            'to' => array($to),
            'headers' => array('Reply-To' => $mailSettings->fromEmail),
            'important' => false,
            'track_opens' => true,
            'track_clicks' => true,
            'auto_text' => null,
            'auto_html' => null,
            'inline_css' => null,
            'url_strip_qs' => null,
            'preserve_recipients' => true,
            'view_content_link' => null,
            'bcc_address' => 'message.bcc_address@example.com',
            'tracking_domain' => null,
            'signing_domain' => null,
            'return_path_domain' => null,
            'merge' => true,
            'tags' => array($tags),
            'google_analytics_domains' => array('nannyster.fr'),
            'google_analytics_campaign' => 'no-reply@nannyster.fr',
            'metadata' => array('nannyster' => 'www.nannyster.fr')
        );
        $async = false;
        $ip_pool = 'Main Pool';
        $send_at = null;
        $result = $mandrill->messages->send($message, $async, $ip_pool, $send_at);
        //print_r($result);
        //die();
        /*
          Array
          (
          [0] => Array
          (
          [email] => recipient.email@example.com
          [status] => sent
          [reject_reason] => hard-bounce
          [_id] => abc123abc123abc123abc123abc123
          )

          )
         */
        /* } catch(Mandrill_Error $e) {
          // Mandrill errors are thrown as exceptions
          echo 'A mandrill error occurred: ' . get_class($e) . ' - ' . $e->getMessage();
          // A mandrill error occurred: Mandrill_Unknown_Subaccount - No subaccount exists with the id 'customer-123'
          throw $e;
          } */
    }

}
