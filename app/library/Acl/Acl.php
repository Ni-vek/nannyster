<?php

namespace Nannyster\Acl;

use Phalcon\Mvc\User\Component;
use Phalcon\Acl\Adapter\Memory as AclMemory;
use Phalcon\Acl\Role as AclRole;
use Phalcon\Acl\Resource as AclResource;
use Nannyster\Models\Profiles;
use Nannyster\Models\Permissions;

/**
 * Nannyster\Acl\Acl
 */
class Acl extends Component
{

    /**
     * The ACL Object
     *
     * @var \Phalcon\Acl\Adapter\Memory
     */
    private $acl;

    /**
     * The filepath of the ACL cache file from APP_DIR
     *
     * @var string
     */
    private $filePath = '/cache/acl/data.txt';

    /**
     * Define the resources that are considered "private". These controller => actions require authentication.
     * The second parameter of each field defines if this action is free from subscription. true => free, false => not free 
     *
     * @var array
     */
    private $privateResources = array(
        'administration' => array(
            'index' => false
        ),
        'amendments' => array(
            'index' => false,
            'create' => false,
            'delete' => false
        ),
        'blog' => array(
            'index' => true
        ),
        'calendar' => array(
            'index' => false,
            'ajaxAddEvent' => false,
            'ajaxCalendarFinder' => false,
            'ajaxCalendarRemover' => false,
            'ajaxCalendarLockerUnlocker' => false
        ),
        'cities' => array(
            'ajaxCitiesFinder' => true
        ),
        'contracts' => array(
            'index' => true,
            'view' => false,
            'create' => false,
            'ajaxCreate' => false,
            'ajaxCalendarFinder' => false,
            'delete' => false,
            'print' => false,
            'download' => false
        ),
        'dashboard' => array(
            'index' => true
        ),
        'database' => array(
            'index' => false
        ),
        'directory' => array(
            'index' => true
        ),
        'errors' => array(
            'notFound' => true,
            'uncaughtException' => true,
            'maintenance' => true
        ),
        'faq' => array(
            'index' => true
        ),
        'gallery' => array(
            'index' => false
        ),
        'index' => array(
            'index' => true
        ),
        'invoices' => array(
            'index' => true,
            'view' => false,
            'print' => false,
            'download' => false
        ),
        'legal' => array(
            'notices' => true
        ),
        'mailbox' => array(
            'index' => true,
            'ajaxSendNewMail' => false,
            'ajaxMessageView' => false,
            'ajaxMessageDeleter' => false,
            'ajaxMutipleRemover' => false,
            'ajaxMailboxResponseSender' => false,
            'quick' => false
        ),
        'notifications' => array(
            'index' => true
        ),
        'payrolls' => array(
            'index' => false,
            'view' => false,
            'create' => false,
            'delete' => false
        ),
        'permissions' => array(
            'index' => false
        ),
        'profiles' => array(
            'index' => false,
            'edit' => false,
            'create' => false,
            'delete' => false
        ),
        'session' => array(
            'login' => true,
            'signup' => true,
            'password' => true,
            'logout' => true,
            'lock' => true,
            'unlock' => true,
            'check' => true
        ),
        'seo' => array(
            'index' => false
        ),
        'social_cotis' => array(
            'add' => false,
            'delete' => false,
            'index' => false
        ),
        'subscriptions' => array(
            'index' => true,
            'edit' => false,
            'delete' => false,
            'create' => false,
            'manage' => false,
            'buy' => true
        ),
        'taxes' => array(
            'index' => true
        ),
        'timeline' => array(
            'index' => true
        ),
        'transactions' => array(
            'payment' => true
        ),
        'user_control' => array(
            'confirmEmail' => true,
            'resetPassword' => true
        ),
        'users' => array(
            'index' => false,
            'edit' => false,
            'view' => true,
            'delete' => false,
            'changePassword' => true,
            'ajaxCreateUser' => false,
            'ajaxCheckUserMail' => false,
            'ajaxUsersForNewMail' => false,
            'ajaxProfileEditor' => true,
            'ajaxAdminProfileEditor' => false,
            'ajaxImageEditor' => true,
            'ajaxMotherUsersForNewContract' => false,
            'ajaxFatherUsersForNewContract' => false,
            'manage' => false
        )
    );

    /**
     * Human-readable descriptions of the actions used in {@see $privateResources}
     *
     * @var array
     */
    private $actionDescriptions = array(
        'index' => 'Index',
        'search' => 'Search',
        'create' => 'Create',
        'edit' => 'Edit',
        'delete' => 'Delete',
        'view' => 'View',
        'confirmEmail' => 'Email confirmation for new user',
        'resetPassword' => 'Reset a forgotten password',
        'forgotPassword' => 'Forgotten Password',
        'changePassword' => 'Change password',
        'ajaxCalendarFinder' => 'Ajax request to retrieve calendar events',
        'ajaxCalendarRemover' => 'Ajax request to delete an event',
        'ajaxCalendarLockerUnlocker' => 'Ajax request to lock or unlock an event',
        'ajaxAddEvent' => 'Ajax request adding new date in planning',
        'ajaxCreate' => 'Ajax request to complete each step of a new contract',
        'ajaxSendNewMail' => 'Send a message',
        'quick' => 'Quick message sender',
        'ajaxCitiesFinder' => 'Ajax request to find citites for select2',
        'ajaxMessageView' => 'View a conversation',
        'ajaxMessageDeleter' => 'Delete a conversation',
        'ajaxMutipleRemover' => 'Delete multiple conversations',
        'ajaxMailboxResponseSender' => 'Reply to a conversation',
        'ajaxUsersForNewMail' => 'Ajax request to find users (for mailbox only)',
        'ajaxProfileEditor' => 'Ajax request to edit a profile inline',
        'ajaxAdminProfileEditor' => 'Admin ajax request to edit a profile',
        'ajaxImageEditor' => 'Ajax request to upload a new user\'s avatar',
        'ajaxMotherUsersForNewContract' => 'Ajax request to retrieve mothers account for a new contract',
        'ajaxFatherUsersForNewContract' => 'Ajax request to retrieve fathers account for a new contract'
    );

    /**
     * Checks if a controller is private or not
     *
     * @param string $controllerName
     * @return boolean
     */
    public function isPrivate($controllerName)
    {
        return isset($this->privateResources[$controllerName]);
    }

    /**
     * Checks if the current profile is allowed to access a resource
     *
     * @param string $profile
     * @param string $controller
     * @param string $action
     * @return boolean
     */
    public function isAllowed($profile, $controller, $action)
    {
//         return true;
        if ($this->getAcl()->isAllowed($profile, $controller, $action)) {
            if ($this->auth->hasIdentity()) {
                if ($this->auth->getIdentity()->getSubscription()->getExpired()) {
                    if (!$this->isFreeResource($controller, $action)) {
                        return false;
                    }
                }
            }
            else {
                if (!$this->isFreeResource($controller, $action)) {
                    return false;
                }
            }
            return true;
        }
        else {
            return false;
        }
    }

    public function isFreeResource($controller, $action)
    {
        return $this->privateResources[$controller][$action];
    }

    /**
     * Returns the ACL list
     *
     * @return \Phalcon\Acl\Adapter\Memory
     */
    public function getAcl()
    {
        // Check if the ACL is already created
        if (is_object($this->acl)) {
            return $this->acl;
        }

        // Check if the ACL is in APC
        if (function_exists('apc_fetch')) {
            $acl = apc_fetch('nannyster-acl');
            if (is_object($acl)) {
                $this->acl = $acl;
                return $acl;
            }
        }

        // Check if the ACL is already generated
        if (!file_exists(APP_DIR . $this->filePath)) {
            $this->acl = $this->rebuild();
            return $this->acl;
        }

        // Get the ACL from the data file
        $data = file_get_contents(APP_DIR . $this->filePath);
        $this->acl = unserialize($data);

        // Store the ACL in APC
        if (function_exists('apc_store')) {
            apc_store('nannyster-acl', $this->acl);
        }

        return $this->acl;
    }

    /**
     * Returns the permissions assigned to a profile
     *
     * @param Profiles $profile
     * @return array
     */
    public function getPermissions(Profiles $profile)
    {
        $profilePermissions = Permissions::find(array(array(
                        'profile_id' => $profile->getId())));
        $permissions = array();
        foreach ($profilePermissions as $permission) {
            $permissions[$permission->getResource() . '.' . $permission->getAction()] = true;
        }
        return $permissions;
    }

    /**
     * returns all permissions
     */
    public function getAllPermissions()
    {
        $permissions = Permissions::find();
        $array_permissions = array();
        foreach ($permissions as $permission) {
            $array_permissions[$permission->getResource() . '.' . $permission->getAction()][(string) $permission->getProfileId()] = true;
        }
        return $array_permissions;
    }

    /**
     * Returns all the resoruces and their actions available in the application
     *
     * @return array
     */
    public function getResources()
    {
        return $this->privateResources;
    }

    /**
     * Returns the action description according to its simplified name
     *
     * @param string $action
     * @return $action
     */
    public function getActionDescription($action)
    {
        if (isset($this->actionDescriptions[$action])) {
            return $this->actionDescriptions[$action];
        }
        else {
            return $action;
        }
    }

    /**
     * Rebuilds the access list into a file
     *
     * @return \Phalcon\Acl\Adapter\Memory
     */
    public function rebuild()
    {
        $acl = new AclMemory();

        $acl->setDefaultAction(\Phalcon\Acl::DENY);

        // Register roles
        $profiles = Profiles::find(array(array(
                        'active' => true)));

        foreach ($profiles as $profile) {
            $acl->addRole(new AclRole($profile->getName()));
        }

        foreach ($this->privateResources as $resource => $actions) {
            $aclActions = array();
            foreach ($actions as $key => $is_free) {
                $aclActions[] = $key;
            }
            $acl->addResource(new AclResource($resource), $aclActions);
        }
        
        // Grant acess to private area to role Users
        foreach ($profiles as $profile) {

            //Retrieve permissions for each profile
            $permissions = Permissions::find(array(array(
                            'profile_id' => $profile->getId())));

            // Grant permissions in "permissions" model
            foreach ($permissions as $permission) {
                $acl->allow($profile->getName(), $permission->getResource(), $permission->getAction());
            }

            // Always grant these permissions
//            $acl->allow($profile->getId(), 'users', 'changePassword');
        }

        if (touch(APP_DIR . $this->filePath) && is_writable(APP_DIR . $this->filePath)) {

            file_put_contents(APP_DIR . $this->filePath, serialize($acl));

            // Store the ACL in APC
            if (function_exists('apc_store')) {
                apc_store('nannyster-acl', $acl);
            }
        }
        else {
            $this->flash->error(
                    'The user does not have write permissions to create the ACL list at ' . APP_DIR . $this->filePath
            );
        }

        return $acl;
    }

}
