<?php

namespace Nannyster\Utils;

use Nannyster\Models\Contracts;
use Nannyster\Models\Calendar;

class SalaryCalculator
{

    private $identity;
    private $profile;
    private $civility;
    private $contracts;
    private $nbChildren = 0;
    private $timeGard = 0;
    private $curentTimeGard = 0;
    private $salary = 0;
    private $monthSalary = 0;
    private $indemnityCost = 0;
    private $currentIndemnityCost = 0;
    private $todayCalendars = array();

    public function __construct($identity)
    {
        $this->identity = $identity;
        $this->profile = $identity->getProfile()->getName();
        $this->civility = $identity->getCivility()->getShortName();
        $this->_retrieveActiveContracts();
        $this->_retrieveTodayContractCalendar();
        $this->_retrieveMonthContractCalendars();
    }

    public function getContracts()
    {
        return $this->contracts;
    }

    public function getNbChildren()
    {
        return $this->nbChildren;
    }

    public function getTimeGard()
    {
        return sprintf("%02d%s%02d%s", floor($this->timeGard / 3600), 'h ', ($this->timeGard / 60) % 60, 'min');
    }

    public function getCurentTimeGard()
    {
        return sprintf("%02d%s%02d%s", floor($this->curentTimeGard / 3600), 'h ', ($this->curentTimeGard / 60) % 60, 'min');
    }

    public function getSalary()
    {
        return $this->salary;
    }

    public function getMonthSalary()
    {
        return $this->monthSalary;
    }

    public function getIndemnityCost()
    {
        return $this->indemnityCost;
    }

    public function getCurrentIndemnityCost()
    {
        return $this->currentIndemnityCost;
    }

    public function getTodayCalendars()
    {
        return $this->todayCalendars;
    }

    protected function _retrieveActiveContracts()
    {

//Retrieve active contracts
        if ($this->profile != 'Parent') {
            $this->contracts = Contracts::find(array(
                        array(
                            'valide' => true,
                            'nanny' => $this->identity->getId(),
                            'contract_start_date' => array('$lte' => new \MongoDate())))
            );
        }
        if ($this->profile != 'Nourrice' && $this->civility == 'Mme') {
            $this->contracts = Contracts::find(array(
                        array(
                            'valide' => true,
                            'mother' => $this->identity->getId(),
                            'contract_start_date' => array('$lte' => new \MongoDate()))
                            )
            );
        }
        if ($this->profile != 'Nourrice' && $this->civility == 'Mr') {
            $this->contracts = Contracts::find(array(
                        array(
                            'valide' => true,
                            'father' => $this->identity->getId(),
                            'contract_start_date' => array('$lte' => new \MongoDate()))
                            )
            );
        }
    }

    protected function _retrieveTodayContractCalendar()
    {
        //for each contract, find today calendar events
        if ($this->contracts) {
            foreach ($this->contracts as $contract) {
                $calendars = Calendar::findFirst(array(array(
                                'contract_id' => (string) $contract->getId(),
                                '$or' => array(
                                    array(
                                        'start' => array(
                                            '$lte' => strtotime(date('Y-m-d 00:00:00'))
                                        ),
                                        'end' => array(
                                            '$gte' => strtotime(date('Y-m-d 23:59:59'))
                                        )
                                    ),
                                    array(
                                        'start' => array(
                                            '$gte' => strtotime(date('Y-m-d 00:00:00'))
                                        ),
                                        'end' => array(
                                            '$lte' => strtotime(date('Y-m-d 23:59:59'))
                                        )
                                    ),
                                    array(
                                        'start' => array(
                                            '$lte' => strtotime(date('Y-m-d 00:00:00'))
                                        ),
                                        'end' => array(
                                            '$gte' => strtotime(date('Y-m-d 00:00:00')),
                                            '$lte' => strtotime(date('Y-m-d 23:59:59'))
                                        )
                                    ),
                                    array(
                                        'start' => array(
                                            '$gte' => strtotime(date('Y-m-d 00:00:00')),
                                            '$lte' => strtotime(date('Y-m-d 23:59:59'))
                                        ),
                                        'end' => array(
                                            '$gte' => strtotime(date('Y-m-d 23:59:59'))
                                        )
                                    )
                                )
                )));
                if ($calendars) {
                    $this->todayCalendars[] = $calendars;
                    $this->nbChildren++;
                }
            }
        }
    }

    protected function _retrieveMonthContractCalendars()
    {
        if ($this->contracts) {
            foreach ($this->contracts as $contract) {

                //gard time initialisation
                $this->timeGardSalary = 0;
                $this->currentTimeGardSalary = 0;
                $this->indemnityTreatedDates = array();
                $currentContractMonthSalary = 0;
                $currentContractSalary = 0;

                //retrieving calendars
                $calendars = Calendar::find(array(array(
                                'contract_id' => (string) $contract->getId(),
                                'start' => array(
                                    '$gte' => strtotime(date('Y-m-01 00:00:00')),
                                    '$lte' => strtotime(date('Y-m-t 23:59:59'))
                                )
                )));

                if ($calendars) {

                    //foreach calendar, calculating calendar duration in seconds
                    foreach ($calendars as $calendar) {

                        //add this calendar duration to global gard time
                        $this->timeGard += $calendar->getEnd() - $calendar->getStart();

                        //current salary, time gard & indemnity cost
                        if ($calendar->getStart() <= strtotime(date('Y-m-d 23:59:59'))) {
                            $this->curentTimeGard += $calendar->getEnd() - $calendar->getStart();
                            $this->currentTimeGardSalary += $calendar->getEnd() - $calendar->getStart();
                            if (!array_key_exists(date('d-m-Y', $calendar->getStart()), $indemnityTreatedDates)) {
                                $indemnityTreatedDates[date('d-m-Y', $calendar->getStart())] = 1;
                                $this->indemnityCost += $contract->getMaintenanceAllowance();
                                $this->currentIndemnityCost += $contract->getMaintenanceAllowance();
                            }
                        }

                        if (!array_key_exists(date('d-m-Y', $calendar->getStart()), $indemnityTreatedDates)) {
                            $indemnityTreatedDates[date('d-m-Y', $calendar->getStart())] = 1;
                            $this->indemnityCost += $contract->getMaintenanceAllowance();
                        }
                    }

                    //copy of global time for salary calculation
                    $this->timeGardSalary = $this->timeGard;
                }


                $currentContractMonthSalary += $contract->getHourlyRate() / 3600 * $this->timeGardSalary;
                $currentContractSalary += $contract->getHourlyRate() / 3600 * $this->currentTimeGardSalary;

                if ($currentContractMonthSalary < $contract->getSalaireMensuelBrut()) {
                    $this->monthSalary += $contract->getSalaireMensuelBrut();
                }
                else {
                    $this->monthSalary += $currentContractMonthSalary;
                }
                if ($currentContractSalary < $contract->getSalaireMensuelBrut()) {
                    $this->salary += $contract->getSalaireMensuelBrut();
                }
                else {
                    $this->salary += $currentContractSalary;
                }
            }
        }
    }

}
