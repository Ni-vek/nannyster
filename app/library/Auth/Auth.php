<?php

namespace Nannyster\Auth;

use Phalcon\Mvc\User\Component;
use Nannyster\Models\Users;
use Nannyster\Models\RememberTokens;
use Nannyster\Models\SuccessLogins;
use Nannyster\Models\FailedLogins;
use Nannyster\Models\Profiles;

/**
 * Manages Authentication/Identity Management in Nannyster
 */
class Auth extends Component
{

    /**
     * The current logged identity
     * 
     * @var \Nannyster\Models\Users;
     */
    private $identity = null;

    /**
     * Checks the user credentials
     *
     * @param array $credentials
     * @return boolan
     */
    public function check($credentials)
    {

        // Check if the user exist
        $user = Users::findFirst(array(array('email' => $credentials['email'])));
        if (!$user) {
            throw new \Exception('Nous ne parvenons pas à vous authentifier! Vous avez saisi une mauvaise combinaison Email / Mot de passe!');
        }

        // Check the password
        if (!$this->security->checkHash($credentials['password'], $user->getPassword())) {
            $this->registerUserThrottling($user);
            throw new \Exception('Nous ne parvenons pas à vous authentifier! Vous avez saisi une mauvaise combinaison Email / Mot de passe!');
        }

        // Check if the user was flagged
        $this->checkUserFlags($user);

        // Register the successful login
        $this->saveSuccessLogin($user);

        // Check if the remember me was selected
        if (isset($credentials['remember'])) {
            $this->createRememberEnviroment($user);
        }

        // Send the session cookie
        $this->createSessionCookie($user);

        $this->startSession($user);
    }

    /**
     * Creates the remember me environment settings the related cookies and generating tokens
     *
     * @param Nannyster\Models\Users $user
     */
    public function saveSuccessLogin($user)
    {
        $successLogin = new SuccessLogins();
        $successLogin->setUser($user)
                ->setIpAddress($this->request->getClientAddress())
                ->setUserAgent($this->request->getUserAgent())
                ->save();
    }

    /**
     * Implements login throttling
     * Reduces the efectiveness of brute force attacks
     *
     * @param int
     */
    public function registerUserThrottling(\Nannyster\Models\Users $user)
    {
        $failedLogin = new FailedLogins();
        $failedLogin->setUser($user)
                ->setIpAddress($this->request->getClientAddress())
                ->setAttemptedAt(new \MongoDate())
                ->save();

        $attempts = FailedLogins::count(array(array(
                        'ip_address' => $this->request->getClientAddress()
        )));

        switch ($attempts) {
            case 1:
            case 2:
                // no delay
                break;
            case 3:
            case 4:
                sleep(2);
                break;
            default:
                sleep(4);
                break;
        }
    }

    /**
     * Creates the remember me environment settings the related cookies and generating tokens
     *
     * @param Vokuro\Models\Users $user
     */
    public function createRememberEnviroment(\Nannyster\Models\Users $user)
    {
        $user_agent = $this->request->getUserAgent();
        $token = md5($user->email . $user->password . $user_agent);

        $remember = new RememberTokens();
        $remember->setUser($user)
                ->setToken($token)
                ->setUserAgent($user_agent)
                ->setExpiredAt(new \MongoDate(time() + $this->config->cookies->expireTime));
        if ($remember->save()) {
            $expire = time() + $this->config->cookies->expireTime;
            $this->cookies->set('RMU', $user->getId(), $expire);
            $this->cookies->set('RMT', $token, $expire);
            if (!$this->cookies->send()) {
                $this->flash->warning('<i class="icon-warning-sign"></i> Votre navigateur n\'accepte pas les cookies. Vous ne pourrez donc pas être automatiquement connecté au site à chacune de vos visites');
            }
        }
    }

    /**
     * Create a session cookie
     */
    public function createSessionCookie(\Nannyster\Models\Users $user)
    {
        //Nannyster Session Checker
        $expire = time() + $this->config->cookies->expireTime;
        $this->cookies->set('NSC', serialize(array('id' => $user->getId(), 'name' => $user->getFormatedName(), 'email' => $user->getEmail(), 'avatar' => $user->getAvatarFile())), $expire);
        $this->cookies->send();
    }

    /**
     * Check if the session has a remember me cookie
     *
     * @return boolean
     */
    public function hasRememberMe()
    {
        return $this->cookies->has('RMU');
    }

    /**
     * Check the current session is locked
     * 
     * @return boolean
     */
    public function isLocked()
    {
        return $this->cookies->has('NSC');
    }

    /**
     * Logs on using the information in the coookies
     *
     * @return Phalcon\Http\Response
     */
    public function loginWithRememberMe()
    {
        $user_id = $this->cookies->get('RMU')->getValue();
        $cookie_token = $this->cookies->get('RMT')->getValue();
        if (!\Nannyster\Controllers\ControllerBase::validateMongoId($user_id)) {
            $this->auth->remove();
            $this->response->redirect('index');
        }

        $user = Users::findById(new \MongoId($user_id));
        if ($user) {
            $user_agent = $this->request->getUserAgent();
            $token = md5($user->getEmail() . $user->getPassword() . $user_agent);

            if ($cookie_token == $token) {

                $remember = RememberTokens::findFirst(array(array(
                                'user' => $user->getId(),
                                'token' => $cookie_token
                )));

                if ($remember) {

                    if ($remember->getExpiredAt()->sec > time()) {

                        // Check if the user was flagged
                        $this->checkUserFlags($user);

                        // Register identity and profile
                        $this->startSession($user);

                        // Register the successful login
                        $this->saveSuccessLogin($user);

                        return true;
                    }
                }
            }
        }
//        $this->auth->remove($remember);

        return false;
    }

    /**
     * Checks if the user is banned/inactive/suspended
     *
     * @param \Nannyster\Models\Users $user
     */
    public function checkUserFlags(\Nannyster\Models\Users $user)
    {
        if (!$user->getActive()) {
            throw new \Exception('Vous n\'avez pas encore confirmé votre adresse email. Veuillez sur les instructions contenues dans l\'email reçu lors de votre inscription');
        }

        if ($user->getBanned()) {
            throw new \Exception('Votre compte utilisateur a été bannie du site Nannyster. Pour plus d\'informations, veuillez nous contacter');
        }

        if ($user->getSuspended()) {
            throw new \Exception('Votre compte utilisateur a été suspendu. Pour plus d\'informations, veuillez nous contacter');
        }
    }

    /**
     * Set the current user logged in
     * 
     * @param \Nannyster\Models\Users $user
     */
    public function setIdentity(\Nannyster\Models\Users $user)
    {
        $this->identity = $user;
    }

    /**
     * Returns the current identity
     *
     * @return Nannyster\Models\Users
     */
    public function getIdentity()
    {
        if ($this->identity === null && $this->session->has('auth-identity')) {
            $this->identity = unserialize($this->session->get('auth-identity'));
        }
        return $this->identity;
    }
    
    /**
     * Check if current user is logged or not
     * 
     * @return boolean
     */
    public function hasIdentity(){
        return $this->session->has('auth-identity');
    }

    /**
     * Returns the current name
     *
     * @return string
     */
    public function getName()
    {
        if ($this->getIdentity() !== null) {
            return $this->getIdentity()->getFullnames();
        }
        else {
            return null;
        }
    }

    /**
     * Returns the current profile name
     *
     * @return string
     */
    public function getProfile()
    {
        if ($this->getIdentity() !== null) {
            return $this->getIdentity()->getProfile();
        }
        else {
            return null;
        }
    }

    /**
     * Returns the current id
     *
     * @return string
     */
    public function getId()
    {
        if ($this->getIdentity() !== null) {
            return $this->getIdentity()->getId();
        }
        else {
            return null;
        }
    }

    /**
     * Removes the user identity information from session
     */
    public function remove(\Nannyster\Models\RememberTokens $rememberToken = null)
    {
        if ($this->cookies->has('RMU')) {
            $this->cookies->get('RMU')->delete();
        }
        if ($this->cookies->has('RMT')) {
            $this->cookies->get('RMT')->delete();
        }
        if ($this->cookies->has('NSC')) {
            $this->cookies->get('NSC')->delete();
        }
        if ($rememberToken) {
            $rememberToken->delete();
        }
        if ($this->session->has('auth-identity')) {
            $remembers = RememberTokens::find(array(array(
                            'user' => (string) $this->auth->getId())));
            if ($remembers) {
                foreach ($remembers as $remember) {
                    $remember->delete();
                }
            }
            $this->session->remove('auth-identity');
        }
    }

    /**
     * Lock the user
     */
    public function lock()
    {
        $this->session->remove('auth-identity');
    }

    /**
     * Auths the user by his/her id
     *
     * @param int $id
     */
    public function authUserById($id)
    {
        $user = Users::findById($id);
        if (!$user) {
            throw new \Exception('Cet utilisateur n\'existe pas!');
        }
        $this->checkUserFlags($user);
        $this->startSession($user);
    }

    /**
     * Define the session
     */
    public function startSession(\Nannyster\Models\Users $user)
    {
        $user = $user->fetchForSession();
        $this->setIdentity($user);
        $this->session->set('auth-identity', serialize($user));
    }

}
