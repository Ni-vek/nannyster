<?php

namespace Nannyster\Recaptcha;

class Recaptcha extends \Phalcon\DI\Injectable
{
    /**
     * The reCAPTCHA server URL's
     */
    const RECAPTCHA_API_SERVER = 'http://www.google.com/recaptcha/api';
    const RECAPTCHA_API_SECURE_SERVER = 'https://www.google.com/recaptcha/api';
    const RECAPTCHA_VERIFY_SERVER = 'www.google.com';

    /**
     * The reCAPTCHA error messages
     */
    const RECAPTCHA_ERROR_KEY = 'To use reCAPTCHA you must get an API key from <a href="https://www.google.com/recaptcha/admin/create">https://www.google.com/recaptcha/admin/create</a>';
    const RECAPTCHA_ERROR_REMOTE_IP = 'For security reasons, you must pass the remote IP address to reCAPTCHA';

    
    public static $error = 'incorrect-captcha-sol';
    public static $is_valid = false;

    /**
     * Gets the challenge HTML (javascript and non-javascript version).
     * This is called from the browser, and the resulting reCAPTCHA HTML widget
     * is embedded within the HTML form it was called from.
     *
     * @param string $publicKey A public key for reCAPTCHA (optional, default is false)
     * @param string $error The error given by reCAPTCHA (optional, default is '')
     * @param boolean $useSSL Should the request be made over ssl? (optional, default is false)
     * @return string - The HTML to be embedded in the user's form.
     */
    public static function get($publicKey, $error = '', $useSSL = true)
    {
        // Merging method arguments with class fileds 
        $publicKey = $publicKey or die(self::RECAPTCHA_ERROR_KEY);

        // Choosing a server
        $server = $useSSL ? self::RECAPTCHA_API_SECURE_SERVER : self::RECAPTCHA_API_SERVER;

        // Append an error
        if ($error) $error = "&amp;error=".$error;

        // Return HTML
        return '<div id="recaptcha_widget" style="display:none">
                    <div id="recaptcha_image"></div>
                    <input class="form-control index-form" type="text" id="recaptcha_response_field" name="recaptcha_response_field" placeholder="Entrez les mots ci-dessus">
                </div>
                 <script type="text/javascript" src="'.$server.'/challenge?k='.$publicKey.$error.'"></script>
                <noscript>
                    <iframe src="'.$server.'/noscript?k='.$publicKey.$error.'" height="300" width="500" frameborder="0"></iframe><br/>
                    <textarea name="recaptcha_challenge_field" rows="3" cols="40"></textarea>
                    <input type="hidden" name="recaptcha_response_field" value="manual_challenge"/>
                </noscript>';
    }

    /**
     * Calls an HTTP POST function to verify if the user's guess was correct
     *
     * @param string $privateKey
     * @param string $remoteip
     * @param string $challenge
     * @param string $response
     * @param array $extra_params An array of extra variables to post to the server
     * @return boolean $this->is_valid property
     */
    public static function check($privateKey, $remoteIP, $challenge, $response, $extra_params = array())
    {
        $privateKey = $privateKey or die(self::RECAPTCHA_ERROR_KEY);
        $remoteIP = $remoteIP or die(self::RECAPTCHA_ERROR_REMOTE_IP);

        // Discard spam submissions
        if (!$challenge or !$response)
            return self::$is_valid;

        $response = self::httpPost(self::RECAPTCHA_VERIFY_SERVER, "/recaptcha/api/verify", array(
            'privatekey' => $privateKey,
            'remoteip' => $remoteIP,
            'challenge' => $challenge,
            'response' => $response
        ) + $extra_params);

        $answers = explode("\n", $response[1]);
        
        if (trim($answers[0]) == 'true') self::$is_valid = true;
        else self::$error = $answers[1];

        return self::$is_valid;
    }

    /**
     * Submits an HTTP POST to a reCAPTCHA server
     *
     * @param string $host
     * @param string $path
     * @param array $data
     * @param int port
     * @return array response
     */
    private static function httpPost($host, $path, $data, $port = 80)
    {
        $req = self::qsEncode($data);

        $http_request  = "POST $path HTTP/1.0\r\n";
        $http_request .= "Host: $host\r\n";
        $http_request .= "Content-Type: application/x-www-form-urlencoded;\r\n";
        $http_request .= "Content-Length: ".strlen($req)."\r\n";
        $http_request .= "User-Agent: reCAPTCHA/PHP\r\n";
        $http_request .= "\r\n";
        $http_request .= $req;

        $response = '';
        if (!($fs = @fsockopen($host, $port, $errno, $errstr, 10))) {
            die('Could not open socket');
        }

        fwrite($fs, $http_request);

        while (!feof($fs)) $response .= fgets($fs, 1160); // One TCP-IP packet
        fclose($fs);
        $response = explode("\r\n\r\n", $response, 2);

        return $response;
    }

    /**
     * Encodes the given data into a query string format
     *
     * @param array $data Array of string elements to be encoded
     * @return string $req Encoded request
     */
    private static function qsEncode($data)
    {
        $req = '';
        foreach ($data as $key => $value)
            $req .= $key.'='.urlencode(stripslashes($value)).'&';

        // Cut the last '&'
        $req = substr($req, 0, strlen($req) - 1);

        return $req;
    }
}