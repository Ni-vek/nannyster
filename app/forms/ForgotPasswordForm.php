<?php
namespace Nannyster\Forms;

use Phalcon\Forms\Form;
use Phalcon\Forms\Element\Email;
use Phalcon\Forms\Element\Submit;
use Phalcon\Validation\Validator\PresenceOf;
use Phalcon\Validation\Validator\Email as EmailValidator;

class ForgotPasswordForm extends Form
{

    public function initialize()
    {
        $email = new Email('email', array(
            'placeholder' => 'Votre email',
            'class' => 'form-control index-form'
        ));
        $email->addValidators(array(
            new PresenceOf(array(
                'message' => 'Votre email est requis'
            )),
            new EmailValidator(array(
                'message' => 'Votre email n\'est pas valide'
            ))
        ));
        $this->add($email);
    }
}
