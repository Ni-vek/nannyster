<?php
namespace Nannyster\Forms;

use Phalcon\Forms\Form;
use Phalcon\Forms\Element\Text;
use Phalcon\Forms\Element\Password;
use Phalcon\Forms\Element\Submit;
use Phalcon\Forms\Element\Check;
use Phalcon\Forms\Element\Hidden;
use Phalcon\Forms\Element\Email;
use Phalcon\Validation\Validator\PresenceOf;
use Phalcon\Validation\Validator\Email as EmailValidator;
use Phalcon\Validation\Validator\Identical;

class LoginForm extends Form
{

    public function initialize()
    {
        // Email
        $email = new Email('email', array(
            'placeholder' => 'Votre email',
            'class' => 'form-control'
        ));
        $email->addValidators(array(
            new PresenceOf(array(
                'message' => 'Votre email est requis'
            )),
            new EmailValidator(array(
                'message' => 'Votre email n\'est pas valide'
            ))
        ));
        $this->add($email);

        // Password
        $password = new Password('password', array(
            'placeholder' => 'Votre mot de passe',
            'class' => 'form-control'
        ));
        $password->addValidator(new PresenceOf(array(
            'message' => 'Votre mot de passe est requis'
        )));
        $this->add($password);

        $this->add(new Submit('go', array(
            'class' => 'btn btn-success'
        )));
    }
}
