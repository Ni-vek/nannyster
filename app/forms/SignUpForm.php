<?php
namespace Nannyster\Forms;

use Phalcon\Forms\Form;
use Phalcon\Forms\Element\Text;
use Phalcon\Forms\Element\Hidden;
use Phalcon\Forms\Element\Password;
use Phalcon\Forms\Element\Check;
use Phalcon\Forms\Element\Select;
use Phalcon\Validation\Validator\PresenceOf;
use Phalcon\Validation\Validator\Email;
use Phalcon\Validation\Validator\Identical;
use Phalcon\Validation\Validator\StringLength;
use Phalcon\Validation\Validator\Confirmation;
use Nannyster\Models\Profiles;
use Nannyster\Models\Civilities;
use Nannyster\Validator\UniqueValidator;

class SignUpForm extends Form
{

    public function initialize()
    {
        //User profile
        $profiles = Profiles::find(array(array(
            'active' => true,
            'visible' => true),
            'fields' => array(
                '_id',
                'name')
        ));
        $profile = new Select('profile', Profiles::returnArrayForSelect($profiles), array(
            'useEmpty' => true,
            'emptyText' => 'Vous êtes ...',
            'emptyValue' => '',
            'class' => 'form-control'
        ));
        $profile->addValidators(array(
            new PresenceOf(array(
                'message' => 'Le type de compte est obligatoire'
            ))
        ));
        $this->add($profile);

        //User civility
        $civilities = Civilities::find(array(
            'fields' => array(
                '_id',
                'name'
            )
        ));
        $civility = new Select('civility', Civilities::returnArrayForSelect($civilities), array(
            'useEmpty' => true,
            'emptyText' => 'Votre civilité ...',
            'emptyValue' => '',
            'class' => 'form-control'));
        $civility->addValidators(array(
            new PresenceOf(array(
                'message' => 'Votre civilité est obligatoire'
            ))
        ));
        $this->add($civility);

        //Surname
        $surname = new Text('surname', array(
            'placeholder' => 'NOM',
            'class' => 'form-control'
        ));
        $surname->addValidators(array(
            new PresenceOf(array(
                'message' => 'Votre nom est obligatoire'
            ))
        ));
        $this->add($surname);

        //Wedding surname
        $wedding_surname = new Text('wedding_surname', array(
            'placeholder' => 'NOM MARITAL',
            'class' => 'form-control hidden'
        ));
        $this->add($wedding_surname);

        //Firstname
        $firstname = new Text('firstname', array(
            'placeholder' => 'Prénom',
            'class' => 'form-control'
        ));
        $firstname->addValidators(array(
            new PresenceOf(array(
                'message' => 'Votre prénom est obligatoire'
            ))
        ));
        $this->add($firstname);

        // Email
        $email = new Text('email', array(
            'placeholder' => 'Email',
            'class' => 'form-control'
        ));
        $email->addValidators(array(
            new PresenceOf(array(
                'message' => 'Votre email est obligatoire'
            )),
            new Email(array(
                'message' => 'Votre email n\'est pas valide'
            )),
            new UniqueValidator(array(
                'field' => 'email',
                'message' => 'Votre email est déjà associé à un compte ' . $this->config->application->siteTitle,
                'model' => 'Users'
            ))
        ));
        $this->add($email);

        // Password
        $password = new Password('password', array(
            'placeholder' => 'Mot de passe',
            'class' => 'form-control'
        ));
        $password->addValidators(array(
            new PresenceOf(array(
                'message' => 'Votre mot de passe est obligatoire'
            )),
            new StringLength(array(
                'min' => 8,
                'messageMinimum' => 'Votre mot de passe est trop petit. Au moins 8 caractères'
            )),
            new Confirmation(array(
                'message' => 'Les mots de passe ne correspondent pas',
                'with' => 'confirm_password'
            ))
        ));
        $this->add($password);

        // Confirm Password
        $confirm_password = new Password('confirm_password', array(
            'placeholder' => 'Confirmez votre mot de passe',
            'class' => 'form-control'
        ));
        $confirm_password->addValidators(array(
            new PresenceOf(array(
                'message' => 'La confirmation du mot de passe est obligatoire'
            ))
        ));
        $this->add($confirm_password);

        // Terms
        $terms = new Check('terms', array(
            'value' => 'yes',
            'class' => 'ace'
        ));
        $terms->addValidator(new Identical(array(
            'value' => 'yes',
            'message' => 'Vous devez accepter les conditions d\'utilisations'
        )));

        $this->add($terms);

        // CSRF
        $csrfSignUp = new Hidden('csrfSignUp');
        $csrfSignUp->addValidator(new Identical(array(
            'value' => $this->security->getSessionToken(),
            'message' => 'CSRF validation failed'
        )));
        $this->add($csrfSignUp);
    }

    /**
     * Prints messages for a specific element
     */
    public function messages($name)
    {
        if ($this->hasMessagesFor($name)) {
            foreach ($this->getMessagesFor($name) as $message) {
                $this->flash->error($message);
            }
        }
    }
}
