<?php

namespace Nannyster\Models;

class SocialCotis extends BaseModel
{

    /**
     * The cotisation id
     * 
     * @var \MongoId
     */
    public $_id;

    /**
     * The cotisation short code
     * 
     * @var string
     */
    public $short_code;

    /**
     * The cotisation name
     * 
     * @var string
     */
    public $name;

    /**
     * The cotisation base
     * 
     * @var float
     */
    public $base;

    /**
     * The cotisation employee rate
     * 
     * @var float
     */
    public $employee_rate;

    /**
     * The cotisation boss rate
     * 
     * @var float
     */
    public $boss_rate;
    
    /**
     * The cotisation type
     * Can be 'general', 'alsace' or 'both'
     * 
     * @var string
     */
    public $type;
    
    public function getId()
    {
        return $this->_id;
    }

    public function getShortCode()
    {
        return $this->short_code;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getBase()
    {
        return $this->base;
    }

    public function getEmployeeRate()
    {
        return $this->employee_rate;
    }

    public function getBossRate()
    {
        return $this->boss_rate;
    }

    public function getType()
    {
        return $this->type;
    }

    public function setShortCode($short_code)
    {
        $this->short_code = $short_code;
        return $this;
    }

    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    public function setBase($base)
    {
        $this->base = $base;
        return $this;
    }

    public function setEmployeeRate($employee_rate)
    {
        $this->employee_rate = $employee_rate;
        return $this;
    }

    public function setBossRate($boss_rate)
    {
        $this->boss_rate = $boss_rate;
        return $this;
    }

    public function setType($type)
    {
        $this->type = $type;
        return $this;
    }


    
}