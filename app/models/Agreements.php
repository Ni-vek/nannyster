<?php

namespace Nannyster\Models;

class Agreements extends BaseModel
{

    /**
     * The agreement object ID
     * 
     * @var \MongoID
     */
    public $_id;

    /**
     * The number and age of the children allowed to the nanny
     * 
     * @var string
     */
    public $children_permissions;

    /**
     * The official id of the agreement
     * 
     * @var string
     */
    public $id_number;

    /**
     * The refresh date of the agreement
     * 
     * @var \MongoDate
     */
    public $renew_date;

    /**
     * The start date of the agreement
     * 
     * @var \MongoDate
     */
    public $start_date;

    public function getId()
    {
        return $this->_id;
    }

    public function getChildrenPermissions()
    {
        return $this->children_permissions;
    }

    public function getIdNumber()
    {
        return $this->id_number;
    }

    public function getRenewDate()
    {
        return $this->renew_date;
    }

    public function getStartDate()
    {
        return $this->start_date;
    }

    public function setChildrenPermissions($children_permissions)
    {
        $this->children_permissions = $children_permissions;
        return $this;
    }

    public function setIdNumber($id_number)
    {
        $this->id_number = $id_number;
        return $this;
    }

    public function setRenewDate(\MongoDate $renew_date)
    {
        $this->renew_date = $renew_date;
        return $this;
    }

    public function setStartDate(\MongoDate $start_date)
    {
        $this->start_date = $start_date;
        return $this;
    }

}
