<?php
namespace Nannyster\Models;


/**
 * SuccessLogins
 * This model registers successfull logins registered users have made
 */
class SuccessLogins extends BaseModel
{

    /**
     * The success login id
     * 
     * @var string
     */
    public $_id;

    /**
     * The success login user id
     * 
     * @var \MongoId
     */
    public $user;

    /**
     * The user object
     * 
     * @var \Nannyster\Models\Users
     */
    protected $user_object = null;

    /**
     * The ip address of the success login
     * 
     * @var string
     */
    public $ip_address;

    /**
     * The user agent of the success login
     * 
     * @var string
     */
    public $user_agent;

    public function getId()
    {
        return $this->_id;
    }

    public function getUser()
    {
        if ($this->user_object === null && $this->user !== null) {
            $this->user_object = Users::findById($this->user);
        }
        return $this->user_object;
    }

    public function getIpAddress()
    {
        return $this->ip_address;
    }

    public function getUserAgent()
    {
        return $this->user_agent;
    }

    public function setUser(\Nannyster\Models\Users $user)
    {
        $this->user_object = $user;
        $this->user = $user->getId();
        return $this;
    }

    public function setIpAddress($ip_address)
    {
        $this->ip_address = $ip_address;
        return $this;
    }

    public function setUserAgent($user_agent)
    {
        $this->user_agent = $user_agent;
        return $this;
    }

}
