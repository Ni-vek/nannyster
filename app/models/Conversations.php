<?php

namespace Nannyster\Models;

class Conversations extends BaseModel
{
    
    /**
     *
     * @var \MongoId
     */
    public $_id;

    /**
     *
     * @var string
     */
    public $subject;

    /**
     *
     * @var \MongoDate
     */
    public $last_message_date;
    
    /**
     * All messages in a conversation
     * 
     * @var \Nannyster\Models\Messages
     */
    protected $conversation_messages = null;
    
    public function getId()
    {
        return $this->_id;
    }

    public function getSubject()
    {
        return $this->subject;
    }

    public function getLastMessageDate()
    {
        return $this->last_message_date;
    }
    
    public function getConversationMessages(){
        if($this->conversation_messages === null){
            $this->conversation_messages = Messages::find(array(array(
                'conversation' => $this->getId()
            )));
        }
        return $this->conversation_messages;
    }

    public function setSubject($subject)
    {
        $this->subject = $subject;
        return $this;
    }

    public function setLastMessageDate(\MongoDate $last_message_date)
    {
        $this->last_message_date = $last_message_date;
        return $this;
    }



}