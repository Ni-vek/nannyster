<?php

namespace Nannyster\Models;

use Phalcon\Mvc\Collection;
use Nannyster\Utils\CamelCase;

abstract class BaseModel extends Collection
{

    public function hydrate(array $datas)
    {
        foreach ($datas as $k => $v) {
            $setter = 'set' . CamelCase::to_camel_case($k, true);
            $this->$setter($v);
        }
    }

    public static function returnArrayForSelect($obj)
    {
        $array = array();
        foreach ($obj as $v) {
            $array[(string) $v['_id']] = $v['name'];
        }
        return $array;
    }

}
