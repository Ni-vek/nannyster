<?php

namespace Nannyster\Models;

class Invoices extends BaseModel
{

    /**
     * The invoice id
     * 
     * @var \MongoId
     */
    public $_id;

    /**
     * The invoice date
     * 
     * @var \MongoDate
     */
    public $date;

    /**
     * The subscription
     * 
     * @var \Nannyster\Models\Subscriptions
     */
    public $subscription;

    /**
     * The user id
     * 
     * @var \MongoId
     */
    public $user_id;

    /**
     * The user
     * 
     * @var \Nannyster\Models\Users
     */
    public $user;

    /**
     * The transaction linked
     * 
     * @var \Nannyster\Models\Transactions
     */
    public $transaction;

    public function afterValidationOnCreate()
    {
        $this->setDate(new \MongoDate());
    }

    public function getId()
    {
        return $this->_id;
    }

    public function getDate()
    {
        return $this->date;
    }

    public function getSubscription()
    {
        return unserialize($this->subscription);
    }

    public function getUser()
    {
        return unserialize($this->user);
    }

    public function getUserId()
    {
        return $this->user_id;
    }

    public function getTransaction()
    {
        return unserialize($this->transaction);
    }

    public function setDate(\MongoDate $date)
    {
        $this->date = $date;
        return $this;
    }

    public function setSubscription(\Nannyster\Models\Subscriptions $subscription)
    {
        $this->subscription = serialize($subscription);
        return $this;
    }

    public function setUser(\Nannyster\Models\Users $user)
    {
        $this->user = serialize($user);
        $this->user_id = $user->getId();
        return $this;
    }

    public function setTransaction($transaction)
    {
        $this->transaction = serialize($transaction);
        return $this;
    }

}
