<?php

namespace Nannyster\Models;

/**
 * EmailConfirmations
 * Stores the reset password codes and their evolution
 */
class EmailConfirmations extends BaseModel
{

    /**
     *
     * @var \MongoId
     */
    public $_id;

    /**
     * The user id
     * 
     * @var \MongoId
     */
    public $user = null;

    /**
     * The user object
     * 
     * @var \Nannyster\Models\Users 
     */
    private $user_object;

    /**
     *
     * @var string
     */
    public $code;

    /**
     * 
     * @var \MongoDate
     */
    public $created;

    /**
     *
     * @var \MongoDate
     */
    public $modified;

    /**
     *
     * @var boolean
     */
    public $confirmed;

    public function getId()
    {
        return $this->_id;
    }

    public function getUser()
    {
        if ($this->user !== null && $this->user_object === null) {
            $this->user_object = Users::findById($this->user);
        }
        return $this->user_object;
    }

    public function getCode()
    {
        return $this->code;
    }

    public function getCreated()
    {
        return $this->created;
    }

    public function getModified()
    {
        return $this->modified;
    }

    public function getConfirmed()
    {
        return $this->confirmed;
    }

    public function setUser(\Nannyster\Models\Users $user)
    {
        $this->user = $user->getId();
        $this->user_object = $user;
        return $this;
    }

    public function setCode($code)
    {
        $this->code = $code;
        return $this;
    }

    public function setCreated(\MongoDate $created)
    {
        $this->created = $created;
        return $this;
    }

    public function setModified(\MongoDate $modified)
    {
        $this->modified = $modified;
        return $this;
    }

    public function setConfirmed($confirmed)
    {
        $this->confirmed = $confirmed;
        return $this;
    }

    /**
     * Before create the user assign a password
     */
    public function beforeValidationOnCreate()
    {
        // Timestamp the confirmaton
        $this->setCreated(new \MongoDate())

                // Generate a random confirmation code
                ->setCode(preg_replace('/[^a-zA-Z0-9]/', '', base64_encode(openssl_random_pseudo_bytes(24))))

                // Set status to non-confirmed
                ->setConfirmed(false);
    }

    /**
     * Sets the timestamp before update the confirmation
     */
    public function beforeValidationOnUpdate()
    {
        // Timestamp the confirmaton
        $this->setModified(new \MongoDate());
    }

    /**
     * Send a confirmation e-mail to the user after create the account
     */
    public function afterCreate()
    {
        $user = $this->getUser();
        if ($user->getCreatedByNanny()) {
            $this->getDI()->getMail()->send(
                    array(
                        'email' => $user->getEmail(),
                        'name' => $user->getFormatedName(),
                        'type' => 'to'
                    ), 
                    $user->getParent()->getFormatedName() . ' vous a créé un compte sur ' . $this->config->application->siteTitle . '. Veuillez confirmer votre email',
                    'confirmationFromNanny', 
                    array(
                        'confirmUrl' => '/confirm/' . $this->getCode() . '/' . $user->getEmail(),
                        'password' => $user->getUncryptedPassword(),
                        'creator' => $user->getParent()->getFormatedName()
                    ),
                    'Confirmation inscription'
            );
        }
        else {
            $this->getDI()->getMail()->send(
                    array(
                'email' => $user->getEmail(),
                'name' => $user->getFormatedName(),
                'type' => 'to'
                    ), 'Veuillez confirmer votre email', 'confirmation', array(
                'confirmUrl' => '/confirm/' . $this->getCode() . '/' . $user->getEmail(),
                    ), 'Confirmation inscription'
            );
        }
    }

}
