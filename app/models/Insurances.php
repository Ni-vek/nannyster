<?php

namespace Nannyster\Models;

class Insurances extends BaseModel
{

    /**
     * The insurance object ID
     * 
     * @var \MongoID
     */
    public $_id;

    /**
     * The Address
     * 
     * @var strign
     */
    public $address;

    /**
     * The city id
     * 
     * @var string
     */
    public $city;

    /**
     * The city
     * 
     * @var \Nannyster\Models\Cities
     */
    protected $city_object;

    /**
     * The insurance compagny name
     * 
     * @var string
     */
    public $compagny_name;

    /**
     * The policy number of the insurance
     * 
     * @var string
     */
    public $policy_nb;

    /**
     * The validity date of the insurance
     * 
     * @var \MongoDate
     */
    public $validity_date;

    public function getId()
    {
        return $this->_id;
    }

    public function getAddress()
    {
        return $this->address;
    }

    public function getCity()
    {
        if ($this->city_object === null && $this->city !== null) {
            $this->city_object = Cities::findById($this->city);
        }
        return $this->city_object;
    }

    public function getCompagnyName()
    {
        return $this->compagny_name;
    }

    public function getPolicyNb()
    {
        return $this->policy_nb;
    }

    public function getValidityDate()
    {
        return $this->validity_date;
    }

    public function setAddress($address)
    {
        $this->address = $address;
        return $this;
    }

    public function setCity(\Nannyster\Models\Cities $city)
    {
        $this->city = $city->getId();
        $this->city_object = $city;
    }

    public function setCompagnyName($compagny_name)
    {
        $this->compagny_name = $compagny_name;
        return $this;
    }

    public function setPolicyNb($policy_nb)
    {
        $this->policy_nb = $policy_nb;
        return $this;
    }

    public function setValidityDate(\MongoDate $validity_date)
    {
        $this->validity_date = $validity_date;
        return $this;
    }

}
