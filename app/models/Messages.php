<?php

namespace Nannyster\Models;

class Messages extends BaseModel
{
    
    /**
     * The message id
     * @var \MongoId
     */
    public $_id;

    /**
     * The user id
     * 
     * @var \MongoId
     */
    public $user;
    
    /**
     * The user object
     * 
     * @var \Nannyster\Models\Users
     */
    protected $user_object = null;

    /**
     * The conversation id
     * 
     * @var \MongoId
     */
    public $conversation;
    
    /**
     * The conversation object
     * 
     * @var \Nannyster\Models\Conversations
     */
    protected $conversation_object = null;

    /**
     * The text message
     * 
     * @var string
     */
    public $message;

    /**
     * The message's date
     * 
     * @var \MongoDate
     */
    public $date;
    
    public function getId()
    {
        return $this->_id;
    }

    public function getUser()
    {
        if($this->user_object === null && $this->user !== null){
            $this->user_object = Users::findById($this->user);
        }
        return $this->user_object;
    }

    public function getConversation()
    {
        if($this->conversation_object === null && $this->conversation !== null){
            $this->conversation_object = Conversations::findById($this->conversation);
        }
        return $this->conversation_object;
    }

    public function getMessage()
    {
        return $this->message;
    }

    public function getDate()
    {
        return $this->date;
    }

    public function setUser(\Nannyster\Models\Users $user)
    {
        $this->user = $user->getId();
        $this->user_object = $user;
        return $this;
    }

    public function setConversation(\Nannyster\Models\Conversations $conversation)
    {
        $this->conversation = $conversation->getId();
        $this->conversation_object = $conversation;
        return $this;
    }

    public function setMessage($message)
    {
        $this->message = $message;
        return $this;
    }

    public function setDate(\MongoDate $date)
    {
        $this->date = $date;
        return $this;
    }



}