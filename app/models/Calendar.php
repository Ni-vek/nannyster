<?php

namespace Nannyster\Models;

class Calendar extends BaseModel
{

    /**
     * The event id
     * 
     * @var \MongoId
     */
    public $_id;

    /**
     * The event start timestamp
     * 
     * @var string
     */
    public $start;

    /**
     * The event end timestamp
     * 
     * @var string
     */
    public $end;

    /**
     * The contract object
     * 
     * @var \Nannyster\Models\Calendars
     */
    protected $contract;

    /**
     * The contract id
     * Not saved as MongoId object for search prupose only!
     * 
     * @var string
     */
    public $contract_id;

    /**
     * The event title
     * 
     * @var string
     */
    public $title;

    /**
     * The event creator id
     * 
     * @var \MongoId
     */
    public $creator_id;

    /**
     * The event css class
     * 
     * @var string
     */
    public $className;

    /**
     * Does the event is all day long?
     * 
     * @var boolean
     */
    public $allDay = false;

    /**
     * Does the event is editable?
     * 
     * @var boolean
     */
    public $editable = true;

    /**
     * The event creation date
     * 
     * @var \MongoDate
     */
    public $created;

    /**
     * The event modification date
     * 
     * @var \MongoDate
     */
    public $modified;

    public function beforeValidationOnCreate()
    {
        $this->created = time();
    }

    public function beforeValidationOnUpdate()
    {

        $this->modified = time();
    }
    
    public function getId()
    {
        return $this->_id;
    }

    public function getStart()
    {
        return $this->start;
    }

    public function getEnd()
    {
        return $this->end;
    }
    
    public function getContract(){
        if($this->contract_id !== null && $this->contract === null){
            $this->contract = Contracts::findById(new \MongoId($this->contract_id));
        }
        return $this->contract;
    }

    public function getContracId()
    {
        return $this->contract_id;
    }

    public function getTitle()
    {
        return $this->title;
    }

    public function getCreatorId()
    {
        return $this->creator_id;
    }

    public function getClassName()
    {
        return $this->className;
    }

    public function getAllDay()
    {
        return $this->allDay;
    }

    public function getEditable()
    {
        return $this->editable;
    }

    public function getCreated()
    {
        return $this->created;
    }

    public function getModified()
    {
        return $this->modified;
    }

    public function setStart($start)
    {
        $this->start = (int) $start;
        return $this;
    }

    public function setEnd($end)
    {
        $this->end = (int) $end;
        return $this;
    }

    public function setContractId($contract_id)
    {
        $this->contract_id = $contract_id;
        return $this;
    }

    public function setTitle($title)
    {
        $this->title = $title;
        return $this;
    }

    public function setCreatorId(\MongoId $creator_id)
    {
        $this->creator_id = $creator_id;
        return $this;
    }

    public function setClassName($className)
    {
        $this->className = $className;
        return $this;
    }

    public function setAllDay($allDay)
    {
        $this->allDay = $allDay;
        return $this;
    }

    public function setEditable($editable)
    {
        $this->editable = $editable;
        return $this;
    }

    public function setCreated(\MongoDate $created)
    {
        $this->created = $created;
        return $this;
    }

    public function setModified(\MongoDate $modified)
    {
        $this->modified = $modified;
        return $this;
    }
    
}
