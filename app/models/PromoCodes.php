<?php

namespace Nannyster\Models;

class PromoCodes extends BaseModel
{

    /**
     * The promo code id
     * 
     * @var \MongoId
     */
    public $_id;

    /**
     * The promo code name
     * 
     * @var string
     */
    public $name;

    /**
     * The promo code
     * 
     * @var string
     */
    public $code;

    /**
     * The promo code amoount
     * 
     * @var double
     */
    public $amount;

    /**
     * The promo code start date
     * 
     * @var \MongoDate
     */
    public $start_date;

    /**
     * The promo code end date
     * 
     * @var \MongoDate
     */
    public $end_date;

    /**
     * Define if this promo code is cumulable or not
     * 
     * @var boolean
     */
    public $cumulable;

    /**
     * The user id that is allowed to use this promo code
     * 
     * @var \MongoId
     */
    public $user;

    /**
     * The user object
     * 
     * @var \Nannyster\Models\Users
     */
    public $user_object;

    /**
     * The subscription that is linked with this promo code
     * 
     * @var \MongoId
     */
    public $subscription;

    /**
     * The subscription object
     * 
     * @var \Nannyster\Models\Subscriptions
     */
    public $subscription_object;

    public function getId()
    {
        return $this->_id;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getCode()
    {
        return $this->code;
    }

    public function getAmount()
    {
        return $this->amount;
    }

    public function getStartDate()
    {
        return $this->start_date;
    }

    public function getEndDate()
    {
        return $this->end_date;
    }

    public function getCumulable()
    {
        return $this->cumulable;
    }

    public function getUser()
    {
        if ($this->user_object === null && $this->user !== null) {
            $this->user_object = Users::findById($this->user);
        }
        return $this->user_object;
    }

    public function getSubscription()
    {
        if ($this->subscription_object === null && $this->subscription !== null) {
            $this->subscription_object = Subscriptions::findById($this->subscription);
        }
        return $this->subscription_object;
    }

    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    public function setCode($code)
    {
        $this->code = $code;
        return $this;
    }

    public function setAmount($amount)
    {
        $this->amount = $amount;
        return $this;
    }

    public function setStartDate(\MongoDate $start_date)
    {
        $this->start_date = $start_date;
        return $this;
    }

    public function setEndDate(\MongoDate $end_date)
    {
        $this->end_date = $end_date;
        return $this;
    }

    public function setCumulable($cumulable)
    {
        $this->cumulable = $cumulable;
        return $this;
    }

    public function setUser(\Nannyster\Models\Users $user)
    {
        $this->user = $user->getId();
        $this->user_object = $user;
        return $this;
    }

    public function setSubscription(\Nannyster\Models\Subscriptions $subscription)
    {
        $this->subscription = $subscription->getId();
        $this->subscription_object = $subscription;
        return $this;
    }

}
