<?php

namespace Nannyster\Models;

class Mailboxes extends BaseModel
{

    /**
     *
     * @var \MongoId
     */
    public $_id;

    /**
     * The user id
     * 
     * @var \MongoId
     */
    public $user;

    /**
     * The user object
     * 
     * @var \Nannyster\Models\Users
     */
    protected $user_object = null;

    /**
     * The conversaiton id
     * 
     * @var \MongoId
     */
    public $conversation;

    /**
     * The conversation object
     * 
     * @var \Nannyster\Models\Conversations
     */
    protected $conversation_object = null;

    /**
     * Is there unread messages
     * @var boolean
     */
    public $unread_messages = false;

    /**
     * Is it deleted
     * 
     * @var boolean
     */
    public $conversation_deleted = false;
    
    public function getId()
    {
        return $this->_id;
    }

    public function getUser()
    {
        if($this->user_object === null && $this->user !== null){
            $this->user_object = Users::findById($this->user);
        }
        return $this->user_object;
    }

    public function getConversation()
    {
        if($this->conversation_object === null && $this->conversation !== null){
            $this->conversation_object = Conversations::findById($this->conversation);
        }
        return $this->conversation_object;
    }

    public function getUnreadMessages()
    {
        return $this->unread_messages;
    }

    public function getConversationDeleted()
    {
        return $this->conversation_deleted;
    }

    public function setUser(\Nannyster\Models\Users $user)
    {
        $this->user = $user->getId();
        $this->user_object = $user;
        return $this;
    }

    public function setConversation(\Nannyster\Models\Conversations $conversation)
    {
        $this->conversation = $conversation->getId();
        $this->conversation_object = $conversation;
        return $this;
    }

    public function setUnreadMessages($unread_messages)
    {
        $this->unread_messages = $unread_messages;
        return $this;
    }

    public function setConversationDeleted($conversation_deleted)
    {
        $this->conversation_deleted = $conversation_deleted;
        return $this;
    }


}
