<?php

namespace Nannyster\Models;

class MoneyRates extends BaseModel
{

    /**
     * The money rate object ID
     * 
     * @var \MongoID
     */
    public $_id;

    /**
     * The breakfast costs
     * 
     * @var double
     */
    public $breakfast_costs;

    /**
     * The diner costs
     * 
     * @var double
     */
    public $diner_costs;

    /**
     * The hourly rate
     * @var doule
     */
    public $hourly_rate;

    /**
     * The lunch costs
     * 
     * @var double
     */
    public $lunch_costs;

    /**
     * The maintenance allowance
     * 
     * @var double
     */
    public $maintenance_allowance;

    /**
     * The snack costs
     * 
     * @var double
     */
    public $snack_costs;

    public function getId()
    {
        return $this->_id;
    }

    public function getBreakfastCosts()
    {
        return $this->breakfast_costs;
    }

    public function getDinerCosts()
    {
        return $this->diner_costs;
    }

    public function getHourlyRate()
    {
        return $this->hourly_rate;
    }

    public function getLunchCosts()
    {
        return $this->lunch_costs;
    }

    public function getMaintenanceAllowance()
    {
        return $this->maintenance_allowance;
    }

    public function getSnackCosts()
    {
        return $this->snack_costs;
    }

    public function setBreakfastCosts($breakfast_costs)
    {
        $this->breakfast_costs = $breakfast_costs;
        return $this;
    }

    public function setDinerCosts($diner_costs)
    {
        $this->diner_costs = $diner_costs;
        return $this;
    }

    public function setHourlyRate($hourly_rate)
    {
        $this->hourly_rate = $hourly_rate;
        return $this;
    }

    public function setLunchCosts($lunch_costs)
    {
        $this->lunch_costs = $lunch_costs;
        return $this;
    }

    public function setMaintenanceAllowance($maintenance_allowance)
    {
        $this->maintenance_allowance = $maintenance_allowance;
        return $this;
    }

    public function setSnackCosts($snack_costs)
    {
        $this->snack_costs = $snack_costs;
        return $this;
    }

}
