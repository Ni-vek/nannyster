<?php

namespace Nannyster\Models;

use Nannyster\Models\Cities;

class Addresses extends BaseModel
{

    /**
     * The address object ID
     * 
     * @var \MongoID
     */
    public $_id;

    /**
     * The first part of the address
     * 
     * @var string
     */
    public $address_1;

    /**
     * The second part of the address
     * 
     * @var string
     */
    public $address_2;

    /**
     * The third part of the address
     * 
     * @var string
     */
    public $address_3;

    /**
     * The city and zip code _id
     * 
     * @var \MongoID
     */
    public $city;

    /**
     * The city and zip code object
     * 
     * @var Nannyster\Models\Cities
     */
    private $city_object;

    public function getId()
    {
        return $this->_id;
    }

    public function getAddress1()
    {
        return ucwords($this->address_1);
    }

    public function getAddress2()
    {
        return ucwords($this->address_2);
    }

    public function getAddress3()
    {
        return ucwords($this->address_3);
    }

    public function getAddresses($separator = ' ')
    {
        $address = $this->getAddress1();
        if ($this->getAddress2() !== '') {
            $address .= ',' . $separator . $this->getAddress2();
        }
        if ($this->getAddress3() !== '') {
            $address .= ',' . $separator . $this->getAddress3();
        }
        return $address;
    }

    /**
     * Return the city object
     * 
     * @return \Nannyster\Models\Cities
     */
    public function getCity()
    {
        if ($this->city_object === null && $this->city !== null) {
            $this->setCity(Cities::findById($this->city));
        }
        return $this->city_object;
    }

    public function setAddress1($address_1)
    {
        $this->address_1 = $address_1;
        return $this;
    }

    public function setAddress2($address_2)
    {
        $this->address_2 = $address_2;
        return $this;
    }

    public function setAddress3($address_3)
    {
        $this->address_3 = $address_3;
        return $this;
    }

    public function setCity(\Nannyster\Models\Cities $city)
    {
        $this->city_object = $city;
        $this->city = $city->getId();
        return $this;
    }

    public function fetchForAjax()
    {
        $this->getCity();
        $this->city = $this->city_object;
    }

}
