<?php

namespace Nannyster\Models;

use Nannyster\Utils\ArrayCollection;
use Nannyster\Utils\Slug;

class Users extends BaseModel
{

    /**
     * The user object ID
     * 
     * @var \MongoID
     */
    public $_id;

    /**
     * Define if the user is active
     * 
     * @var boolean
     */
    public $active = false;

    /**
     * An array Address _id
     * 
     * @var array
     */
    public $address;

    /**
     * An ArrayCollection of Nannyster\Models\Address
     * 
     * @var \Nannyster\Models\Addresses 
     */
    protected $address_object = null;

    /**
     * The nanny's agreemnt
     * 
     * @var \MongoID
     */
    public $agreement;

    /**
     * The nanny's agreemnt
     * 
     * @var Nannyster\Models\Agreements
     */
    private $agreement_object = null;

    /**
     * Tne nanny's auto insurance
     * 
     * @var \MongoID
     */
    public $auto_insurance;

    /**
     * Tne nanny's auto insurance
     * 
     * @var Nannyster\Models\Insurances
     */
    private $auto_insurance_object = null;

    /**
     * User's avatar file
     * 
     * @var string
     */
    public $avatar_file = 'default.jpg';

    /**
     * Define if the user is banned
     * 
     * @var boolean
     */
    public $banned = false;

    /**
     * User's birth city
     * 
     * @var \MongoID 
     */
    public $birth_city;

    /**
     * User's birth city
     * 
     * @var Nannyster\Models\Cities
     */
    private $birth_city_object = null;

    /**
     * The user's birth date
     * 
     * @var \MongoDate
     */
    public $birth_date;

    /**
     * User's civility
     * 
     * @var \MongoID
     */
    public $civility;

    /**
     * User's civility
     * 
     * @var Nannyster\Models\Civilities
     */
    private $civility_object = null;

    /**
     * Tne nanny's civil insurance
     * 
     * @var \MongoID 
     */
    public $civil_responsability;

    /**
     * Tne nanny's civil insurance
     * 
     * @var Nannyster\Models\Insurances
     */
    private $civil_responsability_object = null;

    /**
     * The creation date
     * 
     * @var \MongoDate
     */
    public $created;

    /**
     * Does this account has been created by a nanny
     * 
     * @var boolean
     */
    public $created_by_nanny = false;

    /**
     * Define if user's informations are visible in Nannyster directory
     * 
     * @var boolean 
     */
    public $directory_private_informations = false;

    /**
     * Define if the user is visible in Nannyster directory
     * 
     * @var boolean
     */
    public $directory_visible = true;

    /**
     * User's email
     * 
     * @var string
     */
    public $email;

    /**
     * Define if the user agrees with receiving each notifications by email
     * 
     * @var boolean
     */
    public $email_notifications = true;

    /**
     * User's firstname
     * 
     * @var string
     */
    public $firstname;

    /**
     * An array of fullname
     * 
     * @var array
     */
    public $fullname;

    /**
     * An ArrayCollection of fullname
     * 
     * @var Nannyster\Utils\ArrayCollection
     */
    private $fullname_object = null;

    /**
     * An array of invoices
     * 
     * @var array
     */
    public $invoices;

    /**
     * An ArrayCollection of invoices
     * 
     * @var Nannyster\Utils\ArrayCollection
     */
    private $invoices_object = null;

    /**
     * User's mobile number
     * 
     * @var string
     */
    public $mobile;

    /**
     * The Nanny's costs
     * 
     * @var \MongoID
     */
    public $money_rate;

    /**
     * The Nanny's costs
     * 
     * @var Nannyster\Models\MoneyRates
     */
    private $money_rate_object = null;

    /**
     * Define if the user must change his password
     * 
     * @var boolean
     */
    public $must_change_password = false;

    /**
     * User's pajemploi number
     * 
     * @var string
     */
    public $pajemploi_nb;

    /**
     * The user's parent
     * It can only be a nanny for a parent or null
     *
     * @var \MongoID
     */
    public $parent;

    /**
     * The user's parent
     * It can only be a nanny for a parent or null
     *
     * @var Nannyster\Models\Users
     */
    private $parent_object = null;

    /**
     * User's password
     * 
     * @var string
     */
    public $password;

    /**
     * User's phone
     * 
     * @var string
     */
    public $phone;

    /**
     * Define if the user's personal informations are public or not
     * If it's true, $directory_private_informations must be also true
     * 
     * @var boolean
     */
    public $private_informations = true;

    /**
     * User's profile
     * 
     * @var \MongoID 
     */
    public $profile;

    /**
     * User's profile
     * 
     * @var Nannyster\Models\Profiles
     */
    private $profile_object = null;

    /**
     * User's security number
     * 
     * @var string
     */
    public $social_security_nb;

    /**
     * The current subscription id
     * 
     * @var \MongoId
     */
    public $subscription = null;

    /**
     * The current subscription object
     * 
     * @var \Nannyster\Models\Subscriptions
     */
    private $subscription_object;

    /**
     * User's surname
     * 
     * @var string
     */
    public $surname;

    /**
     * Define if the user is suspended
     * 
     * @var boolean
     */
    public $suspended = false;

    /**
     * Define if the terms and conditions have been accepted
     * @var boolean
     */
    public $terms = false;

    /**
     * User's update date
     * 
     * @var \MongoDate
     */
    public $updated;

    /**
     * The uncrypted password.
     * Needed only when a nanny create an account
     * 
     * @var string
     */
    protected $uncrypted_password;

    /**
     * User's wedding surname
     * 
     * @var string
     */
    public $wedding_surname = null;

    public function onConstruct($datas = null)
    {

        if ($datas) {
            $this->hydrate($datas);
        }
        $this->fullname_object = new ArrayCollection();
    }

    public function getId()
    {
        return $this->_id;
    }

    public function getActive()
    {
        return $this->active;
    }

    public function getAddress()
    {
        if ($this->address_object === null && !empty($this->address)) {
            $this->address_object = Addresses::findById($this->address);
        }
        return $this->address_object;
    }

    public function getAgreement()
    {
        if ($this->agreement_object === null && $this->agreement !== null) {
            $this->setAgreement(Agreements::findById($this->agreement));
        }
        return $this->agreement_object;
    }

    public function getAutoInsurance()
    {
        if ($this->auto_insurance_object === null && $this->auto_insurance !== null) {
            $this->setAutoInsurance(Insurances::findById($this->auto_insurance));
        }
        return $this->auto_insurance_object;
    }

    public function getAvatarFile()
    {
        return $this->avatar_file;
    }

    public function getBanned()
    {
        return $this->banned;
    }

    public function getBirthCity()
    {
        if ($this->birth_city_object === null && $this->birth_city !== null) {
            $this->setBirthCity(Cities::findById($this->birth_city));
        }
        return $this->birth_city_object;
    }

    public function getBirthDate()
    {
        return $this->birth_date;
    }

    public function getCivility()
    {
        if ($this->civility_object === null && $this->civility !== null) {
            $this->setCivility(Civilities::findById($this->civility));
        }
        return $this->civility_object;
    }

    public function getCivilResponsability()
    {
        if ($this->civil_responsability_object === null && $this->civil_responsability !== null) {
            $this->setCivilResponsability(Insurances::findById($this->civil_responsability));
        }
        return $this->civil_responsability_object;
    }

    public function getCreated()
    {
        return $this->created;
    }

    public function getCreatedByNanny()
    {
        return $this->created_by_nanny;
    }

    public function getDirectoryPrivateInformations()
    {
        return $this->directory_private_informations;
    }

    public function getDirectoryVisible()
    {
        return $this->directory_visible;
    }

    public function getEmail()
    {
        return $this->email;
    }

    public function getEmailNotifications()
    {
        return $this->email_notifications;
    }

    public function getFirstname()
    {
        return $this->firstname;
    }

    public function getFullnames()
    {
        return $this->fullname;
    }

    public function getFullname($key)
    {
        return $this->fullname[$key];
    }

    public function getInvoices()
    {
        if ($this->invoices_object === null && $this->invoices !== null) {
            foreach ($this->invoices as $invoice) {
                $this->invoices_object->add($invoice);
            }
        }
        return $this->invoices_object;
    }

    public function getInvoice($key)
    {
        if ($this->invoices_object === null && $this->invoices !== null) {
            foreach ($this->invoices as $invoice) {
                $this->invoices_object->add($invoice);
            }
        }
        return $this->invoices_object->get($key);
    }

    public function getFormatedName()
    {
        return ucwords($this->getFirstname()) . ' ' . (($this->getWeddingSurname() != null) ? strtoupper($this->getWeddingSurname()) : strtoupper($this->getSurname()));
    }

    public function getMobile()
    {
        return $this->mobile;
    }

    public function getMoneyRate()
    {
        if ($this->money_rate_object === null && $this->money_rate !== null) {
            $this->setMoneyRate(MoneyRates::findById($this->money_rate));
        }
        return $this->money_rate_object;
    }

    public function getMustChangePassword()
    {
        return $this->must_change_password;
    }

    public function getPajemploiNb()
    {
        return strtoupper($this->pajemploi_nb);
    }

    public function getParent()
    {
        if ($this->parent !== null && $this->parent_object === null) {
            $this->parent_object = Users::findById($this->parent);
        }
        return $this->parent_object;
    }

    public function getPassword()
    {
        return $this->password;
    }

    public function getPhone()
    {
        return $this->phone;
    }

    public function getPrivateInformations()
    {
        return $this->private_informations;
    }

    public function getProfile()
    {
        if ($this->profile_object === null && $this->profile !== null) {
            $this->setProfile(Profiles::findById($this->profile));
        }
        return $this->profile_object;
    }

    public function getProfileUrl()
    {
        return 'profil/' . Slug::generate($this->getFormatedName()) . '/' . $this->getId();
    }

    public function getSocialSecurityNb()
    {
        return $this->social_security_nb;
    }

    public function getSubscription()
    {
        if ($this->subscription_object === null && $this->subscription !== null) {
            $this->subscription_object = Subscriptions::findById($this->subscription);
        }
        return $this->subscription_object;
    }

    public function getSurname()
    {
        return $this->surname;
    }

    public function getSuspended()
    {
        return $this->suspended;
    }

    public function getTerms()
    {
        return $this->terms;
    }

    public function getUpdated()
    {
        return $this->updated;
    }

    public function getUncryptedPassword()
    {
        return $this->uncrypted_password;
    }

    public function setUncryptedPassword($uncrypted_password)
    {
        $this->uncrypted_password = $uncrypted_password;
        return $this;
    }

    public function getWeddingSurname()
    {
        return $this->wedding_surname;
    }

    public function setActive($active)
    {
        $this->active = $active;
        return $this;
    }

    public function setAddress(\Nannyster\Models\Addresses $address)
    {
        $this->address_object = $address;
        $this->address = $address->getId();
        return $this;
    }

    public function setAgreement(\Nannyster\Models\Agreements $agreement)
    {
        $this->agreement_object = $agreement;
        $this->agreement = $agreement->getId();
        return $this;
    }

    public function setAutoInsurance(\Nannyster\Models\Insurances $auto_insurance)
    {
        $this->auto_insurance_object = $auto_insurance;
        $this->auto_insurance = $auto_insurance->getId();
        return $this;
    }

    public function setAvatarFile($avatar_file)
    {
        $this->avatar_file = $avatar_file;
        return $this;
    }

    public function setBanned($banned)
    {
        $this->banned = $banned;
        return $this;
    }

    public function setBirthCity(\Nannyster\Models\Cities $birth_city)
    {
        $this->birth_city_object = $birth_city;
        $this->birth_city = $birth_city->getId();
        return $this;
    }

    public function setBirthDate(\MongoDate $date)
    {
        $this->birth_date = $date;
    }

    public function setCivility(\Nannyster\Models\Civilities $civility)
    {
        $this->civility_object = $civility;
        $this->civility = $civility->getId();
        return $this;
    }

    public function setCivilResponsability(\Nannyster\Models\Insurances $civil_responsability)
    {
        $this->civil_responsability_object = $civil_responsability;
        $this->civil_responsability = $civil_responsability->getId();
        return $this;
    }

    public function setCreated(\MongoDate $created)
    {
        $this->created = $created;
        return $this;
    }

    public function setCreatedByNanny($created)
    {
        $this->created_by_nanny = $created;
        return $this;
    }

    public function setDirectoryPrivateInformations($directory_private_informations)
    {
        $this->directory_private_informations = $directory_private_informations;
        return $this;
    }

    public function setDirectoryVisible($directory_visible)
    {
        $this->directory_visible = $directory_visible;
        return $this;
    }

    public function setEmail($email)
    {
        $this->email = $email;
        return $this;
    }

    public function setEmailNotifications($email_notifications)
    {
        $this->email_notifications = $email_notifications;
        return $this;
    }

    public function setFirstname($firstname)
    {
        $this->firstname = strtolower($firstname);
        return $this;
    }

    public function setFullname($fullname)
    {
        $this->fullname = $fullname;
        return $this;
    }

    public function setInvoice(\Nannyster\Models\Invoices $invoice)
    {
        $this->invoices_object->add($invoice);
        $this->invoices = $this->invoices_object->toArray();
        return $this;
    }

    public function setMobile($mobile)
    {
        $this->mobile = $mobile;
        return $this;
    }

    public function setMoneyRate(\Nannyster\Models\MoneyRates $money_rate)
    {
        $this->money_rate_object = $money_rate;
        $this->money_rate = $money_rate->getId();
        return $this;
    }

    public function setMustChangePassword($must_change_password)
    {
        $this->must_change_password = $must_change_password;
        return $this;
    }

    public function setPajemploiNb($pajemploi_nb)
    {
        $this->pajemploi_nb = $pajemploi_nb;
        return $this;
    }

    public function setParent(\Nannyster\Models\Users $parent)
    {
        $this->parent_object = $parent;
        $this->parent = $parent->getId();
        return $this;
    }

    public function setPassword($password)
    {
        $this->password = $password;
        return $this;
    }

    public function setPhone($phone)
    {
        $this->phone = $phone;
        return $this;
    }

    public function setPrivateInformations($private_informations)
    {
        $this->private_informations = $private_informations;
        return $this;
    }

    public function setProfile(\Nannyster\Models\Profiles $profile)
    {
        $this->profile_object = $profile;
        $this->profile = $profile->getId();
        return $this;
    }

    public function setSocialSecurityNb($social_security_nb)
    {
        $this->social_security_nb = $social_security_nb;
        return $this;
    }

    public function setSubscription(\Nannyster\Models\Subscriptions $subscription)
    {
        $this->subscription = $subscription->getId();
        $this->subscription_object = $subscription;
        return $this;
    }

    public function setSurname($surname)
    {
        $this->surname = strtolower($surname);
        return $this;
    }

    public function setSuspended($suspended)
    {
        $this->suspended = $suspended;
        return $this;
    }

    public function setTerms($terms)
    {
        if ($terms === 'true') {
            $this->terms = true;
        }
    }

    public function setUpdated(\MongoDate $updated)
    {
        $this->updated = $updated;
        return $this;
    }

    public function setWeddingSurname($wedding_surname)
    {
        $this->wedding_surname = $wedding_surname;
        return $this;
    }

    public function afterValidationOnCreate()
    {
        $this->setCreated(new \MongoDate());
        $this->setFullname(array($this->getFirstname() . ' ' . $this->getSurname(), $this->getSurname() . ' ' . $this->getFirstname()));
        if ($this->getAddress() === null) {
            $address = new Addresses();
            $address->save();
            $this->setAddress($address);
        }
    }

    public function afterValidationOnUpdate()
    {
        $this->setFullname(array($this->getFirstname() . ' ' . $this->getSurname(), $this->getSurname() . ' ' . $this->getFirstname()));
        $this->setUpdated(new \MongoDate());
    }

    /**
     * Send a e-mail confirmation to the user if the account is not active
     */
    public function afterCreate()
    {
        if (!$this->getSubscription()) {
            $default_subscription = SubscriptionsBase::findFirst(array(array(
                            'name' => $this->getDI()->getShared('config')->application->defaultSubscription
            )));

            $user_subscription = new Subscriptions();
            $user_subscription->setSubscriptionBase($default_subscription)
                    ->setStartDate(new \MongoDate())
                    ->setUser($this);

            if ($default_subscription->getDuration() > 0) {
                $user_subscription->setEndDate(new \MongoDate((strtotime(date('Y-m-d')) + 2678400 * $default_subscription->getDuration())));
            }

            $user_subscription->save();

            $this->setSubscription($user_subscription)
                    ->save();
        }
        if (!$this->getActive()) {
            $emailConfirmation = new EmailConfirmations();
            $emailConfirmation->setUser($this);
            if ($emailConfirmation->save() && !$this->getCreatedByNanny()) {
                $this->getDI()
                        ->getFlash()
                        ->notice('Un email de confirmation vient de vous être envoyé à l\'adresse ' . $this->getEmail() . ', pour activer votre compte, veuillez suivre les indications contenues dans l\'email');
            }
        }
    }

    public function fetchForSession()
    {
        $this->getAddress();
        $this->getAgreement();
        $this->getAutoInsurance();
        $this->getBirthCity();
        $this->getCivility();
        $this->getCivilResponsability();
        $this->getMoneyRate();
        $this->getParent();
        $this->getProfile();
        $this->getSubscription();
        $this->getSubscription()->getSubscriptionBase();
        return $this;
    }

    public function fetchForAjax()
    {
        $this->fetchForSession();
        $this->getAddress()->fetchForAjax();
        $this->address = $this->address_object;
        $this->agreement = $this->agreement_object;
        $this->auto_insurance = $this->auto_insurance_object;
        $this->birth_city = $this->birth_city_object;
        $this->civility = $this->civility_object;
        $this->civil_responsability = $this->civil_responsability_object;
        $this->money_rate = $this->money_rate_object;
        $this->parent = $this->parent_object;
        $this->profile = $this->profile_object;
        $this->subscription = $this->subscription_object;
    }

    public function randomPassword()
    {
        $alphabet = "abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789";
        $pass = array();
        $alphaLength = strlen($alphabet) - 1;
        for ($i = 0; $i < 8; $i++) {
            $n = rand(0, $alphaLength);
            $pass[] = $alphabet[$n];
        }
        return implode($pass);
    }

}
