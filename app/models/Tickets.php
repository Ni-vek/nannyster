<?php

namespace Nannyster\Models;

use Phalcon\Mvc\Collection;

class Tickets extends BaseModel
{
    /**
     *
     * @var \MongoId
     */
	public $_id;

    /**
     *
     * @var \MongoDate
     */
	public $created_at;

    /**
     *
     * @var MongoDate
     */
	public $solved_at = null;

    /**
     *
     * @var \MongoId
     */
	public $user_id;

    /**
     *
     * @var string
     */
	public $type;

    /**
     *
     * @var string
     */
	public $message;
    
    public function getId()
    {
        return $this->_id;
    }

    public function getCreatedAt()
    {
        return $this->created_at;
    }

    public function getSolvedAt()
    {
        return $this->solved_at;
    }

    public function getUserId()
    {
        return $this->user_id;
    }

    public function getType()
    {
        return $this->type;
    }

    public function getMessage()
    {
        return $this->message;
    }

    public function setId(\MongoId $id)
    {
        $this->_id = $id;
    }

    public function setCreatedAt(\MongoDate $created_at)
    {
        $this->created_at = $created_at;
    }

    public function setSolvedAt(MongoDate $solved_at)
    {
        $this->solved_at = $solved_at;
    }

    public function setUserId(\MongoId $user_id)
    {
        $this->user_id = $user_id;
    }

    public function setType($type)
    {
        $this->type = $type;
    }

    public function setMessage($message)
    {
        $this->message = $message;
    }

    
	public function beforeValidationOnCreate(){

		$this->setCreatedAt(new \MongoDate());

	}
}