<?php

namespace Nannyster\Models;

class Contracts extends BaseModel
{

    /**
     * The contract id
     * 
     * @var \MongoId
     */
    public $_id;

    /**
     * The child surname
     * 
     * @var string
     */
    public $child_surname;

    /**
     * The child firstname
     * 
     * @var string
     */
    public $child_firstname;

    /**
     * The child birth date
     * 
     * @var \MongoDate
     */
    public $child_birth_date;

    /**
     * The mother id
     * 
     * @var \MongoId
     */
    public $mother;
    
    /**
     * The mother object
     * 
     * @var \Nannyster\Models\Users
     */
    public $mother_oject;

    /**
     * The father id
     * 
     * @var \MongoId
     */
    public $father;
    
    /**
     * The father object
     * 
     * @var \Nannyster\Models\Users
     */
    public $father_object;

    /**
     * The nanny id
     * 
     * @var \MongoId
     */
    public $nanny;
    
    /**
     * The nanny object
     * 
     * @var \Nannyster\Models\Users
     */
    public $nanny_object;

    /**
     * The contract start daate
     * 
     * @var \MongoDate
     */
    public $contract_start_date;

    /**
     * The try period time
     * ie. 86400
     * 
     * @var integer
     */
    public $try_period_time;

    /**
     * The type of guard
     * ie. full year guard, partial year guard...
     * 
     * @var string
     */
    public $guard_type;

    /**
     * The number of hour to work each single week
     * 
     * @var integer
     */
    public $nb_hour_weekly;

    /**
     * The delay to prevent the nanny about guard week changes
     * 
     * @var string
     */
    public $time_change_week_nb;

    /**
     * The daiy of the week where the nanny doesn't work
     * 
     * @var string
     */
    public $weekly_rest;

    /**
     * The nanny hourly rate
     * 
     * @var float
     */
    public $hourly_rate;

    /**
     *
     * @var 
     */
    public $hourly_rate_net;
    
    /**
     * 
     * @var 
     */
    public $occasional_childcare_monthly_hours;
    
    /**
     * 
     * @var 
     */
    public $partial_year_week_nb;

    /**
     *
     * @var 
     */
    public $hours_maj_rate;

    /**
     *
     * @var 
     */
    public $hours_maj_value;

    /**
     *
     * @var 
     */
    public $difficulty_maj_rate;

    /**
     *
     * @var 
     */
    public $difficulty_maj_value;

    /**
     *
     * @var 
     */
    public $maintenance_allowance;

    /**
     *
     * @var 
     */
    public $meal_given;

    /**
     *
     * @var 
     */
    public $breakfast_costs;

    /**
     *
     * @var 
     */
    public $lunch_costs;

    /**
     *
     * @var 
     */
    public $snack_costs;

    /**
     *
     * @var 
     */
    public $diner_costs;

    /**
     *
     * @var 
     */
    public $paiement_date;

    /**
     *
     * @var 
     */
    public $fractionnement_conges_du_1;

    /**
     *
     * @var 
     */
    public $fractionnement_conges_au_1;

    /**
     *
     * @var 
     */
    public $fractionnement_conges_du_2;

    /**
     *
     * @var 
     */
    public $fractionnement_conges_au_2;

    /**
     *
     * @var 
     */
    public $vacancy_payment_modality;

    /**
     *
     * @var 
     */
    public $premier_janvier;

    /**
     *
     * @var 
     */
    public $paques;

    /**
     *
     * @var 
     */
    public $huit_mai;

    /**
     *
     * @var 
     */
    public $ascension;

    /**
     *
     * @var 
     */
    public $pentecote;

    /**
     *
     * @var 
     */
    public $quatorze_juillet;

    /**
     *
     * @var 
     */
    public $quinze_aout;

    /**
     *
     * @var 
     */
    public $toussaint;

    /**
     *
     * @var 
     */
    public $onze_nov;

    /**
     *
     * @var 
     */
    public $noel;

    /**
     *
     * @var 
     */
    public $adaptation_period_modality;

    /**
     *
     * @var 
     */
    public $detail_hour_per_day;

    /**
     *
     * @var 
     */
    public $special_guard_modality;

    /**
     * The week number where the nanny works
     * 
     * @var string
     */
    public $guard_week_nb;

    /**
     * The week number where the nanny doesn't work
     * @var 
     */
    public $vacancy_week_nb;

    /**
     *
     * @var 
     */
    public $salaire_mensuel_brut;

    /**
     *
     * @var 
     */
    public $color;

    /**
     *
     * @var 
     */
    public $created;

    /**
     *
     * @var 
     */
    public $valide = false;

    /**
     *
     * @var 
     */
    public $closed = false;

    public function beforeValidationOnCreate()
    {
        
    }

    public function beforeValidationOnUpdate()
    {
        $color = array('grey', 'success', 'danger', 'purple', 'yellow', 'pink', 'info', 'primary', 'dark', 'orange', 'lime', 'magenta', 'brown', 'teal');       
        $this->setColor($color[array_rand($color, 1)]);
        $this->setCreated(new \MongoDate());
    }
    
    public function getId()
    {
        return $this->_id;
    }

    public function getChildSurname()
    {
        return $this->child_surname;
    }

    public function getChildFirstname()
    {
        return $this->child_firstname;
    }

    /**
     * Return the child birth date
     * 
     * @return \MongoDate
     */
    public function getChildBirthDate()
    {
        return $this->child_birth_date;
    }

    /**
     *  Return the mother object
     * 
     * @return \Nannyster\Models\Users
     */
    public function getMother()
    {
        return unserialize($this->mother_oject);
    }

    public function getMotherId()
    {
        return $this->mother;
    }

    /**
     *  Return the father object
     * 
     * @return \Nannyster\Models\Users
     */
    public function getFather()
    {
        return unserialize($this->father_object);
    }

    public function getFatherId()
    {
        return $this->father;
    }

    /**
     *  Return the nanny object
     * 
     * @return \Nannyster\Models\Users
     */
    public function getNanny()
    {
        return unserialize($this->nanny_object);
    }

    public function getNannyId()
    {
        return $this->nanny;
    }

    public function getContractStartDate()
    {
        return $this->contract_start_date;
    }

    public function getTryPeriodTime()
    {
        return $this->try_period_time;
    }

    public function getGuardType()
    {
        return $this->guard_type;
    }

    public function getNbHourWeekly()
    {
        return $this->nb_hour_weekly;
    }

    public function getTimeChangeWeekNb()
    {
        return $this->time_change_week_nb;
    }

    public function getWeeklyRest()
    {
        return $this->weekly_rest;
    }

    public function getHourlyRate()
    {
        return $this->hourly_rate;
    }

    public function getHourlyRateNet()
    {
        return $this->hourly_rate_net;
    }

    public function getPartialYearWeekNb()
    {
        return $this->partial_year_week_nb;
    }
    
    public function getOccasionalChildcareMonthlyHours(){
        return $this->occasional_childcare_monthly_hours;
    }

    public function getHoursMajRate()
    {
        return $this->hours_maj_rate;
    }

    public function getHoursMajValue()
    {
        return $this->hours_maj_value;
    }

    public function getDifficultyMajRate()
    {
        return $this->difficulty_maj_rate;
    }

    public function getDifficultyMajValue()
    {
        return $this->difficulty_maj_value;
    }

    public function getMaintenanceAllowance()
    {
        return $this->maintenance_allowance;
    }

    public function getMealGiven()
    {
        return $this->meal_given;
    }

    public function getBreakfastCosts()
    {
        return $this->breakfast_costs;
    }

    public function getLunchCosts()
    {
        return $this->lunch_costs;
    }

    public function getSnackCosts()
    {
        return $this->snack_costs;
    }

    public function getDinerCosts()
    {
        return $this->diner_costs;
    }

    public function getPaiementDate()
    {
        return $this->paiement_date;
    }

    public function getFractionnementCongesDu1()
    {
        return $this->fractionnement_conges_du_1;
    }

    public function getFractionnementCongesAu1()
    {
        return $this->fractionnement_conges_au_1;
    }

    public function getFractionnementCongesDu2()
    {
        return $this->fractionnement_conges_du_2;
    }

    public function getFractionnementCongesAu2()
    {
        return $this->fractionnement_conges_au_2;
    }

    public function getVacancyPaymentModality()
    {
        return $this->vacancy_payment_modality;
    }

    public function getPremierJanvier()
    {
        return $this->premier_janvier;
    }

    public function getPaques()
    {
        return $this->paques;
    }

    public function getHuitMai()
    {
        return $this->huit_mai;
    }

    public function getAscension()
    {
        return $this->ascension;
    }

    public function getPentecote()
    {
        return $this->pentecote;
    }

    public function getQuatorzeJuillet()
    {
        return $this->quatorze_juillet;
    }

    public function getQuinzeAout()
    {
        return $this->quinze_aout;
    }

    public function getToussaint()
    {
        return $this->toussaint;
    }

    public function getOnzeNov()
    {
        return $this->onze_nov;
    }

    public function getNoel()
    {
        return $this->noel;
    }

    public function getAdaptationPeriodModality()
    {
        return $this->adaptation_period_modality;
    }

    public function getDetailHourPerDay()
    {
        return $this->detail_hour_per_day;
    }

    public function getSpecialGuardModality()
    {
        return $this->special_guard_modality;
    }

    public function getGuardWeekNb()
    {
        return $this->guard_week_nb;
    }

    public function getVacancyWeekNb()
    {
        return $this->vacancy_week_nb;
    }

    public function getSalaireMensuelBrut()
    {
        return $this->salaire_mensuel_brut;
    }

    public function getColor()
    {
        return $this->color;
    }

    public function getCreated()
    {
        return $this->created;
    }

    public function getValide()
    {
        return $this->valide;
    }

    public function getClosed()
    {
        return $this->closed;
    }

    public function setChildSurname($child_surname)
    {
        $this->child_surname = $child_surname;
        return $this;
    }

    public function setChildFirstname($child_firstname)
    {
        $this->child_firstname = $child_firstname;
        return $this;
    }

    public function setChildBirthDate(\MongoDate $child_birth_date)
    {
        $this->child_birth_date = $child_birth_date;
        return $this;
    }

    public function setMother(\MongoId $mother)
    {
        $this->mother = $mother;
        return $this;
    }

    public function setMotherObject(\Nannyster\Models\Users $mother_oject)
    {
        $this->mother_oject = serialize($mother_oject);
        return $this;
    }

    public function setFather(\MongoId $father)
    {
        $this->father = $father;
        return $this;
    }

    public function setFatherObject(\Nannyster\Models\Users $father_object)
    {
        $this->father_object = serialize($father_object);
        return $this;
    }

    public function setNanny(\MongoId $nanny)
    {
        $this->nanny = $nanny;
        return $this;
    }

    public function setNannyObject(\Nannyster\Models\Users $nanny_object)
    {
        $this->nanny_object = serialize($nanny_object);
        return $this;
    }

    public function setContractStartDate(\MongoDate $contract_start_date)
    {
        $this->contract_start_date = $contract_start_date;
        return $this;
    }

    public function setTryPeriodTime($try_period_time)
    {
        $this->try_period_time = $try_period_time;
        return $this;
    }

    public function setGuardType($guard_type)
    {
        $this->guard_type = $guard_type;
        return $this;
    }

    public function setNbHourWeekly($nb_hour_weekly)
    {
        $this->nb_hour_weekly = $nb_hour_weekly;
        return $this;
    }

    public function setTimeChangeWeekNb($time_change_week_nb)
    {
        $this->time_change_week_nb = $time_change_week_nb;
        return $this;
    }

    public function setWeeklyRest($weekly_rest)
    {
        $this->weekly_rest = $weekly_rest;
        return $this;
    }

    public function setHourlyRate($hourly_rate)
    {
        $this->hourly_rate = $hourly_rate;
        return $this;
    }

    public function setHourlyRateNet($hourly_rate_net)
    {
        $this->hourly_rate_net = $hourly_rate_net;
        return $this;
    }

    public function setPartialYearWeekNb($partial_year_week_nb)
    {
        $this->partial_year_week_nb = $partial_year_week_nb;
        return $this;
    }
    
    public function setOccasionalChildcareMonthlyHours($occasional_childcare_monthly_hours){
        $this->occasional_childcare_monthly_hours = $occasional_childcare_monthly_hours;
        return $this;
    }

    public function setHoursMajRate($hours_maj_rate)
    {
        $this->hours_maj_rate = $hours_maj_rate;
        return $this;
    }

    public function setHoursMajValue($hours_maj_value)
    {
        $this->hours_maj_value = $hours_maj_value;
        return $this;
    }

    public function setDifficultyMajRate($difficulty_maj_rate)
    {
        $this->difficulty_maj_rate = $difficulty_maj_rate;
        return $this;
    }

    public function setDifficultyMajValue($difficulty_maj_value)
    {
        $this->difficulty_maj_value = $difficulty_maj_value;
        return $this;
    }

    public function setMaintenanceAllowance($maintenance_allowance)
    {
        $this->maintenance_allowance = $maintenance_allowance;
        return $this;
    }

    public function setMealGiven($meal_given)
    {
        $this->meal_given = $meal_given;
        return $this;
    }

    public function setBreakfastCosts($breakfast_costs)
    {
        $this->breakfast_costs = $breakfast_costs;
        return $this;
    }

    public function setLunchCosts($lunch_costs)
    {
        $this->lunch_costs = $lunch_costs;
        return $this;
    }

    public function setSnackCosts($snack_costs)
    {
        $this->snack_costs = $snack_costs;
        return $this;
    }

    public function setDinerCosts($diner_costs)
    {
        $this->diner_costs = $diner_costs;
        return $this;
    }

    public function setPaiementDate($paiement_date)
    {
        $this->paiement_date = $paiement_date;
        return $this;
    }

    public function setFractionnementCongesDu1($fractionnement_conges_du_1)
    {
        $this->fractionnement_conges_du_1 = $fractionnement_conges_du_1;
        return $this;
    }

    public function setFractionnementCongesAu1($fractionnement_conges_au_1)
    {
        $this->fractionnement_conges_au_1 = $fractionnement_conges_au_1;
        return $this;
    }

    public function setFractionnementCongesDu2($fractionnement_conges_du_2)
    {
        $this->fractionnement_conges_du_2 = $fractionnement_conges_du_2;
        return $this;
    }

    public function setFractionnementCongesAu2($fractionnement_conges_au_2)
    {
        $this->fractionnement_conges_au_2 = $fractionnement_conges_au_2;
        return $this;
    }

    public function setVacancyPaymentModality($vacancy_payment_modality)
    {
        $this->vacancy_payment_modality = $vacancy_payment_modality;
        return $this;
    }

    public function setPremierJanvier($premier_janvier)
    {
        $this->premier_janvier = $premier_janvier;
        return $this;
    }

    public function setPaques($paques)
    {
        $this->paques = $paques;
        return $this;
    }

    public function setHuitMai($huit_mai)
    {
        $this->huit_mai = $huit_mai;
        return $this;
    }

    public function setAscension($ascension)
    {
        $this->ascension = $ascension;
        return $this;
    }

    public function setPentecote($pentecote)
    {
        $this->pentecote = $pentecote;
        return $this;
    }

    public function setQuatorzeJuillet($quatorze_juillet)
    {
        $this->quatorze_juillet = $quatorze_juillet;
        return $this;
    }

    public function setQuinzeAout($quinze_aout)
    {
        $this->quinze_aout = $quinze_aout;
        return $this;
    }

    public function setToussaint($toussaint)
    {
        $this->toussaint = $toussaint;
        return $this;
    }

    public function setOnzeNov($onze_nov)
    {
        $this->onze_nov = $onze_nov;
        return $this;
    }

    public function setNoel($noel)
    {
        $this->noel = $noel;
        return $this;
    }

    public function setAdaptationPeriodModality($adaptation_period_modality)
    {
        $this->adaptation_period_modality = $adaptation_period_modality;
        return $this;
    }

    public function setDetailHourPerDay($detail_hour_per_day)
    {
        $this->detail_hour_per_day = $detail_hour_per_day;
        return $this;
    }

    public function setSpecialGuardModality($special_guard_modality)
    {
        $this->special_guard_modality = $special_guard_modality;
        return $this;
    }

    public function setGuardWeekNb($guard_week_nb)
    {
        $this->guard_week_nb = $guard_week_nb;
        return $this;
    }

    public function setVacancyWeekNb($vacancy_week_nb)
    {
        $this->vacancy_week_nb = $vacancy_week_nb;
        return $this;
    }

    public function setSalaireMensuelBrut($salaire_mensuel_brut)
    {
        $this->salaire_mensuel_brut = $salaire_mensuel_brut;
        return $this;
    }

    public function setColor($color)
    {
        $this->color = $color;
        return $this;
    }

    public function setCreated($created)
    {
        $this->created = $created;
        return $this;
    }

    public function setValide($valide)
    {
        switch ($valide) {
            case 'true':
                $this->valide = true;
                break;

            default:
                $this->valide = false;
                break;
        }
        return $this;
    }

    public function setClosed($closed)
    {
        $this->closed = $closed;
        return $this;
    }



}
