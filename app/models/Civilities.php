<?php

namespace Nannyster\Models;

class Civilities extends BaseModel
{

    /**
     * The civility object ID
     * 
     * @var \MongoID
     */
    public $_id;

    /**
     * The civility name
     * 
     * @var string
     */
    public $name;

    /**
     * The civilty short name
     * 
     * @var string
     */
    public $short_name;

    public function getId()
    {
        return $this->_id;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getShortName()
    {
        return $this->short_name;
    }

    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    public function setShortName($short_name)
    {
        $this->short_name = $short_name;
        return $this;
    }

}
