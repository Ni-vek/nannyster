<?php

namespace Nannyster\Models;

class Subscriptions extends BaseModel
{

    /**
     * The subscription id
     * 
     * @var \MongoId
     */
    public $_id;

    /**
     * Does the subscription is expired
     * 
     * @var boolean
     */
    public $expired = false;

    /**
     * The promo code id
     * 
     * @var \MongoId
     */
    public $promo_code;

    /**
     * The promo code object
     * 
     * @var \Nannyster\Models\PromoCodes
     */
    private $promo_code_object = null;

    /**
     * The subscription base id
     * 
     * @var \MongoId
     */
    public $subscription_base;

    /**
     * The subscription base object
     * 
     * @var \Nannyster\Models\SubscriptionsBase
     */
    private $subscription_base_object = null;

    /**
     * The subscription start date
     * 
     * @var \MongoDate
     */
    public $start_date;

    /**
     * The subscription end date
     * 
     * @var \MongoDate
     */
    public $end_date;

    /**
     * The user id
     * 
     * @var type \MongoId
     */
    public $user;

    /**
     * The user object
     * 
     * @var \Nannyster\Models\Users
     */
    private $user_object = null;

    public function getId()
    {
        return $this->_id;
    }

    public function getExpired()
    {
        return $this->expired;
    }

    public function getEndDate()
    {
        return $this->end_date;
    }

    public function getPromoCode()
    {
        if ($this->promo_code_object === null && $this->promo_code !== null) {
            $this->promo_code_object = PromoCodes::findById($this->promo_code);
        }
        return $this->promo_code_object;
    }

    public function getSubscriptionBase()
    {
        if ($this->subscription_base_object === null && $this->subscription_base !== null) {
            $this->subscription_base_object = SubscriptionsBase::findById($this->subscription_base);
        }
        return $this->subscription_base_object;
    }

    public function getStartDate()
    {
        return $this->start_date;
    }

    public function getUser()
    {
        if ($this->user_object === null && $this->user !== null) {
            $this->user_object = Users::findById($this->user);
        }
        return $this->user_object;
    }

    public function setExpired($expired)
    {
        $this->expired = $expired;
        return $this;
    }

    public function setPromoCode(\Nannyster\Models\PromoCodes $promo_code)
    {
        $this->promo_code = $promo_code->getId();
        $this->promo_code_object = $promo_code;
    }

    public function setSubscriptionBase(\Nannyster\Models\SubscriptionsBase $subscription_base)
    {
        $this->subscription_base = $subscription_base->getId();
        $this->subscription_base_object = $subscription_base;
        return $this;
    }

    public function setEndDate(\MongoDate $end_date)
    {
        $this->end_date = $end_date;
        return $this;
    }

    public function setStartDate(\MongoDate $start_date)
    {
        $this->start_date = $start_date;
        return $this;
    }

    public function setUser(\Nannyster\Models\Users $user)
    {
        $this->user = $user->getId();
        $this->user_object = $user;
        return $this;
    }

}
