<?php

namespace Nannyster\Models;

class Cities extends BaseModel
{

    /**
     * The city object ID
     * 
     * @var \MongoID
     */
    public $_id;
    public $country = 'france';
    public $departement;
    public $slug;
    public $name;
    public $real_name;
    public $soundex;
    public $metaphone;
    public $zip_code;
    public $city_departement_code;
    public $city_code;
    public $city_part;
    public $city_canton;
    public $amdi;
    public $population_2010;
    public $population_1999;
    public $population_2012;
    public $densite_2010;
    public $surface;
    public $longitude_deg;
    public $latitude_deg;
    public $longitude_grd;
    public $latitude_grd;
    public $longitude_dms;
    public $latitude_dms;
    public $zmin;
    public $zmax;
    public $population_2010_order;
    public $densite_2010_order;
    public $surface_order;

    public function getId()
    {
        return $this->_id;
    }

    public function getCountry()
    {
        return $this->country;
    }

    public function getDepartement()
    {
        return $this->departement;
    }

    public function getSlug()
    {
        return $this->slug;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getRealName()
    {
        return $this->real_name;
    }

    public function getSoundex()
    {
        return $this->soundex;
    }

    public function getMetaphone()
    {
        return $this->metaphone;
    }

    public function getZipCode()
    {
        return $this->zip_code;
    }

    public function getCityDepartementCode()
    {
        return $this->city_departement_code;
    }

    public function getCityCode()
    {
        return $this->city_code;
    }

    public function getCityPart()
    {
        return $this->city_part;
    }

    public function getCityCanton()
    {
        return $this->city_canton;
    }

    public function getAmdi()
    {
        return $this->amdi;
    }

    public function getPopulation2010()
    {
        return $this->population_2010;
    }

    public function getPopulation1999()
    {
        return $this->population_1999;
    }

    public function getPopulation2012()
    {
        return $this->population_2012;
    }

    public function getDensite2010()
    {
        return $this->densite_2010;
    }

    public function getSurface()
    {
        return $this->surface;
    }

    public function getLongitudeDeg()
    {
        return $this->longitude_deg;
    }

    public function getLatitudeDeg()
    {
        return $this->latitude_deg;
    }

    public function getLongitudeGrd()
    {
        return $this->longitude_grd;
    }

    public function getLatitudeGrd()
    {
        return $this->latitude_grd;
    }

    public function getLongitudeDms()
    {
        return $this->longitude_dms;
    }

    public function getLatitudeDms()
    {
        return $this->latitude_dms;
    }

    public function getZmin()
    {
        return $this->zmin;
    }

    public function getZmax()
    {
        return $this->zmax;
    }

    public function getPopulation2010Order()
    {
        return $this->population_2010_order;
    }

    public function getDensite2010Order()
    {
        return $this->densite_2010_order;
    }

    public function getSurfaceOrder()
    {
        return $this->surface_order;
    }

    public function setCountry($country)
    {
        $this->country = $country;
    }

    public function setDepartement($departement)
    {
        $this->departement = $departement;
    }

    public function setSlug($slug)
    {
        $this->slug = $slug;
    }

    public function setName($name)
    {
        $this->name = $name;
    }

    public function setRealName($real_name)
    {
        $this->real_name = $real_name;
    }

    public function setSoundex($soundex)
    {
        $this->soundex = $soundex;
    }

    public function setMetaphone($metaphone)
    {
        $this->metaphone = $metaphone;
    }

    public function setZipCode($zip_code)
    {
        $this->zip_code = $zip_code;
    }

    public function setCityDepartementCode($city_departement_code)
    {
        $this->city_departement_code = $city_departement_code;
    }

    public function setCityCode($city_code)
    {
        $this->city_code = $city_code;
    }

    public function setCityPart($city_part)
    {
        $this->city_part = $city_part;
    }

    public function setCityCanton($city_canton)
    {
        $this->city_canton = $city_canton;
    }

    public function setAmdi($amdi)
    {
        $this->amdi = $amdi;
    }

    public function setPopulation2010($population_2010)
    {
        $this->population_2010 = $population_2010;
    }

    public function setPopulation1999($population_1999)
    {
        $this->population_1999 = $population_1999;
    }

    public function setPopulation2012($population_2012)
    {
        $this->population_2012 = $population_2012;
    }

    public function setDensite2010($densite_2010)
    {
        $this->densite_2010 = $densite_2010;
    }

    public function setSurface($surface)
    {
        $this->surface = $surface;
    }

    public function setLongitudeDeg($longitude_deg)
    {
        $this->longitude_deg = $longitude_deg;
    }

    public function setLatitudeDeg($latitude_deg)
    {
        $this->latitude_deg = $latitude_deg;
    }

    public function setLongitudeGrd($longitude_grd)
    {
        $this->longitude_grd = $longitude_grd;
    }

    public function setLatitudeGrd($latitude_grd)
    {
        $this->latitude_grd = $latitude_grd;
    }

    public function setLongitudeDms($longitude_dms)
    {
        $this->longitude_dms = $longitude_dms;
    }

    public function setLatitudeDms($latitude_dms)
    {
        $this->latitude_dms = $latitude_dms;
    }

    public function setZmin($zmin)
    {
        $this->zmin = $zmin;
    }

    public function setZmax($zmax)
    {
        $this->zmax = $zmax;
    }

    public function setPopulation2010Order($population_2010_order)
    {
        $this->population_2010_order = $population_2010_order;
    }

    public function setDensite2010Order($densite_2010_order)
    {
        $this->densite_2010_order = $densite_2010_order;
    }

    public function setSurfaceOrder($surface_order)
    {
        $this->surface_order = $surface_order;
    }


}
