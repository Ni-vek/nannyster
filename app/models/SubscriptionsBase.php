<?php

namespace Nannyster\Models;

class SubscriptionsBase extends BaseModel
{

    /**
     * The subscription MongoId
     * 
     * @var \MongoId
     */
    public $_id;

    /**
     * The subscription name
     * 
     * @var string
     */
    public $name;

    /**
     * The subscription description
     * 
     * @var string
     */
    public $description;

    /**
     * The subscription duration
     * 
     * @var integer
     */
    public $duration;

    /**
     * The subscription access type
     * Can be 'limited', 'full'
     * 
     * @var string
     */
    public $access_type;

    /**
     * The subscription price
     * @var double
     */
    public $price;

    /**
     * Define if a subscription is active or not
     * 
     * @var boolean
     */
    public $active = false;

    /**
     * Define if a subscription is visible or not
     * 
     * @var boolean
     */
    public $visible = false;

    public function getId()
    {
        return $this->_id;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function getDuration()
    {
        return $this->duration;
    }

    public function getAccessType()
    {
        return $this->access_type;
    }

    public function getPrice()
    {
        return $this->price;
    }

    public function getActive()
    {
        return $this->active;
    }

    public function getVisible()
    {
        return $this->visible;
    }

    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    public function setDescription($description)
    {
        $this->description = $description;
        return $this;
    }

    public function setDuration($duration)
    {
        $this->duration = $duration;
        return $this;
    }

    public function setAccessType($access_type)
    {
        $this->access_type = $access_type;
        return $this;
    }

    public function setPrice($price)
    {
        $this->price = $price;
        return $this;
    }

    public function setActive($active)
    {
        $this->active = $active;
        return $this;
    }

    public function setVisible($visible)
    {
        $this->visible = $visible;
        return $this;
    }

}
