<?php

namespace Nannyster\Models;

class Transactions extends BaseModel
{
    /**
     * The transaction id
     * 
     * @var \MongoId
     */
    public $_id;
    
    /**
     * The payment method
     * 
     * @var string
     */
    public $payment_method;
    
    /**
     * The payer
     * 
     * @var \Nannyster\Models\Users
     */
    public $user;
    
    /**
     * The total amout
     * 
     * @var integer
     */
    public $total;
    
    /**
     * The vat rate
     * 
     * @var float
     */
    public $vat = 0;
    
    /**
     * The currency
     * 
     * @var string
     */
    public $currency;
    
    /**
     * The base subscription
     * 
     * @var \Nannyster\Models\SubscriptionsBase
     */
    public $subscription;
    
    /**
     * The sale id
     * 
     * @var string
     */
    public $sale_id;
    
    /**
     * The sale state
     * 
     * @var string
     */
    public $sale_state;
    
    /**
     * The payment id
     * 
     * @var string
     */
    public $payment_id;
    
    /**
     * The payment date
     * 
     * @var \MongoDate
     */
    public $payment_created;
    
    /**
     * The payment state
     * 
     * @var string
     */
    public $payment_state;
    
    public function getId()
    {
        return $this->_id;
    }

    public function getPaymentMethod()
    {
        return $this->payment_method;
    }

    public function getUser()
    {
        return $this->user;
    }

    public function getTotal()
    {
        return $this->total;
    }

    public function getVat()
    {
        return $this->vat;
    }

    public function getCurrency()
    {
        return $this->currency;
    }

    public function getSubscription()
    {
        return $this->subscription;
    }

    public function getSaleId()
    {
        return $this->sale_id;
    }

    public function getSaleState()
    {
        return $this->sale_state;
    }

    public function getPaymentId()
    {
        return $this->payment_id;
    }

    public function getPaymentCreated()
    {
        return $this->payment_created;
    }

    public function getPaymentState()
    {
        return $this->payment_state;
    }

    public function setPaymentMethod($payment_method)
    {
        $this->payment_method = $payment_method;
        return $this;
    }

    public function setUser(\Nannyster\Models\Users $user)
    {
        $this->user = serialize($user);
        return $this;
    }

    public function setTotal($total)
    {
        $this->total = $total;
        return $this;
    }

    public function setVat($vat)
    {
        $this->vat = $vat;
        return $this;
    }

    public function setCurrency($currency)
    {
        $this->currency = $currency;
        return $this;
    }

    public function setSubscription(\Nannyster\Models\SubscriptionsBase $subscription)
    {
        $this->subscription = serialize($subscription);
        return $this;
    }

    public function setSaleId($sale_id)
    {
        $this->sale_id = $sale_id;
        return $this;
    }

    public function setSaleState($sale_state)
    {
        $this->sale_state = $sale_state;
        return $this;
    }

    public function setPaymentId($payment_id)
    {
        $this->payment_id = $payment_id;
        return $this;
    }

    public function setPaymentCreated(\MongoDate $payment_created)
    {
        $this->payment_created = $payment_created;
        return $this;
    }

    public function setPaymentState($payment_state)
    {
        $this->payment_state = $payment_state;
        return $this;
    }


}