<?php
namespace Nannyster\Models;


/**
 * RememberTokens
 * Stores the remember me tokens
 */
class RememberTokens extends BaseModel
{

    /**
     * The remember id
     * 
     * @var string
     */
    public $_id;

    /**
     * The user id
     * 
     * @var \MongoId
     */
    public $user;
    
    /**
     * The user object
     * 
     * @var \Nannyster\Models\Users
     */
    protected $user_object = null;

    /**
     * The remember token
     * 
     * @var string
     */
    public $token;

    /**
     * The user agent of the remember
     * 
     * @var string
     */
    public $user_agent;

    /**
     * Date of creation
     * 
     * @var \MongoDate
     */
    public $created_at;

    /**
     * The expiration date
     * 
     * @var \MongoDate
     */
    public $expired_at;

    /**
     * Before create the user assign a password
     */
    public function beforeValidationOnCreate()
    {
        $this->created_at = time();
    }
    
    public function getId()
    {
        return $this->_id;
    }

    public function getUser()
    {
        if($this->user_agent === null && $this->user !== null){
            $this->user_object = Users::findById($this->user);
        }
        return $this->user_object;
    }

    public function getToken()
    {
        return $this->token;
    }

    public function getUserAgent()
    {
        return $this->user_agent;
    }

    public function getCreatedAt()
    {
        return $this->created_at;
    }

    public function getExpiredAt()
    {
        return $this->expired_at;
    }

    public function setUser(\Nannyster\Models\Users $user)
    {
        $this->user = $user->getId();
        $this->user_agent = $user;
        return $this;
    }

    public function setToken($token)
    {
        $this->token = $token;
        return $this;
    }

    public function setUserAgent($user_agent)
    {
        $this->user_agent = $user_agent;
        return $this;
    }

    public function setCreatedAt(\MongoDate $created_at)
    {
        $this->created_at = $created_at;
        return $this;
    }

    public function setExpiredAt(\MongoDate $expired_at)
    {
        $this->expired_at = $expired_at;
        return $this;
    }


}
