<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
	</head>
	<body>
		<table>
			<tbody>
				<tr>
					<td>
						<p style="color:#000;font-size: 16px;line-height:24px;font-family:'HelveticaNeue','Helvetica Neue',Helvetica,Arial,sans-serif;font-weight:normal;">

							<h2 style="font-size: 14px;font-family:'HelveticaNeue','Helvetica Neue',Helvetica,Arial,sans-serif;">Vous avez un nouveau message de {{ from }}!</h2>

							<p style="font-size: 13px;line-height:24px;font-family:'HelveticaNeue','Helvetica Neue',Helvetica,Arial,sans-serif;">Pour consulter ce message sur le sujet "{{ subject }}", veuillez cliquer sur le lien ci dessous.

							<br>
							<br>
							<a style="background:#438EB9;color:#fff;padding:10px;text-decoration:none;" href="http://{{ publicUrl }}{{ mailboxUrl }}">Consulter mes messages</a>

							<br>
							<br>
							A très vite sur <a href="{{ publicUrl }}" style="text-decoration: none;"><?php echo $this->config->application->siteTitle; ?></a>!
							<br>
						</p>
					</td>
				</tr>
				<tr>
					<p style="color:#000;font-size: 12px;line-height:24px;font-family:'HelveticaNeue','Helvetica Neue',Helvetica,Arial,sans-serif;font-weight:normal;">
						Vous pouvez à tout moment désactiver les notifications par emails dans votre <a href="http://{{ publicUrl }}/users/view" style="text-decoration: none;">profil</a>.
					</p>
				</tr>
			</tbody>
		</table>
	</body>
</html>

