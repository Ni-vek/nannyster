<?php

$loader = new \Phalcon\Loader();

/**
 * We're a registering a set of directories taken from the configuration file
 */
$loader->registerNamespaces(
        array(
            'Nannyster'             => $config->application->libraryDir,
            'Nannyster\Controllers' => $config->application->controllersDir,
            'Nannyster\Models'      => $config->application->modelsDir,
            'Nannyster\Forms'       => $config->application->formsDir
        )
)->register();
