<?php

return new \Phalcon\Config(array(
    'application' => array(
        'controllersDir'            => APP_DIR . '/controllers/',
        'modelsDir'                 => APP_DIR . '/models/',
        'formsDir'                  => APP_DIR . '/forms/',
        'viewsDir'                  => APP_DIR . '/views/',
        'libraryDir'                => APP_DIR . '/library/',
        'pluginsDir'                => APP_DIR . '/plugins/',
        'cacheDir'                  => APP_DIR . '/cache/',
        'logDir'                    => BASE_DIR . '/var/log/',
        'baseUri'                   => '/',
        'siteTitle'                 => 'Ma Nourrice {{ dev }}',
        'protocol'                  => 'http://',
        'publicUrl'                 => 'ma-nourrice',
        'domain'                    => 'ma-nourrice',
        'defaultSubscription'       => 'Offre de lancement',
        'cryptSalt'                 => 'eEAdfgfR|_&G&f,+vU]:jFr!!A&+71w1ds76786M754@@s9~8_4L!<74@[N@DyaIP_2My|:+.u>/6m,$D',
        'isInMaintenance'           => false
    ),
    'paypal' => array(
        'account'                   => 'payment-facilitator@ma-nourrice.fr',
        'endpoint'                  => 'api.sandbox.paypal.com',
        'mode'                      => 'sandbox',
        'clientId'                  => 'AW5Y9hBOfxfhJYPTQEWzGAkcwaPGyTnG9cVcwrwJZsm3tl_Uarqr0loAZDMV',
        'secret'                    => 'EPKFsxCi75lQdCMafzCLqmD6VnEsplyj5Qqy99JL0UnmVIOKgShfrp-or2mQ',
        'logLevel'                  => 'FINE',
        'logPath'                   => BASE_DIR . '/var/log/paypal.log',
        'currency'                  => 'EUR',
        'redirectSuccessUrl'        => '/transactions/success',
        'redirectCancelUrl'         => '/transactions/error'
    ),
    'company' => array(
        'name'                      => 'Kevin BALINI (ma-nourrice)',
        'address'                   => '4 Bis Rue de la Poëlerie',
        'zip_code'                  => '44120',
        'city'                      => 'Vertou',
        'email'                     => 'contact@ma-nourrice.fr',
        'phone'                     => '02 40 04 63 12',
        'mobile'                    => '06 14 28 40 00',
        'siret'                     => 'En cours d\'attribution',
        'contact'                   => 'contact@ma-nourrice.fr'
    ),
    'cookies' => array(
        'expireTime'                => 86400 * 30
    ),
    'mail' => array(
        'fromName'                  => 'Ma Nourrice',
        'fromEmail'                 => 'contact@ma-nourrice.fr',
        'mandrillApiKey'            => 'HWk4p2D35PjYnpJrvxPFcg' //prod : HWk4p2D35PjYnpJrvxPFcg
    ),
    'db' => array(
        'username'                  => null,
        'password'                  => null,
        'host'                      => null,
        'port'                      => 32172,
        'database'                  => 'ma-nourrice'
    )
));
